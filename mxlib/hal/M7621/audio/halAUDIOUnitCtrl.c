//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    halAUDIOUnitCtrl.c
/// @brief  AUDIO "Unit" Control WITHOUT COMBO SKILL!!!!
/// @author MStar Semiconductor,Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined(ROLLS_ROYCE) || defined(AUDIO_UNIT_CTRL)

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#include standard header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [#include local header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#include "halAUDIOUnitCtrl.h"
#include "halAUDIO.h"
#include "regAUDIO.h"
#include "drvAUDIO_if.h"
#include "../../../drv/audio/internal/drvAUDIO_internal.h"
#include "../../../drv/audio/internal/drvAUDIOUnitCtrl_config.h"

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#include local library] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [#define] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#define HINIBBLE(u8Data)    ((u8Data >> 4) & 0x0F)
#define LONIBBLE(u8Data)    ((u8Data) & 0x0F)

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#define] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [variable / struct / enum] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [variable / struct / enum] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Pre-Declaration Area] pre-declared [function] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
/* Initialize, STR */

/* Connect & Disconnect */

/* Start & Stop */

/* SPDIF */

/* HDMI */

/* ATV */

/* Decoder */

/* Common Decoder */

/* Common */

/* Customized patch */

/* Customized Internal patch */

/* Clip Play for ES */

/* Clip Play for PCM */

/* Gain, Mute & Delay */

/* AENC */

/* PCM Capture */

/* PCM IO Control */

/* MM New Mode */

/* Mstar Sound Effect */

/* Advanced Sound Effect */

/* internal used functions */

#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [function] declare / implement in this area.
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [function] declare / implement in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// please understand, it's important.
//-------------------------------------------------------------------------------------------------------------------------------------
/* Initialize, STR */

/* Connect & Disconnect */
MS_BOOL HAL_AUDIO_UNIT_SetParserMode(AUDIO_UNIT_PARSER_MODE ParserMode)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_ADEC_Connect(AUDIO_UNIT_ADEC_INDEX currentConnect, AUDIO_UNIT_ADEC_INPUT inputConnect)
{
    switch(currentConnect)
    {
        case AUDIO_UNIT_ADEC0:
        {
            switch(inputConnect)
            {
                case AUDIO_UNIT_ADEC_INPUT_DTV:
                {
                    HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER1_CFG, 0x07, 0x00);  // Set main parser source
                    HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER2_CFG, 0x07, 0x00);  // Set AD parser source
                }
                break;

                case AUDIO_UNIT_ADEC_INPUT_MM:
                case AUDIO_UNIT_ADEC_INPUT_MM_UNI:
                case AUDIO_UNIT_ADEC_INPUT_CLIP:
                {
                    HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER1_CFG, 0x07, 0x00);
                }
                break;

                case AUDIO_UNIT_ADEC_INPUT_HDMI:
                {
                    HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER1_CFG, 0x07, 0x04);
                }
                break;

                default:
                    break;
            }
        }
        break;

        case AUDIO_UNIT_ADEC1:
        {
            switch(inputConnect)
            {
                case AUDIO_UNIT_ADEC_INPUT_DTV:
                {
                    HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER3_CFG, 0x07, 0x00);  // Set main parser source
                    HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER4_CFG, 0x07, 0x00);  // Set AD parser source
                }
                break;

                case AUDIO_UNIT_ADEC_INPUT_MM:
                case AUDIO_UNIT_ADEC_INPUT_MM_UNI:
                case AUDIO_UNIT_ADEC_INPUT_CLIP:
                {
                    HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER3_CFG, 0x07, 0x00);
                }
                break;

                case AUDIO_UNIT_ADEC_INPUT_HDMI:
                {
                    HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER3_CFG, 0x07, 0x04);
                }
                break;

                default:
                    break;
            }
        }
        break;

        case AUDIO_UNIT_ADEC_ATV:
        {
            switch(inputConnect)
            {
                case AUDIO_UNIT_ADEC_INPUT_ATV:
                {
                    HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER4_CFG, 0x07, 0x07);
                }
                break;

                default:
                    break;
            }
        }
        break;

        default:
            break;

    }

    return TRUE;
}

MS_BOOL HAL_AUDIO_UNIT_ADEC_Disconnect(AUDIO_UNIT_ADEC_INDEX currentConnect)
{
    switch(currentConnect)
    {
        case AUDIO_UNIT_ADEC0:
        {
            HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER1_CFG, 0x07, 0x00);  // Set main parser source
            HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER2_CFG, 0x07, 0x00);  // Set AD parser source
        }
        break;

        case AUDIO_UNIT_ADEC1:
        {
            HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER3_CFG, 0x07, 0x00);  // Set main parser source
            HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER4_CFG, 0x07, 0x00);  // Set AD parser source
        }
        break;

        case AUDIO_UNIT_ADEC_ATV:
        {
            HAL_AUDIO_WriteMaskByte(REG_AUDIO_DECODER4_CFG, 0x07, 0x00);
        }
        break;

        default:
            break;
    }

    return TRUE;
}

MS_BOOL HAL_AUDIO_UNIT_ADC_Connect(AUDIO_UNIT_ADC_INDEX currentConnect, AUDIO_UNIT_ADC_IN_PORT portNum)
{
    MS_U8 u8input_idx = 0;
    MS_U8 u8temp = 0;

    u8input_idx = HINIBBLE(portNum);

    switch(currentConnect)
    {
        case AUDIO_UNIT_ADC0:
        {
            if(u8input_idx == 0x0A)
            {
                u8temp = 0x40;
            }
            else if(u8input_idx == 0x0B)
            {
                u8temp = 0x50;
            }
            else
            {
                u8temp = (u8input_idx << 4);
            }

            HAL_AUDIO_WriteMaskByte(0x2CE2, 0xF0, u8temp);
        }
        break;

        case AUDIO_UNIT_ADC1:
        {
            if(u8input_idx == 0x0A)
            {
                u8temp = 0x04;
            }
            else if(u8input_idx == 0x0B)
            {
                u8temp = 0x05;
            }
            else
            {
                u8temp = u8input_idx;
            }

            HAL_AUDIO_WriteMaskByte(0x2CE2, 0x0F, u8temp);
        }
        break;

        default:
            break;
    }

    return TRUE;
}

MS_BOOL HAL_AUDIO_UNIT_ADC_Disconnect(AUDIO_UNIT_ADC_INDEX currentConnect, AUDIO_UNIT_ADC_IN_PORT portNum)
{
    switch(currentConnect)
    {
        case AUDIO_UNIT_ADC0:
        {
            HAL_AUDIO_WriteMaskByte(0x2CE2, 0xF0, 0xF0);
        }
        break;

        case AUDIO_UNIT_ADC1:
        {
            HAL_AUDIO_WriteMaskByte(0x2CE2, 0x0F, 0x0F);
        }
        break;

        default:
            break;
    }

    return TRUE;
}

MS_BOOL HAL_AUDIO_UNIT_PCM_Mixer_Connect(AUDIO_UNIT_PCM_MIXER_INDEX currentConnect, AUDIO_UNIT_PCM_MIXER_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_PCM_Mixer_Disconnect(AUDIO_UNIT_PCM_MIXER_INDEX currentConnect, AUDIO_UNIT_PCM_MIXER_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_CH_Sound_Connect(AUDIO_UNIT_CH_SOUND currentConnect, AUDIO_UNIT_CH_INPUT inputConnect)
{
    MS_U32 u32path_reg = 0xFFFF;

    switch(currentConnect)
    {
        case AUDIO_UNIT_CH1_SOUND:
        {
            u32path_reg = REG_AUDIO_CH1_CFG;
        }
        break;

        case AUDIO_UNIT_CH2_SOUND:
        {
            u32path_reg = REG_AUDIO_CH2_CFG;
        }
        break;

        case AUDIO_UNIT_CH3_SOUND:
        {
            u32path_reg = REG_AUDIO_CH3_CFG;
        }
        break;

        case AUDIO_UNIT_CH4_SOUND:
        {
            u32path_reg = REG_AUDIO_CH4_CFG;
        }
        break;

        case AUDIO_UNIT_CH5_SOUND:
        {
            u32path_reg = REG_AUDIO_CH5_CFG;
        }
        break;

        case AUDIO_UNIT_CH6_SOUND:
        {
            u32path_reg = REG_AUDIO_CH6_CFG;
        }
        break;

        case AUDIO_UNIT_CH7_SOUND:
        {
            u32path_reg = REG_AUDIO_CH7_CFG;
        }
        break;

        case AUDIO_UNIT_CH8_SOUND:
        {
            u32path_reg = REG_AUDIO_CH8_CFG;
        }
        break;

        default:
            break;
    }

    if(u32path_reg == 0xFFFF)
    {
        return FALSE;
    }

    switch(inputConnect)
    {
        case AUDIO_UNIT_CH_INPUT_ADEC0:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x80);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_ADEC1:
        {
            #if(R2_SUPPORT_R2_DEC_ISR2_EN)
            {
                //  _____ ___________ _______________
                // |     | R2 DMA    |  DSP Dec out  |
                // |_____|___________|_______________|
                // |ADEC0| R2_DMA_1  |R2_DEC_ISR1_EN |
                // |     | (0x80)    |    (0x80)     |
                // |_____|___________|_______________|
                // |ADEC1| R2_DMA_2  |R2_DEC_ISR2_EN |
                // |     | (0x82)    |    (0x83)     |
                // |     | 1R1D not  |               |
                // |_____|_Support___|_______________|
                HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x83);
            }
            #else
            {
                HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x82);
            }
            #endif
        }
        break;

        case AUDIO_UNIT_CH_INPUT_ADEC_ATV:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x83);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_HDMI:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x84);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_ADC0:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x88);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_ADC1:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x89);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_SPDIF:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x86);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_R2DMA_DSP1:
        {

        }
        break;

        case AUDIO_UNIT_CH_INPUT_R2DMA_DSP3:
        {

        }
        break;

        case AUDIO_UNIT_CH_INPUT_SWDMA_DSP3:
        {

        }
        break;

        case AUDIO_UNIT_CH_INPUT_HWDMA:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x8F);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_NONE:
        {

        }
        break;

        default:
            break;
    }

    return TRUE;
}

MS_BOOL HAL_AUDIO_UNIT_CH_Sound_Disconnect(AUDIO_UNIT_CH_SOUND currentConnect, AUDIO_UNIT_CH_INPUT inputConnect)
{
    MS_U32 u32path_reg = 0xFFFF;

    switch(currentConnect)
    {
        case AUDIO_UNIT_CH1_SOUND:
        {
            u32path_reg = REG_AUDIO_CH1_CFG;
        }
        break;

        case AUDIO_UNIT_CH5_SOUND:
        {
            u32path_reg = REG_AUDIO_CH5_CFG;
        }
        break;

        case AUDIO_UNIT_CH6_SOUND:
        {
            u32path_reg = REG_AUDIO_CH6_CFG;
        }
        break;

        case AUDIO_UNIT_CH7_SOUND:
        {
            u32path_reg = REG_AUDIO_CH7_CFG;
        }
        break;

        case AUDIO_UNIT_CH8_SOUND:
        {
            u32path_reg = REG_AUDIO_CH8_CFG;
        }
        break;

        default:
            break;
    }

    if(u32path_reg == 0xFFFF)
    {
        return FALSE;
    }

    switch(inputConnect)
    {
        case AUDIO_UNIT_CH_INPUT_ADEC0:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x81);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_ADEC1:
        {
            #if(R2_SUPPORT_R2_DEC_ISR2_EN)
            {
                //  _____ ___________ _______________
                // |     | R2 DMA    |  DSP Dec out  |
                // |_____|___________|_______________|
                // |ADEC0| R2_DMA_1  |R2_DEC_ISR1_EN |
                // |     | (0x80)    |    (0x80)     |
                // |_____|___________|_______________|
                // |ADEC1| R2_DMA_2  |R2_DEC_ISR2_EN |
                // |     | (0x82)    |    (0x83)     |
                // |     | 1R1D not  |               |
                // |_____|_Support___|_______________|
                HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x81);
            }
            #else
            {
                HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x81);
            }
            #endif
        }
        break;

        case AUDIO_UNIT_CH_INPUT_ADEC_ATV:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x81);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_HDMI:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x81);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_ADC0:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x81);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_ADC1:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x81);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_SPDIF:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x81);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_R2DMA_DSP1:
        {

        }
        break;

        case AUDIO_UNIT_CH_INPUT_R2DMA_DSP3:
        {

        }
        break;

        case AUDIO_UNIT_CH_INPUT_SWDMA_DSP3:
        {

        }
        break;

        case AUDIO_UNIT_CH_INPUT_HWDMA:
        {
            HAL_AUDIO_WriteMaskByte(u32path_reg, 0x9F, 0x81);
        }
        break;

        case AUDIO_UNIT_CH_INPUT_NONE:
        {

        }
        break;

        default:
            break;
    }

    return TRUE;
}

MS_BOOL HAL_AUDIO_UNIT_FW_MIXER_Connect(AUDIO_UNIT_FWM_INDEX currentConnect, AUDIO_UNIT_FWM_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_FW_MIXER_Disconnect(AUDIO_UNIT_FWM_INDEX currentConnect, AUDIO_UNIT_FWM_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_SE_Connect(AUDIO_UNIT_SE_INDEX currentConnect, AUDIO_UNIT_SE_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_SE_Disconnect(AUDIO_UNIT_SE_INDEX currentConnect, AUDIO_UNIT_SE_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_SNDOUT_Connect(AUDIO_UNIT_SOUNDOUT_INDEX currentConnect, AUDIO_UNIT_SOUNDOUT_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_SNDOUT_Disconnect(AUDIO_UNIT_SOUNDOUT_INDEX currentConnect, AUDIO_UNIT_SOUNDOUT_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_PCM_CAPTURE_Connect(AUDIO_UNIT_PCM_CAPTURE_INDEX currentConnect, AUDIO_UNIT_PCM_CAPTURE_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_PCM_CAPTURE_Disconnect(AUDIO_UNIT_PCM_CAPTURE_INDEX currentConnect, AUDIO_UNIT_PCM_CAPTURE_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_MP3_ENC_Connect(AUDIO_UNIT_MP3_ENC_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_MP3_ENC_Disconnect(AUDIO_UNIT_MP3_ENC_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_AAC_ENC_Connect(AUDIO_UNIT_AAC_ENC_INPUT inputConnect)
{
    return FALSE;
}

MS_BOOL HAL_AUDIO_UNIT_AAC_ENC_Disconnect(AUDIO_UNIT_AAC_ENC_INPUT inputConnect)
{
    return FALSE;
}

/* Start & Stop */

/* SPDIF */

/* HDMI */

/* ATV */

/* Decoder */

/* Common Decoder */

/* Common */

/* Customized patch */

/* Clip Play for ES */

/* Clip Play for PCM */

/* Gain, Mute & Delay */

/* AENC */

/* PCM Capture */

/* PCM IO Control */

/* MM New Mode */

/* Mstar Sound Effect */

/* Advanced Sound Effect */

#endif //#if defined(ROLLS_ROYCE) || defined(AUDIO_UNIT_CTRL)
