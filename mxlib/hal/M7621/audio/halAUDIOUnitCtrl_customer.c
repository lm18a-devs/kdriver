//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    halAUDIOUnitCtrl_customer.c
/// @brief  AUDIO "Unit" Control WITHOUT COMBO SKILL!!!!
/// @author MStar Semiconductor,Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined(ROLLS_ROYCE) || defined(AUDIO_UNIT_CTRL)

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#include standard header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [#include local header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#include "halAUDIOUnitCtrl_customer.h"
#include "../../../drv/audio/internal/drvAUDIOUnitCtrl_config.h"
#include "../../../drv/audio/internal/drvAUDIOUnitCtrl_customer_config.h"

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#include local library] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [#define] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#define] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
#define HalAudioUnitCusPRINT(_f,_a...)              \
    {                                               \
        printf("\033[1;36m"_f"\033[0m", ##_a);      \
    }

#else //User Space
#define HalAudioUnitCusPRINT(_f,_a...)              \
    {                                               \
        printf("\033[1;36m"_f"\033[0m", ##_a);      \
    }

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [variable / struct / enum] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [variable / struct / enum] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Pre-Declaration Area] pre-declared [function] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [function] declare / implement in this area.
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [function] declare / implement in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
int HalAudioUnitCus_strcmp(const char * str1, const char * str2, size_t n)
{
    #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
    {
        #if(LINUX_KERNEL_VERSION_4_4_3)
        {
            return strncasecmp(str1, str2, n);
        }
        #else
        {
            return strnicmp(str1, str2, n);
        }
        #endif
    }
    #else //User Space
    {
        return strcmp(str1, str2);
    }
    #endif
}

#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// please understand, it's important.
//-------------------------------------------------------------------------------------------------------------------------------------
/* String Cmd */
MS_BOOL HAL_AUDIO_UNIT_CUSTOMER_CMD(char *pStrCmd, MS_U32 u32StrCmdLength, void *pData, MS_U32 u32DataLength)
{
    if((pStrCmd == NULL) || (u32StrCmdLength == 0) || (pData == NULL) || (u32DataLength == 0))
    {
        return FALSE;
    }

    //================================================================================================
    //                                          Set
    //================================================================================================
    API_AUDIO_UNIT_CUSTOMER_SET_PARAM_DEMO *setParamDemo_t;

    HalAudioUnitCusPRINT("u32StrCmdLength: %d \n", (unsigned int)u32StrCmdLength);
    HalAudioUnitCusPRINT("u32DataLength:   %d \n", (unsigned int)u32DataLength);

    if(HalAudioUnitCus_strcmp(pStrCmd, "setParamDemo", u32StrCmdLength) == 0)
    {
        setParamDemo_t = (API_AUDIO_UNIT_CUSTOMER_SET_PARAM_DEMO *)pData;
        HalAudioUnitCusPRINT("[param   = %d] \n", (unsigned int)setParamDemo_t->demoParam);
        HalAudioUnitCusPRINT("[param2  = %d] \n", (unsigned int)setParamDemo_t->demoParam2);
        HalAudioUnitCusPRINT("[param3  = %d] \n", (unsigned int)setParamDemo_t->demoParam3);
        HalAudioUnitCusPRINT("[param4  = %d] \n", (unsigned int)setParamDemo_t->demoParam4);
        HalAudioUnitCusPRINT("[param5  = %d] \n", (unsigned int)setParamDemo_t->demoParam5);
        HalAudioUnitCusPRINT("[param6  = %d] \n", (unsigned int)setParamDemo_t->demoParam6);
        HalAudioUnitCusPRINT("[param7  = %d] \n", (unsigned int)setParamDemo_t->demoParam7);
        HalAudioUnitCusPRINT("[param8  = %d] \n", (unsigned int)setParamDemo_t->demoParam8);
        HalAudioUnitCusPRINT("[param9  = %d] \n", (unsigned int)setParamDemo_t->demoParam9);
        HalAudioUnitCusPRINT("[param10 = %d] \n", (unsigned int)setParamDemo_t->demoParam10);
    }

    //================================================================================================
    //                                          Get
    //================================================================================================
    API_AUDIO_UNIT_CUSTOMER_GET_INFO_DEMO *getInfoDemo_t;

    if(HalAudioUnitCus_strcmp(pStrCmd, "getInfoDemo", u32StrCmdLength) == 0)
    {
        getInfoDemo_t = (API_AUDIO_UNIT_CUSTOMER_GET_INFO_DEMO *)pData;
        getInfoDemo_t->demoInfo   = 1;
        getInfoDemo_t->demoInfo2  = 2;
        getInfoDemo_t->demoInfo3  = 3;
        getInfoDemo_t->demoInfo4  = 4;
        getInfoDemo_t->demoInfo5  = 5;
        getInfoDemo_t->demoInfo6  = 6;
        getInfoDemo_t->demoInfo7  = 7;
        getInfoDemo_t->demoInfo8  = 8;
        getInfoDemo_t->demoInfo9  = 9;
        getInfoDemo_t->demoInfo10 = 10;
    }

    return TRUE;
}

#endif //#if defined(ROLLS_ROYCE) || defined(AUDIO_UNIT_CTRL)

