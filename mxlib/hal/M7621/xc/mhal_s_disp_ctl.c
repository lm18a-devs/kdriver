//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (!¡±MStar Confidential Information!¡L) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////
//==============================================================================
// Common Definition
#ifdef STELLAR
#define SHARP_VBY1 1
#ifdef MSOS_TYPE_LINUX_KERNEL
#include <linux/string.h>
#include <asm/div64.h>
#else
#include <string.h>
#define do_div(x,y) ((x)/=(y))
#endif
#include "UFO.h"

// Common Definition
#include "MsCommon.h"
#include "MsIRQ.h"
#include "MsOS.h"
#include "mhal_xc_chip_config.h"
#include "utopia.h"
#include "utopia_dapi.h"
#include "xc_hwreg_utility2.h"
#include "hwreg_pm_sleep.h"//alex_tung
#include "hwreg_dvi_atop.h"//alex_tung
#include "hwreg_adc_atop.h"
#include "hwreg_adc_dtop.h"
#include "hwreg_adc_dtopb.h"
#include "hwreg_hdmi.h"
#include "hwreg_ipmux.h"
#include "hwreg_ddc.h"

#include "xc_Analog_Reg.h"
#include "drvXC_IOPort.h"
#include "apiXC.h"
#include "drv_sc_display.h"
#include "drv_sc_isr.h"
#include "apiXC_Adc.h"
#include "apiXC_PCMonitor.h"
#include "apiXC_ModeParse.h"
#include "apiXC_Auto.h"
#include "drvXC_HDMI_if.h"
#include "mvideo_context.h"
#include "drv_sc_ip.h"
#include "mhal_dynamicscaling.h"
#include "mdrv_sc_dynamicscaling.h"
#include "drvXC_HDMI_Internal.h"
#include "drvMVOP.h"
#include "drvIPAUTH.h"
#if (LD_ENABLE==1)
#include "mdrv_ld.h"
#include "mdrv_ldalgo.h"
#endif
#include "mdrv_sc_3d.h"
#include "drv_sc_menuload.h"
#include "drvXC_ADC_Internal.h"
#include "mhal_sc.h"
#if FRC_INSIDE
#include "mdrv_frc.h"
#include "mhal_frc.h"
#endif
#include "XC_private.h"
#include "mhal_menuload.h"
#include "mhal_adc.h"
#include "halCHIP.h"
#include "drvSYS.h"
#include "drv_sc_scaling.h"
#include "drvSEAL.h"

#include "mhal_s_disp_ctl.h"


#include "MsCommon.h"
#include "MsOS.h"
#include "mhal_xc_chip_config.h"
#include "apiPNL.h"

#include "drvBDMA.h"
#include "drvPWM.h"

#include "drvPQ.h"

// input resolution
typedef struct
{
    MS_U16 u16DisplayWidth;  // width
    MS_U16 u16DisplayHeight; // height

}MS_MODE_RESOLUTION;

/*#ifdef STELLAR*/
typedef enum
{
    MHAL_E_TOOL_OPTION1_W8 = 0, // 1
    MHAL_E_TOOL_OPTION1_C8, 	// 2
    MHAL_E_TOOL_OPTION1_E8, 	// 3
    MHAL_E_TOOL_OPTION1_G8, 	// 4
    MHAL_E_TOOL_OPTION1_B8, 	// 5
    MHAL_E_TOOL_OPTION1_G7, 	// 6
    MHAL_E_TOOL_OPTION1_SK95, 	// 7
    MHAL_E_TOOL_OPTION1_SK85, 	// 8
    MHAL_E_TOOL_OPTION1_SK80, 	// 9
    MHAL_E_TOOL_OPTION1_UK75, 	// 10
    MHAL_E_TOOL_OPTION1_UK65, 	// 11
    MHAL_E_TOOL_OPTION1_UK63, 	// 12
    MHAL_E_TOOL_OPTION1_UK62, 	// 13
    MHAL_E_TOOL_OPTION1_LK61, 	// 14
    MHAL_E_TOOL_OPTION1_LK57, 	// 15
    MHAL_E_TOOL_OPTION1_LK54, 	// 16
    MHAL_E_TOOL_OPTION1_LK619,	// 17
    MHAL_E_TOOL_OPTION1_LK616,	// 18
    MHAL_E_TOOL_OPTION1_NUM,
    MHAL_E_TOOL_OPTION1_LCD_END = MHAL_E_TOOL_OPTION1_NUM,
} MHAL_EN_TOOL_OPTION1_ToolType_IDX_T;

typedef enum
{
    MHAL_E_TOOL_OPTION1_INCH_22 = 0,
    MHAL_E_TOOL_OPTION1_INCH_23,
    MHAL_E_TOOL_OPTION1_INCH_24,
    MHAL_E_TOOL_OPTION1_INCH_26,
    MHAL_E_TOOL_OPTION1_INCH_27,
    MHAL_E_TOOL_OPTION1_INCH_28,
    MHAL_E_TOOL_OPTION1_INCH_32,
    MHAL_E_TOOL_OPTION1_INCH_39,
    MHAL_E_TOOL_OPTION1_INCH_40,
    MHAL_E_TOOL_OPTION1_INCH_42,
    MHAL_E_TOOL_OPTION1_INCH_43,
    MHAL_E_TOOL_OPTION1_INCH_47,
    MHAL_E_TOOL_OPTION1_INCH_49,
    MHAL_E_TOOL_OPTION1_INCH_50,
    MHAL_E_TOOL_OPTION1_INCH_55,
    MHAL_E_TOOL_OPTION1_INCH_58,
    MHAL_E_TOOL_OPTION1_INCH_60,
    MHAL_E_TOOL_OPTION1_INCH_65,
    MHAL_E_TOOL_OPTION1_INCH_70,

    //16Y Inch addition
    MHAL_E_TOOL_OPTION1_INCH_75,

    MHAL_E_TOOL_OPTION1_INCH_77,
    MHAL_E_TOOL_OPTION1_INCH_79,
    MHAL_E_TOOL_OPTION1_INCH_84,
    MHAL_E_TOOL_OPTION1_INCH_86,
    MHAL_E_TOOL_OPTION1_INCH_98,
    MHAL_E_TOOL_OPTION1_INCH_105,

    //17Y Inch addition
    MHAL_E_TOOL_OPTION1_INCH_48,
    MHAL_E_TOOL_OPTION1_INCH_BASE
} MHAL_EN_TOOL_OPTION1_INCH_TYPE_T;

typedef enum
{
    MHAL_EN_TOOL_OPTION1_MODULE_LGD = 0,
    MHAL_EN_TOOL_OPTION1_MODULE_AUO,
    MHAL_EN_TOOL_OPTION1_MODULE_SHARP,
    MHAL_EN_TOOL_OPTION1_MODULE_BOE,
    MHAL_EN_TOOL_OPTION1_MODULE_CSOT,
    MHAL_EN_TOOL_OPTION1_MODULE_INNOLUX,
    MHAL_EN_TOOL_OPTION1_MODULE_LGD_M,
    MHAL_EN_TOOL_OPTION1_MODULE_ODM_B,
    MHAL_EN_TOOL_OPTION1_MODULE_BOE_TPV,
    MHAL_EN_TOOL_OPTION1_MODULE_HKC,
    MHAL_EN_TOOL_OPTION1_MODULE_LCD_END,
    MHAL_EN_TOOL_OPTION1_MODULE_BASE	= MHAL_EN_TOOL_OPTION1_MODULE_LCD_END,
}  MHAL_EN_TOOL_OPTION1_MAKER_TYPE_T;
/*#endif*/
#if SHARP_VBY1
static MS_U16 u16Inch = 0xFFFF;
static MS_U16 u16ToolType = 0xFFFF;
static MS_U16 u16ModuleType = 0xFFFF;
#endif


static const MS_MODE_RESOLUTION astStandardModeResolution[RES_MAXIMUM] =
{
    { 640,  350}, // 00: RES_640X350
    { 640,  400}, // 01: RES_640X400
    { 720,  400}, // 02: RES_720X400
    { 640,  480}, // 03: RES_640X480
    { 800,  600}, // 04: RES_800X600
    { 832,  624}, // 05: RES_832X624
    {1024,  768}, // 06: RES_1024X768
    {1280, 1024}, // 07: RES_1280X1024
    {1600, 1200}, // 08: RES_1600X1200
    {1152,  864}, // 09: RES_1152X864
    {1152,  870}, // 10: RES_1152X870
    {1280,  768}, // 11: RES_1280x768
    {1280,  960}, // 12: RES_1280X960
    { 720,  480}, // 13: RES_720X480
    {1920, 1080}, // 14: RES_1920X1080
    {1280,  720}, // 15: RES_1280X720
    { 720,  576}, // 16: RES_720X576
    {1920, 1200}, // 17: RES_1920X1200
    {1400, 1050}, // 18: RES_1400X1050
    {1440,  900}, // 19: RES_1440X900
    {1680, 1050}, // 20: RES_1680X1050
    {1280,  800}, // 21: RES_1280X800
    {1600, 1024}, // 22: RES_1600X1024
    {1600,  900}, // 23: RES_1600X900
    {1360,  768}, // 24: RES_1360X768
    { 848,  480}, // 25: RES_848X480
    {1920, 1080}, // 26: RES_1920X1080P
    {1366,  768}, // 27: RES_1366X768,
    { 864,  648}, // 28: RES_864X648,
    {1280, 1470}, // 29: RES_1280X1470,
    {1920, 2205}, // 30: RES_1920X2205,
    { 720,  240}, // 31: RES_720x240,
    { 720,  288}, // 32: RES_720x288,
    {1152,  900}, // 33: RES_1152X900,
    { 856,  480}, // 34: RES_856x480,
    {1536,  960}, // 35: RES_1536X960,
    {1600, 1000}, // 36: RES_1600X1000,
    {1704,  960}, // 37: RES_1704X960,
    {1728, 1080}, // 38: RES_1728X1080,
    {1864, 1050}, // 39: RES_1864X1050,
};

static MS_BOOL bIsSmartZoomOn = FALSE;
static MS_BOOL bIsKR3DMode = FALSE;
static MS_BOOL bDualWinForAPVR = FALSE;
static MS_U16  u16ForceSC2OP = 0;
static MS_BOOL bIsGstreamerProcess = FALSE;
static MS_BOOL bIsCaptureProcess = FALSE;

static MS_U16 _u16PQdriver_status = 0;
static MS_U16 _u16MADI_SST_status = 0;
static MS_U16 _u16DIPF_status = 0;
static MS_U16 _u16film32_status = 0;
static MS_U16 _u16film22_status = 0;
static MS_U16 _u16filmAny_status = 0;

static MS_BOOL bLdInterruptStatus = 0;
static MS_U32 u32LdRecordingTime = 0;

static MS_XC_CUS_PATTERN_VIDEO_LATENCY gstPatternPara =
{
    .bEnable = FALSE,
    .bColorFMTYUV = FALSE,
    .eVscPatternType = E_XC_VSC_PATTERN_WHITE,
    .eWindow = MAIN_WINDOW,
    .stOverlapWindow = {0,0,0,0}
};

MS_BOOL bSTROn = FALSE;

extern ST_PNL_TCON_Config stTCONPanelConfig;

extern XC_PNL_OBJ g_IPanel;
extern MS_BOOL MHal_PNL_MOD_Control_Out_PE_Current (void *pInstance, MS_U16 u16Current_Level);


// ADC
MS_BOOL Hal_XC_S_ADC_ScartOverlay_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->bParam)	// Show SCART-CVBS & SCART-RGB simutaneously
    {
        W2BYTEMSK(REG_ADC_ATOP_42_L, 0x3 << 6, (BIT(7)|BIT(6)));
        W2BYTEMSK(REG_ADC_ATOP_43_L, 0x1F, 0x3F);
	}
    else
    {
        W2BYTEMSK(REG_ADC_ATOP_42_L, 0x1 << 6, (BIT(7)|BIT(6)));
        W2BYTEMSK(REG_ADC_ATOP_43_L, 0x00, 0x3F);
    }
    return TRUE;
}

MS_BOOL Hal_XC_S_ADC_Gain_SET(void *pInstance,APIXC_AdcGainOffsetSetting *stBuf)
{
    Hal_ADC_SetGainR(pInstance, stBuf->u16RedGain);
    Hal_ADC_SetGainG(pInstance, stBuf->u16GreenGain);
    Hal_ADC_SetGainB(pInstance, stBuf->u16BlueGain);
    return TRUE;
}

MS_BOOL Hal_XC_S_ADC_Gain_GET(void *pInstance,APIXC_AdcGainOffsetSetting *stBuf)
{
    stBuf->u16RedGain= Hal_ADC_GetGainR(pInstance);
    stBuf->u16GreenGain = Hal_ADC_GetGainG(pInstance);
    stBuf->u16BlueGain = Hal_ADC_GetGainB(pInstance);
    return TRUE;
}

MS_BOOL Hal_XC_S_ADC_Offset_SET(void *pInstance,APIXC_AdcGainOffsetSetting *stBuf)
{
    Hal_ADC_SetOffsetR(pInstance, stBuf->u16RedOffset);
    Hal_ADC_SetOffsetG(pInstance, stBuf->u16GreenOffset);
    Hal_ADC_SetOffsetB(pInstance, stBuf->u16BlueOffset);
    return TRUE;
}

MS_BOOL Hal_XC_S_ADC_Offset_GET(void *pInstance,APIXC_AdcGainOffsetSetting *stBuf)
{
    stBuf->u16RedOffset = Hal_ADC_GetOffsetR(pInstance);
    stBuf->u16GreenOffset = Hal_ADC_GetOffsetG(pInstance);
    stBuf->u16BlueOffset = Hal_ADC_GetOffsetB(pInstance);
    return TRUE;
}

// HDMI
MS_BOOL Hal_XC_S_HDMI_HDE_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    stBuf->u16Param = R2BYTEMSK(REG_HDMI_65_L, 0x3FFF);
    return TRUE;
}

MS_BOOL Hal_XC_S_HDMI_HTT_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    stBuf->u16Param = R2BYTEMSK(REG_HDMI2_32_L, 0x3FFF);
    return TRUE;
}

MS_BOOL Hal_XC_S_HDMI_VDE_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    stBuf->u16Param = R2BYTEMSK(REG_HDMI2_31_L, 0x3FFF);
    return TRUE;
}

MS_BOOL Hal_XC_S_HDMI_VTT_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    stBuf->u16Param = 0;
    return TRUE;
}

MS_BOOL Hal_XC_S_HDMI_DDCChannel_EN(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->bParam)
    {
        switch(stBuf->enInputPortType)
        {
            case INPUT_PORT_DVI0://A
            {
                PM_W2BYTE(REG_DDC_4B_L, BIT(1)|BIT(5), BIT(1)|BIT(5)|BIT(7));
                break;
            }
            case INPUT_PORT_DVI1://B
            {
                PM_W2BYTE(REG_DDC_4B_L, BIT(9)|BIT(13), BIT(9)|BIT(13)|BIT(15));
                break;
            }
            case INPUT_PORT_DVI3://D
            {
                PM_W2BYTE(REG_DDC_4C_L, BIT(9)|BIT(13), BIT(9)|BIT(13)|BIT(15));
                break;
            }
            case INPUT_PORT_DVI2://C
            {
                PM_W2BYTE(REG_DDC_4C_L, BIT(1)|BIT(5), BIT(1)|BIT(5)|BIT(7));
                break;
            }
            default:
            {
                return FALSE;
            }
        }
    }
    else
    {
        switch(stBuf->enInputPortType)
        {
            case INPUT_PORT_DVI0://A
            {
                PM_W2BYTE(REG_DDC_4B_L, BIT(7), BIT(1)|BIT(5)|BIT(7));
                break;
            }
            case INPUT_PORT_DVI1://B
            {
                PM_W2BYTE(REG_DDC_4B_L, BIT(15), BIT(9)|BIT(13)|BIT(15));
                break;
            }
            case INPUT_PORT_DVI3://D
            {
                PM_W2BYTE(REG_DDC_4C_L, BIT(15), BIT(9)|BIT(13)|BIT(15));
                break;
            }
            case INPUT_PORT_DVI2://C
            {
                PM_W2BYTE(REG_DDC_4C_L, BIT(7), BIT(1)|BIT(5)|BIT(7));
                break;
            }
            default:
            {
                return FALSE;
            }
        }
    }
    return TRUE;
}

MS_BOOL Hal_XC_S_HDMI_AVmute_Blank(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->bParam)
    {
        W2BYTEMSK(REG_HDMI_06_L, BIT(11)|BIT(10) , BIT(11)|BIT(10));
    }
    else
    {
        W2BYTEMSK(REG_HDMI_06_L, 0 , BIT(11)|BIT(10));
    }
}

// PCMode
MS_BOOL Hal_XC_S_PCMode_VResolution_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MS_BOOL bRet = FALSE;
    if(stBuf->u32Param < RES_MAXIMUM)
    {
        stBuf->u16Param = astStandardModeResolution[stBuf->u32Param].u16DisplayHeight;
		bRet = TRUE;
    }
    return bRet;
}

MS_BOOL Hal_XC_S_PCMode_HResolution_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MS_BOOL bRet = FALSE;
    if(stBuf->u32Param < RES_MAXIMUM)
    {
        stBuf->u16Param = astStandardModeResolution[stBuf->u32Param].u16DisplayWidth;
		bRet = TRUE;
    }
    return bRet;
}

MS_BOOL Hal_XC_S_PCMode_Htotal_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    XC_PCMONITOR_HK_INFO stPCMonitorInfo;
    stPCMonitorInfo = MDrv_XC_PCMonitor_GetStatus(pInstance, stBuf->eWindow);
    if((stPCMonitorInfo.eCurrentState == E_XC_PCMONITOR_UNSTABLE) || (stPCMonitorInfo.eCurrentState == E_XC_PCMONITOR_STABLE_NOSYNC))
    {
        stBuf->u16Param = 0;
    }
    else
    {
        stBuf->u16Param = stPCMonitorInfo.u16Hperiod;
    }
    return TRUE;
}

// XC
MS_BOOL Hal_XC_S_XC_FrameBufferNumber_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
  //  void *pInstanceNew = pu32XCInst_private;
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));

    if(stBuf->eWindow == MAIN_WINDOW)
    {
        stBuf->u16Param = SC_R2BYTEMSK(psXCInstPri->u32DeviceID,REG_SC_BK12_19_L, 0x001F);
        //stBuf->u16Param = R2BYTEMSK(REG_SC_BK12_19_L, 0x001F);
    }
    else
    {
        stBuf->u16Param = SC_R2BYTEMSK(psXCInstPri->u32DeviceID,REG_SC_BK12_59_L, 0x001F);
        //stBuf->u16Param = R2BYTEMSK(REG_SC_BK12_59_L, 0x001F);
    }
    return TRUE;
}

MS_BOOL Hal_XC_S_XC_FrameBufferNumber_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->eWindow == MAIN_WINDOW)
    {
        W2BYTEMSK(REG_SC_BK12_19_L, stBuf->u16Param, 0x001F);
    }
	else
	{
        W2BYTEMSK(REG_SC_BK12_59_L, stBuf->u16Param, 0x001F);
	}
    return TRUE;
}

MS_BOOL Hal_XC_S_XC_RwPointDiff_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->eWindow == MAIN_WINDOW)
    {
        W2BYTEMSK(REG_SC_BK12_07_L, stBuf->u16Param, BIT(13)|BIT(14)|BIT(15));
    }
	else
	{
        W2BYTEMSK(REG_SC_BK12_47_L, stBuf->u16Param, BIT(13)|BIT(14)|BIT(15));
	}
    return TRUE;
}

MS_BOOL Hal_XC_S_XC_RwPointDiff_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->eWindow == MAIN_WINDOW)
    {
        stBuf->u16Param = R2BYTEMSK(REG_SC_BK12_07_L, BIT(13)|BIT(14)|BIT(15));
    }
	else
	{
        stBuf->u16Param = R2BYTEMSK(REG_SC_BK12_47_L, BIT(13)|BIT(14)|BIT(15));
	}
    return TRUE;
}

MS_BOOL Hal_XC_S_XC_OpwOff_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    stBuf->bParam = R2BYTEMSK(REG_SC_BK12_67_L, BIT(1))? TRUE: FALSE;
    return TRUE;
}

MS_BOOL Hal_XC_S_XC_OpwOff_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    W2BYTEMSK(REG_SC_BK12_67_L, stBuf->bParam? BIT(1): 0, BIT(1));
    return TRUE;
}

MS_BOOL Hal_XC_S_XC_InterlaceStatus_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MS_U8 reg_IP1F2_1D, reg_IP1F2_23;
    MS_U16 reg_IP1F2_21;

    if(stBuf->bParam)
    {
        reg_IP1F2_1D = 0xA1;
        reg_IP1F2_21 = 0x0403;
        reg_IP1F2_23 = 0x30;
    }
    else
    {
        reg_IP1F2_1D = 0x21;
        reg_IP1F2_21 = 0x0400;
        reg_IP1F2_23 = 0x00;
    }

    if( stBuf->eWindow == MAIN_WINDOW )
    {
        W2BYTEMSK(REG_SC_BK01_1D_L, (reg_IP1F2_1D<<8), 0xEF00);
        W2BYTEMSK(REG_SC_BK01_21_L, reg_IP1F2_21, 0x3FFF);
        W2BYTEMSK(REG_SC_BK01_23_L, (reg_IP1F2_23<<8), HBMASK);
    }
    else if(stBuf->eWindow == SUB_WINDOW)
    {
        W2BYTEMSK(REG_SC_BK03_1D_L, (reg_IP1F2_1D<<8), 0xEF00);
        W2BYTEMSK(REG_SC_BK03_21_L, reg_IP1F2_21, 0x3FFF);
        W2BYTEMSK(REG_SC_BK03_23_L, (reg_IP1F2_23<<8), HBMASK);
    }
    else if(stBuf->eWindow == OFFLINE_WINDOW)
    {
        W2BYTEMSK(REG_SC_BK13_1D_L, (reg_IP1F2_1D<<8), 0xEF00);
        W2BYTEMSK(REG_SC_BK13_21_L, reg_IP1F2_21, 0x3FFF);
        W2BYTEMSK(REG_SC_BK13_23_L, (reg_IP1F2_23<<8), HBMASK);
    }
	else
	{
	    return FALSE;
	}

    return TRUE;
}

MS_BOOL Hal_XC_S_XC_FieldDetect_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MS_U8 reg_IP1F2_1D, reg_IP1F2_23;
    MS_U16 reg_IP1F2_21;
    MS_U16 u16Vtt = 0;

    // Note:
    // IP1F2_1D[13]: Enable auto no signal filter mode.
    // This functionality is to improve timing detection stability.

    if(IsSrcTypeDigitalVD(stBuf->enInputSrcType))
    {
        reg_IP1F2_1D = 0xA1;
        reg_IP1F2_21 = 0x0403;
        reg_IP1F2_23 = 0x30;
    }
    else if(IsSrcTypeDTV(stBuf->enInputSrcType) || IsSrcTypeStorage(stBuf->enInputSrcType))
    {
        if ((EN_VIDEO_SCAN_TYPE)stBuf->u16Param == SCAN_INTERLACE)
        {
            reg_IP1F2_1D = 0xA1;
            reg_IP1F2_21 = 0x0403;
            reg_IP1F2_23 = 0x30;
        }
        else
        {
            reg_IP1F2_1D = 0x21;
            reg_IP1F2_21 = 0x0400;
            reg_IP1F2_23 = 0x00;
        }
    }
    else if (IsSrcTypeHDMI(stBuf->enInputSrcType))
    {
        if (((EN_VIDEO_SCAN_TYPE)stBuf->u16Param == SCAN_INTERLACE)
                || (MDrv_SC_GetInterlaceInPModeStatus(pInstance, stBuf->eWindow)
                    && (MDrv_XC_GetForceiSupportType(pInstance) == ForceI_SW)))
        {
            reg_IP1F2_1D = 0x21;
            reg_IP1F2_21 = 0x0003;      // enable DE -> no field invert, disable DE -> use field invert
            reg_IP1F2_23 = 0x20;

            u16Vtt = MDrv_SC_ip_get_verticaltotal(pInstance, stBuf->eWindow);
            if((u16Vtt/2)%2 )
            {
                reg_IP1F2_21 |= BIT(8);
            }
            else
            {
                reg_IP1F2_21 &= ~BIT(8);
            }
        }
        else
        {
            reg_IP1F2_1D = 0x21;
            reg_IP1F2_21 = 0x0000;
            reg_IP1F2_23 = 0x00;
        }

        reg_IP1F2_21 |= BIT(12);
        if( (MApi_XC_GetHdmiSyncMode() == HDMI_SYNC_HV)
                &&((EN_VIDEO_SCAN_TYPE)stBuf->u16Param == SCAN_INTERLACE) )
        {
            XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
            UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
            MS_U16 u16Cur_IP1F2_21Val = SC_R2BYTE(psXCInstPri->u32DeviceID, REG_SC_BK01_21_L);
            if( u16Cur_IP1F2_21Val&0xC000 )
            {
                if( reg_IP1F2_21&BIT(8) )
                {
                    reg_IP1F2_21 &= ~BIT(8);
                }
                else
                {
                    reg_IP1F2_21 |= BIT(8);
                }
                //printf("daniel_test patch REG_SC_BK01_21_L: FieldInvert\n");
            }
            else
            {
            }
        }

    }
    else if(IsSrcTypeVga(stBuf->enInputSrcType))
    {
        if ((EN_VIDEO_SCAN_TYPE)stBuf->u16Param == SCAN_INTERLACE)
        {
            reg_IP1F2_1D = 0x21;
            reg_IP1F2_21 = 0x0103;
            reg_IP1F2_23 = 0x10;
        }
        else
        {
            reg_IP1F2_1D = 0x21;
            reg_IP1F2_21 = 0x0000;
            reg_IP1F2_23 = 0x00;
        }
    }
    else if (IsSrcTypeCapture(stBuf->enInputSrcType))
    {
        reg_IP1F2_21 = 0x0101;//IP1 sometimes will detect opcapture to interlace,we force it to progressive
        reg_IP1F2_1D = 0xA1;
        reg_IP1F2_23 = 0x08; //0x20  // Vtt will use "IP1F2_23 * 16" as lower bound.
    }
    else // YPbPr
    {
        if(MDrv_XC_ip_GetEuroHdtvStatus(pInstance, MAIN_WINDOW) == EURO_AUS_HDTV_NORMAL)
        {
            reg_IP1F2_21 = 0x0100;
        }
        else
        {
            reg_IP1F2_21 = 0x0000;
            //reg_IP1F2_21 = 0x0100;
        }
        reg_IP1F2_1D = 0xA1;
        reg_IP1F2_23 = 0x08; //0x20  // Vtt will use "IP1F2_23 * 16" as lower bound.
    }

    if( stBuf->eWindow == MAIN_WINDOW )
    {
        W2BYTEMSK(REG_SC_BK01_1D_L, (reg_IP1F2_1D<<8), 0xEF00);
        W2BYTEMSK(REG_SC_BK01_21_L, reg_IP1F2_21, 0x3FFF);
        W2BYTEMSK(REG_SC_BK01_23_L, (reg_IP1F2_23<<8), HBMASK);
    }
    else if(stBuf->eWindow == SUB_WINDOW)
    {
        W2BYTEMSK(REG_SC_BK03_1D_L, (reg_IP1F2_1D<<8), 0xEF00);
        W2BYTEMSK(REG_SC_BK03_21_L, reg_IP1F2_21, 0x3FFF);
        W2BYTEMSK(REG_SC_BK03_23_L, (reg_IP1F2_23<<8), HBMASK);
    }
    else if(stBuf->eWindow == OFFLINE_WINDOW)
    {
        W2BYTEMSK(REG_SC_BK13_1D_L, (reg_IP1F2_1D<<8), 0xEF00);
        W2BYTEMSK(REG_SC_BK13_21_L, reg_IP1F2_21, 0x3FFF);
        W2BYTEMSK(REG_SC_BK13_23_L, (reg_IP1F2_23<<8), HBMASK);
    }
	else
	{
	    return FALSE;
	}

    return TRUE;
}

MS_BOOL Hal_XC_S_XC_DispDeWin_GET(void *pInstance,MS_WINDOW_TYPE *stBuf)
{
    stBuf->x = SC_R2BYTE(0,REG_SC_BK10_04_L);
    stBuf->y = SC_R2BYTE(0,REG_SC_BK10_06_L);
    stBuf->width = SC_R2BYTE(0,REG_SC_BK10_05_L) - stBuf->x + 1;
    stBuf->height = SC_R2BYTE(0,REG_SC_BK10_07_L) - stBuf->y + 1;

    return TRUE;
}

MS_BOOL Hal_XC_S_XC_Fast_Setwindow_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));
    gSrcInfo[stBuf->eWindow].Status2.bEnableFastSetwindow = stBuf->bParam;
    return TRUE;
}

void Hal_XC_S_XC_OPW_UnMask(void *pInstance)
{
#define SC_OPW_REQUEST_MASK 0x310686 //miu0 group6 client1
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK12_07_L, BIT(0)|BIT(1)	, BIT(0)|BIT(1) ); //ipm_rreq_off_f2, ipm_wreq_off_f2
    MDrv_Write2ByteMask(SC_OPW_REQUEST_MASK, 0, BIT(1));//MIU opm Unmask // miu0 group6 client1
}

static MS_BOOL bAPmute[MAX_WINDOW] = {0};
void Hal_XC_S_XC_Set_APmute_flag(void* pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    bAPmute[stBuf->eWindow] = stBuf->bParam;
}

MS_BOOL Hal_XC_S_XC_Get_APmute_flag(SCALER_WIN eWindow)
{
    return bAPmute[eWindow];
}

//PATTERN
MS_BOOL Hal_XC_S_PATTERN_MOD_SET(void* pInstance,MS_XC_CUS_PATTERN_PARA *stBuf)
{
    //MSG_S_PATTERN_CTRL(printf("%s %d: E_XC_S_PATTERN_MOD_SET\n", __FUNCTION__, __LINE__));
    switch(stBuf->u8TestMode)
    {
        case 0: //disable
            {
                MDrv_Write2ByteMask(0x103202, stBuf ->u8TestMode , BIT(15));
                MDrv_Write2ByteMask(0x10329E, stBuf->u8TestMode  , BIT(15));
                break;
            }
        case 1: // full screen
        {
            MDrv_Write2ByteMask(0x103204, stBuf->u16R , 0x03FF);  // red part bit
            MDrv_Write2ByteMask(0x103206, stBuf->u16G, 0x03FF);  // green part bit
            MDrv_Write2ByteMask(0x103208, stBuf->u16B , 0x03FF);  // blue part bit
            MDrv_Write2ByteMask(0x103202, (stBuf ->u8TestMode)? BIT(15):0 , BIT(15));
            break;
        }
        case 2: // partial
        {
            MDrv_Write2ByteMask(0x103204, stBuf->u16R , 0x03FF);  // red part bit
            MDrv_Write2ByteMask(0x103206, stBuf->u16G, 0x03FF);  // green part bit
            MDrv_Write2ByteMask(0x103208, stBuf->u16B , 0x03FF);  // blue part bit
            MDrv_Write2ByteMask(0x10329E, ((stBuf->u16HStart) | BIT(15))  , 0x9FFF);
            MDrv_Write2ByteMask(0x1032A0,( stBuf->u16HEnd), 0x1FFF);
            break;
        }
        default:
        {
            break;
        }
    }
    return TRUE;
}

MS_BOOL Hal_XC_S_PATTERN_IP1_RGB_SET(void* pInstance,MS_XC_CUS_PATTERN_PARA *stBuf)
{
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));

    if(gSrcInfo[MAIN_WINDOW].bBlackscreenEnabled)
    {
        if(stBuf->bEnable)
        {
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_40_L, BIT(0)|BIT(6)|BIT(11)|BIT(12), BIT(0)|BIT(6)|BIT(11)|BIT(12));
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_42_L, stBuf->u16R| stBuf->u16G, 0xFFFF);
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_43_L, stBuf->u16B, 0x00FF);
        }
        else
        {
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_42_L, 0, 0xFFFF);
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_43_L, 0, 0x00FF);
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_40_L, 0, BIT(0)|BIT(6)|BIT(11)|BIT(12) );
        }
    }
    else
    {
#if 1
        MLOAD_TYPE eMloadStatus = MDrv_XC_MLoad_GetStatus(pInstance);
        if(eMloadStatus != E_MLOAD_ENABLED)
        {
            MApi_XC_MLoad_Enable(TRUE);
        }

        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK10_2F_L, stBuf->u16CSC,0x0010);//enable color matrix
        if (stBuf->bEnable)
        {
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_40_L, BIT(0)|BIT(6)|BIT(11)|BIT(12), BIT(0)|BIT(6)|BIT(11)|BIT(12) );
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_42_L, stBuf->u16R|stBuf->u16G, 0xFFFF);
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_43_L, stBuf->u16B, 0x00FF);
        }
        else
        {
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_42_L, 0, 0xFFFF);
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_43_L, 0, 0x00FF);
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_40_L, 0, BIT(0)|BIT(6)|BIT(11)|BIT(12) );
        }

        if (!MDrv_XC_MLoad_Fire(pInstance, TRUE))
        {
                printf("[%d][%s]Error: Set MLoad Fire Error!!!!\n ", __LINE__, __FUNCTION__);
        }

        if(eMloadStatus != E_MLOAD_ENABLED)
        {
            MApi_XC_MLoad_Enable(FALSE);
        }
#else
        MDrv_XC_MLoad_set_trigger_sync(pInstance, MLOAD_TRIGGER_BY_IP_MAIN_SYNC);
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK10_2F_L, stBuf->u16CSC,0x0010);//enable color matrix
        if (stBuf->bEnable)
        {
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_40_L, BIT(0)|BIT(6)|BIT(11)|BIT(12), BIT(0)|BIT(6)|BIT(11)|BIT(12) );
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_42_L, stBuf->u16R|stBuf->u16R, 0xFFFF);
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_43_L, stBuf->u16B, 0x00FF);
        }
        else
        {
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_42_L, 0, 0xFFFF);
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_43_L, 0, 0x00FF);
            MDrv_XC_MLoad_WriteCmd(pInstance,REG_SC_BK01_40_L, 0, BIT(0)|BIT(6)|BIT(11)|BIT(12) );
        }
        if (!MDrv_XC_MLoad_Fire(pInstance, TRUE))
        {
                printf("[%d][%s]Error: Set MLoad Fire Error!!!!\n ", __LINE__, __FUNCTION__);
        }

        MDrv_XC_MLoad_set_trigger_sync(pInstance, MLOAD_TRIGGER_BY_OP_SYNC);
#endif
    }

    return TRUE;
}
void _Hal_XC_S_Pattern_IP1_Video_Latency_ISR(SC_INT_SRC eIntNum, void * pParam)
{
    void *pInstance = pu32XCInst_private;
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));

    MS_U8 u8PatternColor_R = 0;
    MS_U8 u8PatternColor_G = 0;
    MS_U8 u8PatternColor_B = 0;
    static MS_BOOL bPreEnable = FALSE;
    static MS_BOOL bPreColorFMTYUV = FALSE;
    static E_XC_VSC_PATTERN_TYPE_T ePreVscPatternType = E_XC_VSC_PATTERN_WHITE;

    if((gstPatternPara.bEnable != bPreEnable)
        || (gstPatternPara.bColorFMTYUV != bPreColorFMTYUV)
        || (gstPatternPara.eVscPatternType != ePreVscPatternType))
    {

        if (gstPatternPara.bEnable)
        {
            if(gstPatternPara.bColorFMTYUV)
            {
                u8PatternColor_R = 0x80;
                u8PatternColor_G = (gstPatternPara.eVscPatternType==E_XC_VSC_PATTERN_WHITE) ? 0xEB : 0x10;
                u8PatternColor_B = 0x80;
            }
            else
            {
                u8PatternColor_R = (gstPatternPara.eVscPatternType==E_XC_VSC_PATTERN_WHITE) ? 0xFF : 0x00;
                u8PatternColor_G = (gstPatternPara.eVscPatternType==E_XC_VSC_PATTERN_WHITE) ? 0xFF : 0x00;
                u8PatternColor_B = (gstPatternPara.eVscPatternType==E_XC_VSC_PATTERN_WHITE) ? 0xFF : 0x00;
            }

            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_40_L, BIT(0)|BIT(6)|BIT(11)|BIT(12), BIT(0)|BIT(6)|BIT(11)|BIT(12));
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_42_L, u8PatternColor_R | (u8PatternColor_G << 8), 0xFFFF);
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_43_L, u8PatternColor_B, 0x00FF);
            //GPIO PAD_GPIO7_PM, output mode
            PM_W2BYTE(REG_PM_GPIO_07_L, 0, BIT(0));
            PM_W2BYTE(REG_PM_GPIO_07_L, (gstPatternPara.eVscPatternType==E_XC_VSC_PATTERN_WHITE) ?  BIT(1) : 0, BIT(1));
        }
        else
        {
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_42_L, 0, 0xFFFF);
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_43_L, 0, 0x00FF);
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK01_40_L, 0, BIT(0)|BIT(6)|BIT(11)|BIT(12));
            PM_W2BYTE(REG_PM_GPIO_07_L, BIT(0), BIT(0));
            //GPIO PAD_GPIO7_PM, input mode
            PM_W2BYTE(REG_PM_GPIO_07_L, 0, BIT(1));
            MDrv_XC_InterruptDeAttachWithoutMutex(pInstance, SC_INT_F2_IPVS_SB, _Hal_XC_S_Pattern_IP1_Video_Latency_ISR, (void *)NULL);
        }
        bPreEnable = gstPatternPara.bEnable;
        bPreColorFMTYUV = gstPatternPara.bColorFMTYUV;
        ePreVscPatternType = gstPatternPara.eVscPatternType;
    }

}

MS_BOOL Hal_XC_S_Pattern_IP1_Video_Latency(void* pInstance, MS_XC_CUS_PATTERN_VIDEO_LATENCY *stPatternPara)
{
    gstPatternPara.bEnable = stPatternPara->bEnable;
    gstPatternPara.bColorFMTYUV = stPatternPara->bColorFMTYUV;
    gstPatternPara.eVscPatternType = stPatternPara->eVscPatternType;
    gstPatternPara.eWindow = stPatternPara->eWindow;
    gstPatternPara.stOverlapWindow = stPatternPara->stOverlapWindow;
    MS_U32 u32CurTime = MsOS_GetSystemTime();
    MS_BOOL bReturn = FALSE;

    while(MsOS_GetSystemTime() - u32CurTime < 50)
    {
        if(TRUE == MDrv_XC_InterruptIsAttached(pInstance, SC_INT_F2_IPVS_SB, _Hal_XC_S_Pattern_IP1_Video_Latency_ISR, (void *)NULL))
        {
            MsOS_DelayTask(5);
        }
        else
        {
            MDrv_XC_InterruptAttachWithoutMutex(pInstance, SC_INT_F2_IPVS_SB, _Hal_XC_S_Pattern_IP1_Video_Latency_ISR, (void *)NULL);
            bReturn = TRUE;
            break;
        }
        bReturn = FALSE;
    }

    if(bReturn == FALSE)
    {
        printf("Wait input Vsync Time out! [%s()][%d]\n",__func__,__LINE__);
    }
    return bReturn;
}


// PIP / Smart Zoom
MS_BOOL Hal_XC_S_PIP_SmartZoom_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	bIsSmartZoomOn = stBuf->bParam;
    return TRUE;
}

MS_BOOL Hal_XC_S_PIP_SmartZoom_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	stBuf->bParam = bIsSmartZoomOn;
    return TRUE;
}

// 3D
MS_BOOL Hal_XC_S_3D_KR3DMode_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	bIsKR3DMode = stBuf->bParam;
    return TRUE;
}

MS_BOOL Hal_XC_S_3D_KR3DMode_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    stBuf->bParam = bIsKR3DMode;
    return TRUE;
}

// PVR
MS_BOOL Hal_XC_S_PVR_DualWinForAPVR_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    bDualWinForAPVR = stBuf->bParam;
    return TRUE;
}

MS_BOOL Hal_XC_S_PVR_DualWinForAPVR_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    stBuf->bParam = bDualWinForAPVR;
    stBuf->u16Param = u16ForceSC2OP;
    return TRUE;
}

MS_BOOL Hal_XC_S_PVR_SC2_OPTiming(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    static MS_U16 u16Sc0ClkRef = 0;
    u16ForceSC2OP = (stBuf->bParam==ENABLE)? 1: 0;

    if(stBuf->bParam)
    {
        u16Sc0ClkRef = SC_R2BYTE(0, REG_SC_BK68_62_L);
        SC_W2BYTEMSK(0, REG_SC_BK68_62_L, 0x1FFF, 0x1FFF);
        SC_W2BYTEMSK(1, REG_SC_BK10_0D_L, (stBuf->u16Param-1), 0xFFFF);
    }
    else
    {
        SC_W2BYTEMSK(0, REG_SC_BK68_62_L, u16Sc0ClkRef, 0x1FFF);
        SC_W2BYTEMSK(1, REG_SC_BK10_0D_L, 0x1FFF, 0xFFFF);
    }
    return TRUE;
}


// PQ
extern void msAdjustHSC(void *pInstance, MS_BOOL bScalerWin, MS_U8 ucHue, MS_U8 ucSaturation, MS_U8 ucContrast);
extern void    msAdjustPCContrast(void* pInstance, MS_BOOL bScalerWin, MS_U8 u8Contrast );
MS_BOOL Hal_XC_S_PQ_PicSetHSC_SET(void *pInstance,MS_XC_CUS_PQ_PARA *stBuf)
{
#if 1
    if(stBuf->bUseYUVSpace)
    {
        msAdjustHSC(pInstance, stBuf->eWindow, stBuf->u8Hue, stBuf->u8Saturation, stBuf->u8Contrast);
    }
    else
    {
        msAdjustPCContrast(pInstance, stBuf->eWindow, stBuf->u8Contrast);
    }
#endif
    return TRUE;
}

MS_BOOL Hal_XC_S_PQ_GetUC_Patch_Status(void *pInstance, MS_XC_CUS_PQ_GET_UC_PATCH_STATUS *stBuf)
{
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));

    stBuf->bIsUCPatchOn = pXCResourcePrivate->stdrvXC_MVideo_Context.g_bIsUCPatchOn;
    return TRUE;
}

#define ALL_UCPATCH_KEY 0xF
extern MS_U16 _u16PQDbgSwitch;
MS_BOOL Hal_XC_S_PQ_SET_UC_DISABLE(void *pInstance, MS_XC_CUS_PQ_SET_UC_DISABLE *stBuf)
{
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));
    static MS_BOOL _bEnable = FALSE;// = stBuf->bEnable;
    static MS_U8 _u8Key = 0;// = stBuf->u8Key;

    if(stBuf->bEnable)
    {
        _u8Key |= stBuf->u8Key;
    }
    else
    {
        _u8Key &= ~(stBuf->u8Key);
    }

    if(_u8Key == 0x00)
    {
        _bEnable = FALSE;
    }
    else
    {
        _bEnable = TRUE;
    }

    PQMADi_DBG(printf("[%s][%d] stBuf->bEnable[%d] stBuf->u8Key[%d]\n", __func__, __LINE__, stBuf->bEnable, stBuf->u8Key));

    if(stBuf->bEnable==FALSE && stBuf->u8Key==ALL_UCPATCH_KEY)
    {
        pXCResourcePrivate->stdrvXC_MVideo_Context.g_bIsUCPatchOn = FALSE;
        SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK20_3C_L, 0x0, 0x0001);
        return TRUE;
    }

    if(pXCResourcePrivate->stdrvXC_MVideo_Context.g_bIsUCPatchOn == _bEnable)
    {
        return FALSE;
    }

    pXCResourcePrivate->stdrvXC_MVideo_Context.g_bIsUCPatchOn = stBuf->bEnable;//_bEnable;

    //mbrg_XC_SetWinFreeze(bFreeze, Mbrg_MAIN_WINDOW);

    MS_XC_CUS_CTRL_PARA stApvrStatus;
    memset(&stApvrStatus, 0, sizeof(MS_XC_CUS_CTRL_PARA));
    Hal_XC_S_PVR_DualWinForAPVR_GET(pInstance, &stApvrStatus);
    MS_BOOL bIsApvrRecording = (stApvrStatus.u16Param==1) ? TRUE : FALSE;

    if((stBuf->bEnable == TRUE) || (MDrv_XC_Is_DSForceIndexEnabled(pInstance,MAIN_WINDOW)| MDrv_XC_GetDynamicScalingStatus(pInstance)))
    {
#if 0
        // Store register value
        _u16PQdriver_status = SC_R2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK22_7C_L,0xFFFF);
        _u16MADI_SST_status = SC_R2BYTEMSK(psXCInstPri->u32DeviceID,REG_SC_BK22_18_L, BIT7);
        _u16DIPF_status = SC_R2BYTEMSK(psXCInstPri->u32DeviceID,REG_SC_BK22_14_L, BIT7);
        _u16film32_status = SC_R2BYTEMSK(psXCInstPri->u32DeviceID,REG_SC_BK0A_10_L,BIT15);
        _u16film22_status = SC_R2BYTEMSK(psXCInstPri->u32DeviceID,REG_SC_BK0A_10_L, BIT14);
        _u16filmAny_status = SC_R2BYTEMSK(psXCInstPri->u32DeviceID,REG_SC_BK0A_24_L, BIT15);

        _MLOAD_ENTRY(pInstance);

        // OPW disable
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK12_67_L, _bEnable , BIT(1));
        // PQ driver off
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK22_7C_L,0, 0xFFFF);
        // MADI SST off
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK22_18_L,0, BIT7);
        // DIPF enable
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK22_14_L,BIT7, BIT7);
        // film
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK0A_10_L,0, BIT15);
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK0A_10_L,0, BIT14);
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK0A_24_L,0, BIT15);
        MDrv_XC_MLoad_Fire(pInstance, TRUE);
        _MLOAD_RETURN(pInstance);
#endif
        MDrv_PQ_DisableUCFeature(PQ_MAIN_WINDOW);

        SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK22_78_L, 0x008F, 0xFFFF);
        SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK20_3C_L, 0x0001, 0x0001);

        if(bIsApvrRecording)
        {
            PQMADi_DBG(printf("skip sub window bob mode control in APVR case %s,%d\n",__FUNCTION__,__LINE__));
        }
        else
        {
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BKA2_78_L, 0x008F, 0xFFFF);
        }
    }
    else
    {
#if 0
        _MLOAD_ENTRY(pInstance);

        // OPW turn on
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK12_67_L, _bEnable , BIT(1));
        // PQ driver
         MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK22_7C_L,_u16PQdriver_status, 0xFFFF);
        // MADI SST recover
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK22_18_L,_u16MADI_SST_status, BIT7);
        // DIPF recover
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK22_14_L,_u16DIPF_status, BIT7);
        //film
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK0A_10_L,_u16film32_status, BIT15);
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK0A_10_L,_u16film22_status, BIT14);
        MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK0A_24_L,_u16filmAny_status, BIT15);
        MDrv_XC_MLoad_Fire(pInstance, TRUE);
        _MLOAD_RETURN(pInstance);
#endif
        SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK22_78_L, 0x0, 0xFFFF);
        SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK20_3C_L, 0x0, 0x0001);

        if(bIsApvrRecording)
        {
            PQMADi_DBG(printf("skip sub window bob mode control in APVR case %s,%d\n",__FUNCTION__,__LINE__));
        }
        else
        {
            SC_W2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BKA2_78_L, 0x0, 0xFFFF);
        }

        if(MDrv_PQ_GetGameMode_Status(PQ_MAIN_WINDOW) == FALSE)
            MDrv_PQ_EnableUCFeature(PQ_MAIN_WINDOW);
    }
    return TRUE;
}
#include "mdrv_xc_io.h"
#if (defined MSOS_TYPE_LINUX) || (defined ANDROID)
#include <sys/ioctl.h>
#include <fcntl.h> // O_RDWR
static MS_S32   _s32FdScaler = -1;
#endif

extern MS_BOOL KDrv_XC_SetDeFlickerControl(ST_KDRV_PQ_DEFLICKER *pstDeFlickerControl);
MS_BOOL Hal_XC_S_PQ_Set_DeFlicker_Control(void *pInstance, MS_XC_CUS_PQ_DEFLICKER *stBuf)
{
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));

    ST_KDRV_PQ_DEFLICKER stPQDeFlicker;
    memset(&stPQDeFlicker, 0, sizeof(ST_KDRV_PQ_DEFLICKER));

    stPQDeFlicker.u8Vid = stBuf->u8Vid;
    stPQDeFlicker.u8DeflickerEn = stBuf->u8DeflickerEn;
    stPQDeFlicker.u8SnrMotionEn = stBuf->u8SnrMotionEn;
    stPQDeFlicker.u8DefaultSnrGain = stBuf->u8DefaultSnrGain;
    stPQDeFlicker.u8DeflickerStr = stBuf->u8DeflickerStr;
    stPQDeFlicker.u8HiCplxStr = stBuf->u8HiCplxStr;

#if (defined MSOS_TYPE_LINUX) || (defined ANDROID)
    if(0 > _s32FdScaler)
    {
        _s32FdScaler = open("/dev/scaler", O_RDWR);

        if(0 > _s32FdScaler)
        {
            printf("Scaler device not opened!!!!\n");
            return E_APIXC_RET_FAIL;
        }
    }

    if (ioctl(_s32FdScaler, MDRV_XC_IOC_SET_DEFLICKER_CONTROL, &stPQDeFlicker))
    {
        printf("MDRV_XC_IOC_SET_DEFLICKER_CONTROL fail!!!!\n");
        return E_APIXC_RET_FAIL;
    }
#endif
#if defined(MSOS_TYPE_LINUX_KERNEL)
    if (KDrv_XC_SetDeFlickerControl(&stPQDeFlicker) != TRUE)
    {
        return E_APIXC_RET_FAIL;
    }
#endif
    return TRUE;
}

// DS
MS_BOOL Hal_XC_S_DS_Status_GET(void *pInstance,DSLOAD_TYPE *stBuf)
{
    if (!MHAL_SC_Get_DynamicScaling_Status(pInstance))
    {
        *stBuf = E_DSLOAD_DISABLED;
    }
	else
	{
        *stBuf = E_DSLOAD_ENABLED_HWMODE;
	}

    return TRUE;
}

MS_BOOL Hal_XC_S_DS_GST_PROCESS_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	bIsGstreamerProcess = stBuf->bParam;
    return TRUE;
}

MS_BOOL Hal_XC_S_DS_GST_PROCESS_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	stBuf->bParam = bIsGstreamerProcess;
    return TRUE;
}

MS_BOOL Hal_XC_S_DS_CAPTURE_PROCESS_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	bIsCaptureProcess = stBuf->bParam;
    return TRUE;
}

MS_BOOL Hal_XC_S_DS_CAPTURE_PROCESS_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	stBuf->bParam = bIsCaptureProcess;
    return TRUE;
}

MS_BOOL Hal_XC_S_DS_SET_ForceP_Flag(void *pInstance,MS_XC_CUS_FORCE_P_STATUS *stBuf)
{
	XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));

    pXCResourcePrivate->stdrvXC_MVideo_Context.g_bIsForcePEnabled = stBuf->bIsForceP;
    return TRUE;
}

MS_BOOL Hal_XC_S_DS_GET_ForceP_Flag(void *pInstance,MS_XC_CUS_FORCE_P_STATUS *stBuf)
{
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));

    stBuf->bIsForceP = pXCResourcePrivate->stdrvXC_MVideo_Context.g_bIsForcePEnabled;
    return TRUE;
}

// PNL
MS_BOOL Hal_XC_S_PNL_STR_EN(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	bSTROn = stBuf->bParam;
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_OutPECurrent_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    return MHal_PNL_MOD_Control_Out_PE_Current(pInstance, stBuf->u16Param);
}

MS_BOOL Hal_XC_S_PNL_PanelHStart_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	stBuf->u16Param = g_IPanel.HStart();
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_PanelVStart_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	stBuf->u16Param = g_IPanel.VStart();
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_PanelWidth_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	stBuf->u16Param = g_IPanel.Width();
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_PanelHeight_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
	stBuf->u16Param = g_IPanel.Height();
    return TRUE;
}

////Tcon
#define Panel_type_EPI 5
#define Panel_type_CEDS 8

MS_BOOL Hal_XC_S_PNL_Tcon_SetBOERGBWBypass(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MS_U8 u8PanelType = ((MDrv_Read2Byte(0x13F2E6) & 0x00F0)>>4); // EPI is 5, CEDS is 8
    if(u8PanelType == Panel_type_CEDS)
    {
        MS_BOOL bRGBWBypass = (R2BYTE(0x332602) & 0x01);

        if(stBuf->bParam)
        {
            MDrv_Write2ByteMask(0x332602, BIT(0) , BIT(0));
        }
        else
        {
            MDrv_Write2ByteMask(0x332602, 0 , BIT(0));
        }

        if(bRGBWBypass != stBuf->bParam)
        {
            MsOS_DelayTask(20);
        }
    }
    else
    {
        printf("[%s()][%d], [Warning]It's not BOE panel, skip!\n",__func__,__LINE__);
    }
    return TRUE;
}


MS_BOOL Hal_XC_S_PNL_Tcon_WPR_ON(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->bParam)
    {
        MDrv_Write2ByteMask(0x330302, BIT(15) , BIT(15));
    }
    else
    {
        MDrv_Write2ByteMask(0x330302, 0 , BIT(15));
    }
    MsOS_DelayTask(20);
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_Tcon_OPC_ON(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->bParam)
    {
        MDrv_Write2ByteMask(0x330302, BIT(14) , BIT(14));
    }
    else
    {
        MDrv_Write2ByteMask(0x330302, 0 , BIT(14));
    }
    MsOS_DelayTask(20);
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_Tcon_WSE_ON(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->bParam)
    {
        MDrv_Write2ByteMask(0x330302, BIT(13) , BIT(13));
    }
    else
    {
        MDrv_Write2ByteMask(0x330302, 0 , BIT(13));
    }
    MsOS_DelayTask(20);
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_Tcon_Mode_Sel(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{

    if(stBuf->u16Param > 0x03)
    {
        printf("\33[0;35m: [%s][%d], Error Mode Sel!!!!! ModeSel= %u\33[m \n",__FUNCTION__,__LINE__,stBuf->u16Param);
        return FALSE;
    }

    if(stBuf->u16Param)
    {
        MDrv_Write2ByteMask(0x330302, stBuf->u16Param , 0x03);
    }
    else
    {
        MDrv_Write2ByteMask(0x330302, stBuf->u16Param , 0x03);
    }
    MsOS_DelayTask(20);
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_Get_Tcon_Mode(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    stBuf->u16Param=MDrv_Read2Byte(0x330302);
    stBuf->u16Param= stBuf->u16Param&0x3;
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_Tcon_SET_FrameGainLimit(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{

    /// Set FrameGainLimit
    MDrv_WriteByteMask(0x330162, (stBuf->u16Param & 0xFF) , 0xFF);
    MDrv_WriteByteMask(0x330161, ((stBuf->u16Param >> 8) & 0xFF) , 0xFF);
    //MDrv_Write2ByteMask(0x330161, stBuf->u16Param , 0xFFFF);

    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_Tcon_GET_FrameGainLimit(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    /// Get FrameGainLimit
    stBuf->u16Param =(MDrv_ReadByte(0x330161)<<8) | (MDrv_ReadByte(0x330162) ) ;
    //stBuf->u16Param=MDrv_Read2Byte(0x330161);


    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_Tcon_SET_PixelGainLimit(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{

    /// Set PixelGainLimit
    MDrv_WriteByteMask(0x33017D, (stBuf->u16Param & 0xFF) , 0xFF);
    MDrv_WriteByteMask(0x33017C, ((stBuf->u16Param >> 8) & 0xFF) , 0xFF);
    //MDrv_Write2ByteMask(0x33017C, stBuf->u16Param , 0xFFFF);
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_Tcon_GET_PixelGainLimit(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    /// Get PixelGainLimit
    stBuf->u16Param =(MDrv_ReadByte(0x33017C)<<8) | MDrv_ReadByte(0x33017D);
    //stBuf->u16Param = MDrv_Read2Byte(0x33017C);
    return TRUE;
}
#define FCIC0_MaxNum 253
#define FCIC1_MaxNum 507
#define FCIC2_MaxNum 661
MS_BOOL Hal_XC_S_PNL_TSCIC_ControlTbl(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MS_U8* pControlTbl = (MS_U8*)stBuf->pParam ;
    MS_U32 u32ControlTblSize = stBuf->u32Param;
    if(u32ControlTblSize > (FCIC2_MaxNum + 1) )
    {
        u32ControlTblSize = FCIC2_MaxNum + 1;
    }
    MS_U32 u32TblIndex = 0;
    MS_U16 u16offset = 0;
    for(u32TblIndex=0;u32TblIndex<u32ControlTblSize;u32TblIndex++)
    {
        if(u32TblIndex <= FCIC0_MaxNum)
        {
            u16offset = u32TblIndex;
            MDrv_WriteByteMask(0x330702+u16offset, pControlTbl[u32TblIndex] , 0xFF);
        }
        else if(u32TblIndex <= FCIC1_MaxNum)
        {
            u16offset = (u32TblIndex -FCIC0_MaxNum)-1;
            MDrv_WriteByteMask(0x330802+u16offset, pControlTbl[u32TblIndex] , 0xFF);
        }
        else if(u32TblIndex <= FCIC2_MaxNum)
        {
            u16offset = (u32TblIndex -FCIC1_MaxNum)-1;
            MDrv_WriteByteMask(0x330902+u16offset, pControlTbl[u32TblIndex] , 0xFF);
        }
    }
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_TSCIC_TSCIClTbl(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MS_U32 u32_NonCacheAddr = stBuf->u32Param;
    MS_U16 u16Tscicsize = stBuf->u16Param;
    MDrv_BDMA_CopyHnd (u32_NonCacheAddr, 0x0, u16Tscicsize*sizeof(MS_U32), E_BDMA_SDRAM2FCIC, 0);
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_GET_MPLUS_DIMMING_DUTY(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    stBuf->u16Param=MDrv_Read2Byte(0x330310);
    stBuf->u16Param = stBuf->u16Param&0x1FFF;
    return TRUE;
}

#define PWM_CH_MAX 10
MS_U32 u32PWM_DUTY_fromAP[PWM_CH_MAX] = {0};
MS_U32 u32PWM_DUTY_VALUE_fromAP[PWM_CH_MAX] = {0};
MS_U32 u32PWM_SHIFT_fromAP[PWM_CH_MAX] = {0};
MS_U32 u32PWM_SHIFT_VALUE_fromAP[PWM_CH_MAX] = {0};
MS_BOOL bPWM_MPLUSDUTY_ISR_STATUS = FALSE;

MS_BOOL Hal_XC_S_PNL_SET_PWM_DUTY_fromAP(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->u16Param < PWM_CH_MAX)
    {
        u32PWM_DUTY_fromAP[stBuf->u16Param] = stBuf->u32Param;
        u32PWM_DUTY_VALUE_fromAP[stBuf->u16Param] = stBuf->u16Param1;
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

MS_BOOL Hal_XC_S_PNL_GET_PWM_DUTY_fromAP(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->u16Param < PWM_CH_MAX)
    {
        stBuf->u32Param = u32PWM_DUTY_fromAP[stBuf->u16Param];
        stBuf->u16Param1 = u32PWM_DUTY_VALUE_fromAP[stBuf->u16Param];
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

MS_BOOL Hal_XC_S_PNL_SET_PWM_SHIFT_fromAP(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->u16Param < PWM_CH_MAX)
    {
        u32PWM_SHIFT_fromAP[stBuf->u16Param] = stBuf->u32Param;
        u32PWM_SHIFT_VALUE_fromAP[stBuf->u16Param] = stBuf->u16Param1;
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

MS_BOOL Hal_XC_S_PNL_GET_PWM_SHIFT_fromAP(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    if(stBuf->u16Param < PWM_CH_MAX)
    {
        stBuf->u32Param = u32PWM_SHIFT_fromAP[stBuf->u16Param];
        stBuf->u16Param1 = u32PWM_SHIFT_VALUE_fromAP[stBuf->u16Param];
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

MS_BOOL Hal_XC_S_PNL_GET_MPLUSDUTY_ISR_STATUS(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
        return bPWM_MPLUSDUTY_ISR_STATUS;
}

MS_BOOL Hal_XC_S_PNL_GET_MPLUS_STATUS(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)//0:RGB, 1:RGBW
{
    MS_U8 u8PanelType = ((MDrv_Read2Byte(0x13F2E6) & 0x00F0)>>4); // EPI is 5, CEDS is 8
    MS_BOOL bEpiRgbEn = (((MDrv_Read2Byte(0x330302) & 0x20)>>5) ? (DISABLE) : (ENABLE)); // (16bit)Bank 0x3303_0x01[5]
    MS_BOOL bCedsRgbEn = ((MDrv_Read2Byte(0x332602) & 0x01) ? (DISABLE) : (ENABLE)); // (16bit)Bank 0x3326_0x01[0]
    if(u8PanelType==5)//EPI
    {
        stBuf->bParam = bEpiRgbEn;
        return TRUE;
    }
    else if(u8PanelType==8)//CEDS
    {
        stBuf->bParam = bCedsRgbEn;
        return TRUE;
    }
    else
    {
        stBuf->bParam = DISABLE;
        return TRUE;
    }

}
#if 0
void _Hal_XC_S_PWM_VDBen_SW(MS_U8 index, MS_BOOL bSwitch)
 {
	if(1)
	{
		//printf("%s(0x%08X, %x)", __FUNCTION__, (int)index, bSwitch);
		switch(index)
    	        {
        	case E_XC_PWM_CH0:
            	             MDrv_Write2ByteMask(0x13F408,BITS(14:14,bSwitch),BMASK(14:14));
            	break;
        	case E_XC_PWM_CH1:
				MDrv_Write2ByteMask(0x13F40E,BITS(14:14,bSwitch),BMASK(14:14));
            	break;
        	case E_XC_PWM_CH2:
				MDrv_Write2ByteMask(0x13F414,BITS(14:14,bSwitch),BMASK(14:14));
            	break;
        	case E_XC_PWM_CH3:
				MDrv_Write2ByteMask(0x13F41A,BITS(14:14,bSwitch),BMASK(14:14));
            	break;
        	case E_XC_PWM_CH4:
				MDrv_Write2ByteMask(0x13F420,BITS(14:14,bSwitch),BMASK(14:14));
            	break;
        	case E_XC_PWM_CH5:
        	case E_XC_PWM_CH6:
        	case E_XC_PWM_CH7:
        	case E_XC_PWM_CH8:
        	case E_XC_PWM_CH9:
				printf("[Utopia] The PWM%d is not support\n", (int)index);
            	UNUSED(bSwitch);
            	break;
			default:
				break;
		}
	}
}

void _Hal_XC_S_PNL_Shift(MS_U8 index, MS_U32 u32ShiftPWM)
{
    MS_U16 Shift_L, Shift_H;

    Shift_L = (MS_U16)(u32ShiftPWM & 0xFFFF);
    Shift_H = (MS_U16)(u32ShiftPWM >> 16);
    //HAL_SUBBANK1;
    _Hal_XC_S_PWM_VDBen_SW(index,0);
    /* the Duty capability is restricted to ONLY 10-bit */
    switch(index)
    {
        case E_XC_PWM_CH0:
            MDrv_Write2Byte(0x13F450, Shift_L);
            MDrv_Write2ByteMask(0x13F452,BITS(1:0,Shift_H),BMASK(1:0));
            break;
        case E_XC_PWM_CH1:
            MDrv_Write2Byte(0x13F454, Shift_L);
            MDrv_Write2ByteMask(0x13F456,BITS(1:0,Shift_H),BMASK(1:0));
            break;
        case E_XC_PWM_CH2:
            MDrv_Write2Byte(0x13F458, Shift_L);
            MDrv_Write2ByteMask(0x13F45A,BITS(1:0,Shift_H),BMASK(1:0));
            break;
        case E_XC_PWM_CH3:
            MDrv_Write2Byte(0x13F45C, Shift_L);
            MDrv_Write2ByteMask(0x13F45E,BITS(1:0,Shift_H),BMASK(1:0));
            break;
        case E_XC_PWM_CH4:
            MDrv_Write2Byte(0x13F460, Shift_L);
            MDrv_Write2ByteMask(0x13F462,BITS(1:0,Shift_H),BMASK(1:0));
            break;
        case E_XC_PWM_CH5:
        case E_XC_PWM_CH6:
        case E_XC_PWM_CH7:
        case E_XC_PWM_CH8:
        case E_XC_PWM_CH9:
            printf("[Utopia] The PWM%d is not support\n", (int)index);
            UNUSED(Shift_L);
            UNUSED(Shift_H);
            break;
        default:
            break;
    }
    _Hal_XC_S_PWM_VDBen_SW(index,1);
   // HAL_SUBBANK0;
}

void _Hal_XC_S_PNL_DutyCycle(MS_U8 index, MS_U32 u32DutyPWM)
{
    MS_U16  Duty_L, Duty_H;
    Duty_L = (MS_U16)u32DutyPWM;
    Duty_H = (MS_U8)(u32DutyPWM >> 16);
    //HAL_SUBBANK1;
    _Hal_XC_S_PWM_VDBen_SW(index,0);
    /* the Duty capability is restricted to ONLY 10-bit */
    switch(index)
    {
        case E_XC_PWM_CH0:
            MDrv_Write2Byte(0x13F406, Duty_L);
            MDrv_Write2ByteMask(0x13F442,BITS(1:0,Duty_H),BMASK(1:0));
            break;
        case E_XC_PWM_CH1:
            MDrv_Write2Byte(0x13F40C, Duty_L);
            MDrv_Write2ByteMask(0x13F442,BITS(3:2,Duty_H),BMASK(3:2));
            break;
        case E_XC_PWM_CH2:
            MDrv_Write2Byte(0x13F412, Duty_L);
	     MDrv_Write2ByteMask(0x13F442,BITS(5:4,Duty_H),BMASK(5:4));
            break;
        case E_XC_PWM_CH3:
            MDrv_Write2Byte(0x13F418, Duty_L);
            MDrv_Write2ByteMask(0x13F442,BITS(7:6,Duty_H),BMASK(7:6));
            break;
        case E_XC_PWM_CH4:
            MDrv_Write2Byte(0x13F41E, Duty_L);
            MDrv_Write2ByteMask(0x13F442,BITS(9:8,Duty_H),BMASK(9:8));
            break;
        case E_XC_PWM_CH5:
        case E_XC_PWM_CH6:
        case E_XC_PWM_CH7:
        case E_XC_PWM_CH8:
        case E_XC_PWM_CH9:
			printf("[Utopia] The PWM%d is not support\n", (int)index);
            UNUSED(Duty_L);
            UNUSED(Duty_H);
            break;
		default:
			break;
    }
    _Hal_XC_S_PWM_VDBen_SW(index,1);
   // HAL_SUBBANK0;
}
#endif
#define MPLUS_DIMMING_DUTY_MAX 0x1000
#define MPLUS_LOW_POWER_MODE1 0
#define MPLUS_LOW_POWER_MODE2 3
#define POWER_SECQUENCE_DELAY_TIME 1000
#define REG_LPLL_OVSRatioOut 0x103119
#define REG_LPLL_OVS 0x103146
#define LPLL_FREQ_XAL 24000000UL
#define PWM_BASE_XTAL 4000000UL
#define PWM_CALIBRATE 255

void Hal_XC_S_PNL_PWM_DutyCycle(void)
{
    #if 1 //Period follow Vsync

    MS_U16 u16OVSRatioOut = (((MDrv_ReadByte(REG_LPLL_OVSRatioOut) & 0xF0) >> 4)+1);
    MS_U16 u16Target_Value = SC_R2BYTE(0, REG_SC_BK10_0D_L); //Target Vtt
    MS_U32 u32OVSFromAP = MDrv_Read4Byte(REG_LPLL_OVS); // current OVS
    MS_U32 u32OVSFrequencyX1000 = 0;
    MS_U32 u32FrameRateFreqX1000, u32Period;

    if(u32OVSFromAP == 0)
        return;

    u32OVSFrequencyX1000 = (LPLL_FREQ_XAL * 100 / u32OVSFromAP)* 10* u16OVSRatioOut; //Frequency from OVS

    if( (47000 <= u32OVSFrequencyX1000 && u32OVSFrequencyX1000 <= 49000) && ( 2755 <= u16Target_Value && u16Target_Value <=  2850))
    {
        u32FrameRateFreqX1000 = u32OVSFrequencyX1000 * 2 ; // 2x mode
        u32Period = ( PWM_BASE_XTAL * 1000 / u32FrameRateFreqX1000 );
    }
    else if((49000 <= u32OVSFrequencyX1000 && u32OVSFrequencyX1000 <=  51000 ) && (2500 <= u16Target_Value && u16Target_Value <=  2755 ))
    {
        u32FrameRateFreqX1000 = u32OVSFrequencyX1000 * 2 ; // 2x mode
        u32Period = ( PWM_BASE_XTAL * 1000 / u32FrameRateFreqX1000 );
    }
    else if(( 59000 <= u32OVSFrequencyX1000 && u32OVSFrequencyX1000 <=  61000 ) && ( 2200 <= u16Target_Value && u16Target_Value <=  2500 ))
    {
        u32FrameRateFreqX1000 = u32OVSFrequencyX1000 * 2 ; // 2x mode
        u32Period = ( PWM_BASE_XTAL * 1000 / u32FrameRateFreqX1000 );
    }
    else{
        u32Period = SC_R2BYTE(0, REG_SC_BKF4_02_L);
            //u32FrameRateFreqX1000 = 120000;
        //return;
    }

    if(u32Period == 0)
        return;

    #endif

    //MS_U32 u32Period = SC_R2BYTE(0, REG_SC_BKF4_02_L);
    MS_U32 u32DutyPWM = u32Period * u32PWM_DUTY_VALUE_fromAP[0] / PWM_CALIBRATE;
    MS_U32 u32ShiftPWM = u32Period * u32PWM_SHIFT_VALUE_fromAP[0] / PWM_CALIBRATE;
    MS_U32 u32DutyPWM1 = u32Period * u32PWM_DUTY_VALUE_fromAP[1] / PWM_CALIBRATE;
    MS_U32 u32ShiftPWM1 = u32Period * u32PWM_SHIFT_VALUE_fromAP[1] / PWM_CALIBRATE;
    static MS_U32 u32preDutyPWM = 0, u32preDutyPWM1 = 0;
    MS_XC_CUS_CTRL_PARA stbuf_TconMode;

    #if 0//for test ISR execution time
    Hal_SC_ISR_UpdateMaxPd(E_XC_ISR_PNL_PWM_DUTY);
    Hal_SC_ISR_Start(E_XC_ISR_PNL_PWM_DUTY);
    #endif
    memset(&stbuf_TconMode,0,sizeof(MS_XC_CUS_CTRL_PARA));
    Hal_XC_S_PNL_Get_Tcon_Mode(NULL,&stbuf_TconMode);
    if( (stbuf_TconMode.u16Param==MPLUS_LOW_POWER_MODE1) || (stbuf_TconMode.u16Param==MPLUS_LOW_POWER_MODE2) )
    {
        MS_U32 u32Mplus_Period = 0;
        MS_XC_CUS_CTRL_PARA stbuf_duty;
        memset(&stbuf_duty,0,sizeof(MS_XC_CUS_CTRL_PARA));
        Hal_XC_S_PNL_GET_MPLUS_DIMMING_DUTY(NULL,&stbuf_duty);
        u32Mplus_Period = stbuf_duty.u16Param;
        u32Mplus_Period = ( (u32Mplus_Period>MPLUS_DIMMING_DUTY_MAX)?(MPLUS_DIMMING_DUTY_MAX):(u32Mplus_Period) );
        u32DutyPWM = u32DutyPWM*u32Mplus_Period/MPLUS_DIMMING_DUTY_MAX;
        u32ShiftPWM = u32ShiftPWM*u32Mplus_Period/MPLUS_DIMMING_DUTY_MAX;

        u32DutyPWM1 = u32DutyPWM1*u32Mplus_Period/MPLUS_DIMMING_DUTY_MAX;
        u32ShiftPWM1 = u32ShiftPWM1*u32Mplus_Period/MPLUS_DIMMING_DUTY_MAX;

        //Make Sure Duty < Period
        //printf("[PWM0] bLdInterruptStatus=%x, u32DutyPWM0=%x, u32ShiftPWM0=%x",bLdInterruptStatus,u32DutyPWM,u32ShiftPWM);
        //printf("[PWM0] u32DutyPWM1FromAP=%x, u32ShiftPWM0FromAP=%x",u32PWM_DUTY_fromAP[0],u32PWM_SHIFT_fromAP[0]);

        if(u32preDutyPWM != u32DutyPWM)
        {
            MDrv_PWM_Period(E_PWM_CH0, u32Period);
            if(u32ShiftPWM == 0)
                u32ShiftPWM = 1;
            if(u32DutyPWM > 0)
            {
                MDrv_PWM_Shift(E_PWM_CH0, u32ShiftPWM);

                if((u32ShiftPWM + u32DutyPWM) >= u32Period)
                {
                    MDrv_PWM_DutyCycle(E_PWM_CH0, (u32Period - 1));
                }
                else
                {
                    MDrv_PWM_DutyCycle(E_PWM_CH0, u32ShiftPWM + (u32DutyPWM - 1));
                }
            }
            else
            {
                MDrv_PWM_DutyCycle(E_PWM_CH0, 0);
                MDrv_PWM_Shift(E_PWM_CH0, 0xFFFF);
            }
        }
        u32preDutyPWM = u32DutyPWM;
        //printf("[PWM1] bLdInterruptStatus=%x, u32DutyPWM1=%x, u32ShiftPWM1=%x",bLdInterruptStatus,u32DutyPWM1,u32ShiftPWM1);
        //printf("[PWM1] u32DutyPWM1FromAP=%x, u32ShiftPWM1FromAP=%x",u32PWM_DUTY_fromAP[1],u32PWM_SHIFT_fromAP[1]);
        if(bLdInterruptStatus == 0) // LD is Off
        {

            if(u32preDutyPWM1 != u32DutyPWM1)
            {
                MDrv_PWM_Period(E_PWM_CH1, u32Period);
                if(u32ShiftPWM1 == 0)
                    u32ShiftPWM1 = 1;
                if(u32DutyPWM1 > 0)
                {
                    MDrv_PWM_Shift(E_PWM_CH1, u32ShiftPWM1);

                    if((u32ShiftPWM1 + u32DutyPWM1) >= u32Period)
                    {
                        MDrv_PWM_DutyCycle(E_PWM_CH1, (u32Period - 1));
                    }
                    else
                    {
                        MDrv_PWM_DutyCycle(E_PWM_CH1, u32ShiftPWM1 + (u32DutyPWM1 - 1));
                    }
                }
                else
                {
                    MDrv_PWM_DutyCycle(E_PWM_CH1, 0);
                    MDrv_PWM_Shift(E_PWM_CH1, 0xFFFF);
                }
            }
            u32preDutyPWM1 = u32DutyPWM1;
        }
    }
    else //Call Once Only in ISR deattached
    {

        //Make Sure Duty < Period
        //printf("[PWM0] bLdInterruptStatus=%x, u32DutyPWM0=%x, u32ShiftPWM0=%x",bLdInterruptStatus,u32DutyPWM,u32ShiftPWM);
        //printf("[PWM0] u32DutyPWM1FromAP=%x, u32ShiftPWM0FromAP=%x",u32PWM_DUTY_fromAP[0],u32PWM_SHIFT_fromAP[0]);

        if(u32preDutyPWM != u32DutyPWM)
        {
            MDrv_PWM_Period(E_PWM_CH0, u32Period);
            if(u32ShiftPWM == 0)
                u32ShiftPWM = 1;
            if(u32DutyPWM > 0)
            {
                MDrv_PWM_Shift(E_PWM_CH0, u32ShiftPWM);

                if((u32ShiftPWM + u32DutyPWM) >= u32Period)
                {
                    MDrv_PWM_DutyCycle(E_PWM_CH0, (u32Period - 1));
                }
                else
                {
                    MDrv_PWM_DutyCycle(E_PWM_CH0, u32ShiftPWM + (u32DutyPWM - 1));
                }
            }
            else
            {
                MDrv_PWM_DutyCycle(E_PWM_CH0, 0);
                MDrv_PWM_Shift(E_PWM_CH0, 0xFFFF);
            }
        }
        u32preDutyPWM = u32DutyPWM;
        //printf("[PWM1] bLdInterruptStatus=%x, u32DutyPWM1=%x, u32ShiftPWM1=%x",bLdInterruptStatus,u32DutyPWM1,u32ShiftPWM1);
        //printf("[PWM1] u32DutyPWM1FromAP=%x, u32ShiftPWM1FromAP=%x",u32PWM_DUTY_fromAP[1],u32PWM_SHIFT_fromAP[1]);
        if(bLdInterruptStatus == 0) // LD is Off
        {

            if(u32preDutyPWM1 != u32DutyPWM1)
            {
                MDrv_PWM_Period(E_PWM_CH1, u32Period);
                if(u32ShiftPWM1 == 0)
                    u32ShiftPWM1 = 1;
                if(u32DutyPWM1 > 0)
                {
                    MDrv_PWM_Shift(E_PWM_CH1, u32ShiftPWM1);

                    if((u32ShiftPWM1 + u32DutyPWM1) >= u32Period)
                    {
                        MDrv_PWM_DutyCycle(E_PWM_CH1, (u32Period - 1));
                    }
                    else
                    {
                        MDrv_PWM_DutyCycle(E_PWM_CH1, u32ShiftPWM1 + (u32DutyPWM1 - 1));
                    }
                }
                else
                {
                    MDrv_PWM_DutyCycle(E_PWM_CH1, 0);
                    MDrv_PWM_Shift(E_PWM_CH1, 0xFFFF);
                }
            }
            u32preDutyPWM1 = u32DutyPWM1;
        }
    }
}

void Hal_XC_S_PNL_PWM_DutyCycle_ISR(void *pInstance, MS_BOOL bEnable)
{
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));
    if(!MDrv_XC_InterruptIsAttached(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_PWM_DutyCycle, (void *) NULL))
    {
        if(bEnable)
        {
            MDrv_XC_InterruptAttachWithoutMutex(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_PWM_DutyCycle, (void *) NULL);
            bPWM_MPLUSDUTY_ISR_STATUS = TRUE;
        }
    }
    else
    {
        if(!bEnable)
        {
            MDrv_XC_InterruptDeAttachWithoutMutex(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_PWM_DutyCycle, (void *) NULL);
            bPWM_MPLUSDUTY_ISR_STATUS = FALSE;
            Hal_XC_S_PNL_PWM_DutyCycle();
        }
    }
}

static MS_U32 u32preOVS = 0;
void Hal_XC_S_PNL_PWM_Period(void)
{
    //Get the value from LPLL OVS and change PWM period dynamicly
    //  1.Get the value from LPLL OVS
    //  2.Check it is vaild OVSFrequencyX1000
    //  3.Check PWM0, PWM1 is pwm_adapt_freq_enable to write dynamic period


    MS_XC_CUS_CTRL_PARA stbuf_TconMode;

    memset(&stbuf_TconMode,0,sizeof(MS_XC_CUS_CTRL_PARA));
    Hal_XC_S_PNL_Get_Tcon_Mode(NULL,&stbuf_TconMode);
    if((bPWM_MPLUSDUTY_ISR_STATUS == TRUE) && ((stbuf_TconMode.u16Param==MPLUS_LOW_POWER_MODE1) || (stbuf_TconMode.u16Param==MPLUS_LOW_POWER_MODE2)))
    {
        //Mplus PWM mode work not do this
        return;
    }
    else
    {
    //MS_U16 u16OVSRaidoIn = ((MDrv_ReadByte(0x103119) & 0x0F)+1);
    MS_U16 u16OVSRatioOut = (((MDrv_ReadByte(REG_LPLL_OVSRatioOut) & 0xF0) >> 4)+1);
    MS_U16 u16Target_Value = SC_R2BYTE(0, REG_SC_BK10_0D_L); //Target Vtt
    MS_U32 u32OVSFromAP = MDrv_Read4Byte(REG_LPLL_OVS); // current OVS
    MS_U32 u32DutyFromAP = SC_R2BYTE(0, REG_SC_BKF4_03_L); // current Duty
    MS_U32 u32PeriodCurrent = SC_R2BYTE(0, REG_SC_BKF4_02_L); // current Period
    MS_U32 u32OVSFrequencyX1000 = 0;
        MS_U32 u32FrameRateFreqX1000, u32Period, u32Duty, u32Shift, u32Duty1, u32Shift1;

    if(u32OVSFromAP == 0)
    {
        //printf("LPLL OVS = 0 \n");
        return;
    }

    u32OVSFrequencyX1000 = (LPLL_FREQ_XAL * 100 / u32OVSFromAP)* 10* u16OVSRatioOut; //Frequency from OVS

    //printf("Dai...FX1000=%u OVSFromAP=%u DutyFromAP=%u FrcIn=%u FrcOut=%u \n",u32OVSFrequencyX1000,u32OVSFromAP, u32DutyFromAP,((MDrv_ReadByte(0x103119) & 0x0F)+1),(((MDrv_ReadByte(0x103119) & 0xF0) >> 4)+1));
    if( u32OVSFromAP !=  u32preOVS)
    {
        if( (47000 <= u32OVSFrequencyX1000 && u32OVSFrequencyX1000 <= 49000) && ( 2755 <= u16Target_Value && u16Target_Value <=  2850))
            u32FrameRateFreqX1000 = u32OVSFrequencyX1000 * 2 ; // 2x mode
        else if((49000 <= u32OVSFrequencyX1000 && u32OVSFrequencyX1000 <=  51000 ) && (2500 <= u16Target_Value && u16Target_Value <=  2755 ))
            u32FrameRateFreqX1000 = u32OVSFrequencyX1000 * 2 ; // 2x mode
        else if(( 59000 <= u32OVSFrequencyX1000 && u32OVSFrequencyX1000 <=  61000 ) && ( 2200 <= u16Target_Value && u16Target_Value <=  2500 ))
            u32FrameRateFreqX1000 = u32OVSFrequencyX1000 * 2 ; // 2x mode
        else{
                //printf("%s GetFrameRate oops!!! u32OVSFrequencyX1000 = %u, u32OVSFromAP = %u \n",__FUNCTION__, u32OVSFrequencyX1000, u32OVSFromAP);
                u32FrameRateFreqX1000 = 120000;
            return;
        }

        u32Period = ( PWM_BASE_XTAL * 1000 / u32FrameRateFreqX1000 );
        if(u32PeriodCurrent == 0)
        {
            //printf("%s GetPeriodCurrent oops!!!\n",__FUNCTION__);
            u32PeriodCurrent = u32Period; //Keep Original Value
        }

            //PWM0
        if(SC_R2BYTE(0, REG_SC_BKF4_04_L)==0x4602) //check pwm_adapt_freq_enable
        {
                //0x4602 is PWM_Dben = 0, PWM_Vdben = 1, PWM_ResetEn = 1, PWM_RstMux = 0
                u32Shift = u32Period * u32PWM_SHIFT_VALUE_fromAP[E_PWM_CH0] / PWM_CALIBRATE;
                u32Duty = u32Period * u32PWM_DUTY_VALUE_fromAP[E_PWM_CH0] / PWM_CALIBRATE;
                MDrv_PWM_Period(E_PWM_CH0, u32Period);

                if(u32Shift == 0)
                    u32Shift = 1;

                if(u32Duty > 0)
                {
                    MDrv_PWM_Shift(E_PWM_CH0, u32Shift);

                    if((u32Shift + u32Duty) >= u32Period)
                    {
                        MDrv_PWM_DutyCycle(E_PWM_CH0, (u32Period - 1));
                    }
                    else
            {
                        MDrv_PWM_DutyCycle(E_PWM_CH0, u32Shift + (u32Duty - 1));
                    }
            }
            else
            {
                    MDrv_PWM_Shift(E_PWM_CH0, 0xFFFF); //PWM pulse issue
                    MDrv_PWM_DutyCycle(E_PWM_CH0, 0);
            }
        }

            //PWM1
        if(SC_R2BYTE(0, REG_SC_BKF4_07_L)==0x4602) //check pwm_adapt_freq_enable
        {
                //0x4602 is PWM_Dben = 0, PWM_Vdben = 1, PWM_ResetEn = 1, PWM_RstMux = 0
                u32Shift1 = u32Period * u32PWM_SHIFT_VALUE_fromAP[E_PWM_CH1] / PWM_CALIBRATE;
                u32Duty1 = u32Period * u32PWM_DUTY_VALUE_fromAP[E_PWM_CH1] / PWM_CALIBRATE;
                MDrv_PWM_Period(E_PWM_CH1, u32Period);

                if(u32Shift1 == 0)
                    u32Shift1 = 1;

                if(u32Duty1 > 0)
                {
                    MDrv_PWM_Shift(E_PWM_CH1, u32Shift1);

                    if((u32Shift1 + u32Duty1) >= u32Period)
            {
                        MDrv_PWM_DutyCycle(E_PWM_CH1, (u32Period - 1));
            }
            else
            {
                        MDrv_PWM_DutyCycle(E_PWM_CH1, u32Shift1 + (u32Duty1 - 1));
                    }
                }
                else
                {
                    MDrv_PWM_Shift(E_PWM_CH1, 0xFFFF); //PWM pulse issue
                    MDrv_PWM_DutyCycle(E_PWM_CH1, 0);
            }
        }

            MDrv_PWM_Period(E_PWM_CH5, u32Period); // PWM5 is used for LD trigger

        u32preOVS = u32OVSFromAP;
    }
    }
}

void Hal_XC_S_PNL_PWM_Period_ISR(void *pInstance, MS_BOOL bEnable)
{
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));
    if(!MDrv_XC_InterruptIsAttached(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_PWM_Period, (void *) NULL))
    {
        if(bEnable)
        {
            //Need Init PWM
            MDrv_PWM_Init(E_PWM_DBGLV_ERR_ONLY);
            MDrv_XC_InterruptAttachWithoutMutex(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_PWM_Period, (void *) NULL);
        }
    }
    else
    {
        if(!bEnable)
        {
            MDrv_XC_InterruptDeAttachWithoutMutex(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_PWM_Period, (void *) NULL);
        }
    }
}

extern void MApi_PNL_SetByPassSSCConversion(MS_BOOL b);
void Hal_XC_S_PNL_TOOL_OPTION1(void *pInstance, MS_BOOL bEnable,MS_U16 u16Param,MS_U16 u16Param1,MS_U32 u32Param)
{
    MApi_PNL_SetByPassSSCConversion(FALSE);

    if(bEnable)
    {
        if(u32Param == MHAL_EN_TOOL_OPTION1_MODULE_BOE \
        && u16Param == MHAL_E_TOOL_OPTION1_INCH_43 \
        && u16Param1 == MHAL_E_TOOL_OPTION1_UK65){
        MApi_PNL_SetByPassSSCConversion(TRUE);
        }

        if(u32Param == MHAL_EN_TOOL_OPTION1_MODULE_INNOLUX \
        && u16Param == MHAL_E_TOOL_OPTION1_INCH_50 \
        && (u16Param1 == MHAL_E_TOOL_OPTION1_UK63 \
        ||u16Param1 == MHAL_E_TOOL_OPTION1_UK65)){
        MApi_PNL_SetByPassSSCConversion(TRUE);
        }
    }
    u16Inch = u16Param;
    u16ToolType = u16Param1;
    u16ModuleType = u32Param;

}

#if SHARP_VBY1
extern MS_BOOL MHal_PNL_IsVBy1(void);
MS_BOOL MHal_S_Disp_Ctl_Set_Tooloption1_IsVBy1SHARP70(void)
{
#if 1
    if(MHal_PNL_IsVBy1() \
    && u16ModuleType == MHAL_EN_TOOL_OPTION1_MODULE_SHARP \
    && u16Inch == MHAL_E_TOOL_OPTION1_INCH_70)
#else
    if(MHal_PNL_IsVBy1() \
    && u16ModuleType == MHAL_EN_TOOL_OPTION1_MODULE_SHARP \
    && u16Inch == MHAL_E_TOOL_OPTION1_INCH_70 \
    && u16ToolType == MHAL_E_TOOL_OPTION1_UK65)
#endif
    return TRUE;
    else
    return FALSE;
}

MS_BOOL MHal_S_Disp_Ctl_Set_Tooloption1_IsVBy1LGD75(void)
{
    if(MHal_PNL_IsVBy1() \
    && u16ModuleType == MHAL_EN_TOOL_OPTION1_MODULE_LGD \
    && u16Inch == MHAL_E_TOOL_OPTION1_INCH_75)
    return TRUE;
    else
    return FALSE;
}

MS_BOOL MHal_S_Disp_Ctl_Set_Tooloption1_IsVBy1LGD65(void)
{
    if(MHal_PNL_IsVBy1() \
    && u16ModuleType == MHAL_EN_TOOL_OPTION1_MODULE_LGD \
    && u16Inch == MHAL_E_TOOL_OPTION1_INCH_65)
    return TRUE;
    else
    return FALSE;
}

MS_BOOL MHal_S_Disp_Ctl_Set_Tooloption1_IsVBy1SHARP60UK62(void)
{
    if(MHal_PNL_IsVBy1() 
    && u16ModuleType == MHAL_EN_TOOL_OPTION1_MODULE_SHARP 
    && u16Inch == MHAL_E_TOOL_OPTION1_INCH_60
    && u16ToolType == MHAL_E_TOOL_OPTION1_UK62
    )
    return TRUE;
    else
    return FALSE;
}

MS_BOOL MHal_S_Disp_Ctl_Set_Tooloption1_IsVBy1HKC50UK65(void)
{
    if(MHal_PNL_IsVBy1() 
    && u16ModuleType == MHAL_EN_TOOL_OPTION1_MODULE_HKC 
    && u16Inch == MHAL_E_TOOL_OPTION1_INCH_50
    && u16ToolType == MHAL_E_TOOL_OPTION1_UK65
    )
    return TRUE;
    else
    return FALSE;
}
MS_BOOL MHal_S_Disp_Ctl_Set_Tooloption1_IsVBy1HKC50UK63(void)
{
    if(MHal_PNL_IsVBy1() 
    && u16ModuleType == MHAL_EN_TOOL_OPTION1_MODULE_HKC 
    && u16Inch == MHAL_E_TOOL_OPTION1_INCH_50
    && u16ToolType == MHAL_E_TOOL_OPTION1_UK63
    )
    return TRUE;
    else
    return FALSE;
}

MS_BOOL MHal_S_Disp_Ctl_Set_Tooloption1_IsVBy1BOE75UK65Vby1(void)
{//LMTASKWBS-74364
    if(MHal_PNL_IsVBy1() 
    && u16ModuleType == MHAL_EN_TOOL_OPTION1_MODULE_BOE 
    && u16Inch == MHAL_E_TOOL_OPTION1_INCH_75
    && u16ToolType == MHAL_E_TOOL_OPTION1_UK65
    )
    return TRUE;
    else
    return FALSE;
}

#endif

MS_BOOL Hal_XC_S_PNL_TSCIC_TSCICFLASHDONECHECK(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MS_U8 u8FlashDone = 0;
    u8FlashDone = (MS_U8)MDrv_ReadByte(0x3309C4)&0x1;
    if( (u8FlashDone) && (stBuf->u32Param != 0) )
    {
        MDrv_Write2ByteMask(0x3309A8, 0 , 0x8000);
    }
    else
    {
        MDrv_Write2ByteMask(0x3309A8, 0x8000 , 0x8000);
    }
    return TRUE;
}

MS_BOOL Hal_XC_S_PNL_TSCIC_TSCICSWRESET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MDrv_Write2ByteMask(0x3309A8, 0x1 , 0x1);
    MDrv_Write2ByteMask(0x3309A8, 0 , 0x1);
    return TRUE;
}

void Hal_XC_S_PNL_Local_Dimming_Set_RecordingTime(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    u32LdRecordingTime = MsOS_GetSystemTime();
}

#define POWER_SECQUENCE_DELAY_TIME 1000
void Hal_XC_S_PNL_Local_Dimming_Detection(void)
{
    //LD DISP_IRQ attached in kernel,but current flow would dectect all IRQs of INT_VSYNC attached in XC(witch didn't consider IRQ attached in kernel).
    //If there are no IRQs of INT_VSYNC attached in XC and it would mask the INT_VSYNC.
    //If INT_VSYNC was masked in this condition LD would stop re-flash its data and cause dark screen or flicker at anytime with mased vsync.
    /*if(MDrv_Read2Byte(0x111E00)==0)
    {
        u32LdRecirdingTime = 0;
    }*/
    if((MDrv_Read2Byte(0x132E02)==1) && (u32LdRecordingTime>0))
    {
        if((MsOS_GetSystemTime() - u32LdRecordingTime) > POWER_SECQUENCE_DELAY_TIME)// For meet LG HW's power sequence
        {
            //Set Demo mode
            MDrv_Write2ByteMask(0x132E44,0x0000, 0xF000); //MHal_LD_SetSWPulseMode
            //led_num
            MDrv_Write2ByteMask(0x132EE4,(0x0) << 1, 0x000E); ////MHal_LD_SetLSFOutMode
            MDrv_Write2ByteMask(0x132E44,0x0000, 0x0FFF); //MHal_LD_SetSWPulseId
            //led_luma
            MDrv_Write2ByteMask(0x132E46,0x0000, 0x00FF); //MHal_LD_SetSWPulseLEDIntensity
            MDrv_Write2ByteMask(0x132E46,0x0000, 0xFF00); //MHal_LD_SetSWPulseLDBIntensity
             //disable Compensation
            MDrv_Write2ByteMask(0x132E8C,0x0040, 0x0040); //MHal_LD_SetCompensationEn

            //MDrv_Write2ByteMask(0x132E02,0x0001 , 0x0001); //Didn't enable there if enable there no-ld panel would enable
            u32LdRecordingTime = 0;
        }
    }
}

MS_BOOL Hal_XC_S_PNL_GET_LOCALDIMMING_ISR_STATUS(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    return bLdInterruptStatus;
}

void Hal_XC_S_PNL_Local_Dimming_Control_ISR(void *pInstance, MS_BOOL bEnable)
{
    XC_INSTANCE_PRIVATE *psXCInstPri = NULL;
    UtopiaInstanceGetPrivate(pInstance, (void**)&psXCInstPri);
    XC_RESOURCE_PRIVATE* pXCResourcePrivate = NULL;
    UtopiaResourceGetPrivate(g_pXCResource[_XC_SELECT_INTERNAL_VARIABLE(psXCInstPri->u32DeviceID)],(void**)(&pXCResourcePrivate));
    if(!MDrv_XC_InterruptIsAttached(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_Local_Dimming_Detection, (void *) NULL))
    {
        if(bEnable)
        {
            bLdInterruptStatus = 1;
            MDrv_XC_InterruptAttachWithoutMutex(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_Local_Dimming_Detection, (void *) NULL);
        }
    }
    else
    {
        if(!bEnable)
        {
            bLdInterruptStatus = 0;
            MDrv_XC_InterruptDeAttachWithoutMutex(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_Local_Dimming_Detection, (void *) NULL);
        }
    }
}

MS_BOOL Hal_XC_S_PNL_PixelOverDriver_Setting(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf)
{
    MS_U8 u8PanelType = ((R2BYTE(0x13F2E6) & 0x00F0)>>4); // EPI is 5, CEDS is 8

    //Only EPI panel has PixelOD IP. If current panel is not EPI, return FALSE
    if(u8PanelType != TCON_PANEL_TYPE_EPI)
    {
        return FALSE;
    }

    MS_U8* pControlTbl = (MS_U8*)stBuf->pParam ;
    MS_U32 u32TblIndex = 0;
    MS_U16 u16offset = 0;
    MDrv_WriteByteMask(0x3321F1, BIT(7), BIT(7));
    for(u32TblIndex=0;u32TblIndex<0x1E2;u32TblIndex++)
    {
        if(u32TblIndex <= 0xFD)
        {
            u16offset = u32TblIndex;
            MDrv_WriteByteMask(0x332002+u16offset, pControlTbl[u32TblIndex] , 0xFF);
        }
        else if(u32TblIndex <= 0x1E1)
        {
            u16offset = u32TblIndex -0xFE;
            MDrv_WriteByteMask(0x332102+u16offset, pControlTbl[u32TblIndex] , 0xFF);
        }
    }
    MDrv_WriteByteMask(0x3321F1, 0, BIT(7));
    return TRUE;
}

void Hal_XC_S_PNL_PanelGamma_Enable(void *pInstance, MS_BOOL bEnable)
{
    if(bEnable)
    {
        if(SC_R2BYTEMSK(0, REG_SC_BK24_6D_L, BIT(12)) != BIT(12))
        {
            // enable new mload trigger. Watch reg_mload_vcnt_trig & reg_mload_vcnt_sel
            SC_W2BYTEMSK(0, REG_SC_BK68_5F_L, 1 << 15, BIT(15)); // 1 OP2

            MApi_XC_MLoad_WriteCmd_And_Fire(REG_SC_BK24_6D_L, BIT(12) , BIT(12)); //EnablePanelGamma

            SC_W2BYTEMSK(0, REG_SC_BK68_5F_L, 0 << 15, BIT(15)); // 0 OP1
        }
    }
    else
    {
        {
            // enable new mload trigger. Watch reg_mload_vcnt_trig & reg_mload_vcnt_sel
            SC_W2BYTEMSK(0, REG_SC_BK68_5F_L, 1 << 15, BIT(15)); // 1 OP2

            MApi_XC_MLoad_WriteCmd_And_Fire(REG_SC_BK24_6D_L, 0 , BIT(12)); //DisablePanelGamma

            SC_W2BYTEMSK(0, REG_SC_BK68_5F_L, 0 << 15, BIT(15)); // 0 OP1
        }
    }
}

void Hal_XC_S_PNL_VidoeFIFOADjust(void)
{
        MS_BOOL bForceSSCoff = false;
        if(R2BYTEMSK((L_BK_LPLL(0x0D)), BIT(11)) == BIT(11))
        {
            W2BYTEMSK((L_BK_LPLL(0x0D)), 0, BIT(11));
            bForceSSCoff = true;
        }
            MOD_W2BYTEMSK(REG_MOD_BK00_5B_L, BIT(1), BIT(1));//auto reset
            MsOS_DelayTask(1);
            MOD_W2BYTEMSK(REG_MOD_BK00_5C_L, 0x8070, 0xFFFF);
            MsOS_DelayTask(1);
            MOD_W2BYTEMSK(REG_MOD_BK00_5C_L, 0x8041, 0xFFFF);
            MOD_W2BYTEMSK(REG_MOD_BK00_5B_L, BIT(3), BIT(3));
            MsOS_DelayTask(1);
            MOD_W2BYTEMSK(REG_MOD_BK00_5B_L, 0, BIT(3));
            MsOS_DelayTask(1);
            MOD_W2BYTEMSK(REG_MOD_BK00_5B_L, 0, BIT(1));//disable auto reset
        if(bForceSSCoff)
        {
            MsOS_DelayTask(1);
            W2BYTEMSK((L_BK_LPLL(0x0D)), BIT(11), BIT(11)); // Enable ssc
        }
        MsOS_DelayTask(1);
}

static MS_U16 u16unlockcnt = 0;

void Hal_XC_S_PNL_CHECKUNLOCK(void)
{
    if((MDrv_Read4Byte(0x111E00)!=0))
    {
        if( (MOD_R2BYTE(REG_MOD_BK00_7E_L)>u16unlockcnt) || ((MOD_R2BYTEMSK(REG_MOD_BK00_61_L,BIT(14)))!=BIT(14)))
        {
            Hal_XC_S_PNL_VidoeFIFOADjust();
        }
        else if((MOD_R2BYTEMSK(REG_MOD_BK00_7E_L,0x7F) == 0x7F))
        {
            MOD_W2BYTEMSK(REG_MOD_BK00_7E_L, BIT(7), BIT(7));
            MsOS_DelayTask(1);
            MOD_W2BYTEMSK(REG_MOD_BK00_7E_L, 0, BIT(7));
        }
        u16unlockcnt = MOD_R2BYTE(REG_MOD_BK00_7E_L);
    }
}


void Hal_XC_S_PNL_CHECKUNLOCK_ISR(void *pInstance, MS_BOOL bEnable)
{
    if(!MDrv_XC_InterruptIsAttached(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_CHECKUNLOCK, (void *) NULL))
    {
        if(bEnable)
        {
            u16unlockcnt = MOD_R2BYTE(REG_MOD_BK00_7E_L);
            MDrv_XC_InterruptAttachWithoutMutex(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_CHECKUNLOCK, (void *) NULL);
        }
    }
    else
    {
        if(!bEnable)
        {
            MDrv_XC_InterruptDeAttachWithoutMutex(pInstance, SC_INT_VSINT, Hal_XC_S_PNL_CHECKUNLOCK, (void *) NULL);
        }
    }
}


#endif

