//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (��MStar Confidential Information��) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef MHAL_S_DISP_CTL_H
#define MHAL_S_DISP_CTL_H
#ifdef STELLAR
#include "apiXC_cus.h"

// ADC
MS_BOOL Hal_XC_S_ADC_ScartOverlay_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_ADC_Gain_SET(void *pInstance,APIXC_AdcGainOffsetSetting *stBuf);
MS_BOOL Hal_XC_S_ADC_Gain_GET(void *pInstance,APIXC_AdcGainOffsetSetting *stBuf);
MS_BOOL Hal_XC_S_ADC_Offset_SET(void *pInstance,APIXC_AdcGainOffsetSetting *stBuf);
MS_BOOL Hal_XC_S_ADC_Offset_GET(void *pInstance,APIXC_AdcGainOffsetSetting *stBuf);

// HDMI
MS_BOOL Hal_XC_S_HDMI_HDE_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_HDMI_HTT_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_HDMI_VDE_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_HDMI_VTT_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_HDMI_DDCChannel_EN(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_HDMI_AVmute_Blank(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

// PCMode
MS_BOOL Hal_XC_S_PCMode_VResolution_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PCMode_HResolution_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PCMode_Htotal_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

// XC
MS_BOOL Hal_XC_S_XC_FrameBufferNumber_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_XC_FrameBufferNumber_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_XC_RwPointDiff_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_XC_RwPointDiff_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_XC_OpwOff_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_XC_OpwOff_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_XC_InterlaceStatus_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_XC_FieldDetect_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_XC_DispDeWin_GET(void *pInstance,MS_WINDOW_TYPE *stBuf);
MS_BOOL Hal_XC_S_XC_Fast_Setwindow_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
void Hal_XC_S_XC_OPW_UnMask(void *pInstance);
void Hal_XC_S_XC_Set_APmute_flag(void* pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_XC_Get_APmute_flag(SCALER_WIN eWindow);


// PATTERN
MS_BOOL Hal_XC_S_PATTERN_MOD_SET(void* pInstance,MS_XC_CUS_PATTERN_PARA *stBuf);
MS_BOOL Hal_XC_S_PATTERN_IP1_RGB_SET(void* pInstance,MS_XC_CUS_PATTERN_PARA *stBuf);
MS_BOOL Hal_XC_S_Pattern_IP1_Video_Latency(void* pInstance,MS_XC_CUS_PATTERN_VIDEO_LATENCY *stPatternPara);

// PIP / Smart Zoom
MS_BOOL Hal_XC_S_PIP_SmartZoom_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PIP_SmartZoom_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

// 3D
MS_BOOL Hal_XC_S_3D_KR3DMode_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_3D_KR3DMode_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

// PVR
MS_BOOL Hal_XC_S_PVR_DualWinForAPVR_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PVR_DualWinForAPVR_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PVR_SC2_OPTiming(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

// PQ
MS_BOOL Hal_XC_S_PQ_PicSetHSC_SET(void *pInstance,MS_XC_CUS_PQ_PARA *stBuf);
MS_BOOL Hal_XC_S_PQ_SET_UC_DISABLE(void *pInstance, MS_XC_CUS_PQ_SET_UC_DISABLE *stBuf);
MS_BOOL Hal_XC_S_PQ_GetUC_Patch_Status(void *pInstance, MS_XC_CUS_PQ_GET_UC_PATCH_STATUS *stBuf);
MS_BOOL Hal_XC_S_PQ_Set_DeFlicker_Control(void *pInstance, MS_XC_CUS_PQ_DEFLICKER *stBuf);

// DS
MS_BOOL Hal_XC_S_DS_Status_GET(void *pInstance,DSLOAD_TYPE *stBuf);
MS_BOOL Hal_XC_S_DS_GST_PROCESS_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_DS_GST_PROCESS_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_DS_CAPTURE_PROCESS_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_DS_CAPTURE_PROCESS_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_DS_SET_ForceP_Flag(void *pInstance,MS_XC_CUS_FORCE_P_STATUS *stBuf);
MS_BOOL Hal_XC_S_DS_GET_ForceP_Flag(void *pInstance,MS_XC_CUS_FORCE_P_STATUS *stBuf);

// PNL
MS_BOOL Hal_XC_S_PNL_STR_EN(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_OutPECurrent_SET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_PanelHStart_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_PanelVStart_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_PanelWidth_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_PanelHeight_GET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

//Tcon
MS_BOOL Hal_XC_S_PNL_Tcon_SetBOERGBWBypass(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_Tcon_WPR_ON(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_Tcon_OPC_ON(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_Tcon_WSE_ON(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_Tcon_Mode_Sel(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_Get_Tcon_Mode(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_Tcon_SET_FrameGainLimit(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_Tcon_GET_FrameGainLimit(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_Tcon_SET_PixelGainLimit(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_Tcon_GET_PixelGainLimit(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

MS_BOOL Hal_XC_S_PNL_TSCIC_ControlTbl(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_TSCIC_TSCIClTbl(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_TSCIC_TSCICSWRESET(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

MS_BOOL Hal_XC_S_PNL_GET_MPLUS_DIMMING_DUTY(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_SET_PWM_DUTY_fromAP(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_GET_PWM_DUTY_fromAP(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_SET_PWM_SHIFT_fromAP(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_GET_PWM_SHIFT_fromAP(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_GET_MPLUSDUTY_ISR_STATUS(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_GET_MPLUS_STATUS(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

void Hal_XC_S_PNL_PWM_DutyCycle(void);
void Hal_XC_S_PNL_PWM_DutyCycle_ISR(void *pInstance, MS_BOOL bEnable);
void Hal_XC_S_PNL_PWM_Period(void);
void Hal_XC_S_PNL_PWM_Period_ISR(void *pInstance, MS_BOOL bEnable);
void Hal_XC_S_PNL_TOOL_OPTION1(void *pInstance, MS_BOOL bEnable,MS_U16 u16Param,MS_U16 u16Param1,MS_U32 u32Param);


MS_BOOL Hal_XC_S_PNL_TSCIC_TSCICFLASHDONECHECK(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

void Hal_XC_S_PNL_Local_Dimming_Set_RecordingTime(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
MS_BOOL Hal_XC_S_PNL_GET_LOCALDIMMING_ISR_STATUS(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);
void Hal_XC_S_PNL_Local_Dimming_Control_ISR(void *pInstance, MS_BOOL bEnable);
void Hal_XC_S_PNL_PanelGamma_Enable(void *pInstance, MS_BOOL bEnable);

MS_BOOL Hal_XC_S_PNL_PixelOverDriver_Setting(void *pInstance,MS_XC_CUS_CTRL_PARA *stBuf);

void Hal_XC_S_PNL_VidoeFIFOADjust(void);
void Hal_XC_S_PNL_CHECKUNLOCK(void);

void Hal_XC_S_PNL_CHECKUNLOCK_ISR(void *pInstance, MS_BOOL bEnable);

typedef enum
{
    E_XC_PWM_CH0,
    E_XC_PWM_CH1,
    E_XC_PWM_CH2,
    E_XC_PWM_CH3,
    E_XC_PWM_CH4,
    E_XC_PWM_CH5,
    E_XC_PWM_CH6,
    E_XC_PWM_CH7,
    E_XC_PWM_CH8,
    E_XC_PWM_CH9,
    E_XC_PWM_MAX,
}XC_PWM_ChNum;

#endif
#endif
