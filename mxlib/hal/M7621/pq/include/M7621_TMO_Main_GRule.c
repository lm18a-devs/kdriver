
#if PQ_GRULE_TMO_MODE_ENABLE
code U8 MST_GRule_TMO_MODE_IP_Index_Main[PQ_GRULE_TMO_MODE_IP_NUM_Main]=
{
    PQ_IP_TMO_SOURCE_SETTING_Main,
    PQ_IP_TMO_TARGET_SETTING_Main,
    PQ_IP_TMO_Option_Main,
};
#endif


#if PQ_GRULE_TMO_MODE_ENABLE
code U8 MST_GRule_TMO_MODE_Main[QM_TMO_INPUTTYPE_NUM_Main][PQ_GRULE_TMO_MODE_NUM_Main][PQ_GRULE_TMO_MODE_IP_NUM_Main]=
{
    #if PQ_QM_VR
    {//RESERVED0, 0
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_RESERVED0_Main,
            PQ_IP_TMO_TARGET_SETTING_RESERVED0_L_Main,
            PQ_IP_TMO_Option_RESERVED0_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_RESERVED0_Main,
            PQ_IP_TMO_TARGET_SETTING_RESERVED0_M_Main,
            PQ_IP_TMO_Option_RESERVED0_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_RESERVED0_Main,
            PQ_IP_TMO_TARGET_SETTING_RESERVED0_H_Main,
            PQ_IP_TMO_Option_RESERVED0_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//BT709, 1
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_BT709_Main,
            PQ_IP_TMO_TARGET_SETTING_BT709_L_Main,
            PQ_IP_TMO_Option_BT709_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_BT709_Main,
            PQ_IP_TMO_TARGET_SETTING_BT709_M_Main,
            PQ_IP_TMO_Option_BT709_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_BT709_Main,
            PQ_IP_TMO_TARGET_SETTING_BT709_H_Main,
            PQ_IP_TMO_Option_BT709_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//UNSPECIFIED, 2
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_UNSPECIFIED_Main,
            PQ_IP_TMO_TARGET_SETTING_UNSPECIFIED_L_Main,
            PQ_IP_TMO_Option_UNSPECIFIED_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_UNSPECIFIED_Main,
            PQ_IP_TMO_TARGET_SETTING_UNSPECIFIED_M_Main,
            PQ_IP_TMO_Option_UNSPECIFIED_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_UNSPECIFIED_Main,
            PQ_IP_TMO_TARGET_SETTING_UNSPECIFIED_H_Main,
            PQ_IP_TMO_Option_UNSPECIFIED_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//RESERVED3, 3
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_RESERVED3_Main,
            PQ_IP_TMO_TARGET_SETTING_RESERVED3_L_Main,
            PQ_IP_TMO_Option_RESERVED3_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_RESERVED3_Main,
            PQ_IP_TMO_TARGET_SETTING_RESERVED3_M_Main,
            PQ_IP_TMO_Option_RESERVED3_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_RESERVED3_Main,
            PQ_IP_TMO_TARGET_SETTING_RESERVED3_H_Main,
            PQ_IP_TMO_Option_RESERVED3_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//GAMMA2P2, 4
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_GAMMA2P2_Main,
            PQ_IP_TMO_TARGET_SETTING_GAMMA2P2_L_Main,
            PQ_IP_TMO_Option_GAMMA2P2_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_GAMMA2P2_Main,
            PQ_IP_TMO_TARGET_SETTING_GAMMA2P2_M_Main,
            PQ_IP_TMO_Option_GAMMA2P2_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_GAMMA2P2_Main,
            PQ_IP_TMO_TARGET_SETTING_GAMMA2P2_H_Main,
            PQ_IP_TMO_Option_GAMMA2P2_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//GAMMA2P8, 5
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_GAMMA2P8_Main,
            PQ_IP_TMO_TARGET_SETTING_GAMMA2P8_L_Main,
            PQ_IP_TMO_Option_GAMMA2P8_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_GAMMA2P8_Main,
            PQ_IP_TMO_TARGET_SETTING_GAMMA2P8_M_Main,
            PQ_IP_TMO_Option_GAMMA2P8_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_GAMMA2P8_Main,
            PQ_IP_TMO_TARGET_SETTING_GAMMA2P8_H_Main,
            PQ_IP_TMO_Option_GAMMA2P8_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//BT601525_601625, 6
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_BT601525_601625_Main,
            PQ_IP_TMO_TARGET_SETTING_BT601525_601625_L_Main,
            PQ_IP_TMO_Option_BT601525_601625_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_BT601525_601625_Main,
            PQ_IP_TMO_TARGET_SETTING_BT601525_601625_M_Main,
            PQ_IP_TMO_Option_BT601525_601625_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_BT601525_601625_Main,
            PQ_IP_TMO_TARGET_SETTING_BT601525_601625_H_Main,
            PQ_IP_TMO_Option_BT601525_601625_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//SMPTE240M, 7
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_SMPTE240M_Main,
            PQ_IP_TMO_TARGET_SETTING_SMPTE240M_L_Main,
            PQ_IP_TMO_Option_SMPTE240M_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_SMPTE240M_Main,
            PQ_IP_TMO_TARGET_SETTING_SMPTE240M_M_Main,
            PQ_IP_TMO_Option_SMPTE240M_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_SMPTE240M_Main,
            PQ_IP_TMO_TARGET_SETTING_SMPTE240M_H_Main,
            PQ_IP_TMO_Option_SMPTE240M_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//LINEAR, 8
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_LINEAR_Main,
            PQ_IP_TMO_TARGET_SETTING_LINEAR_L_Main,
            PQ_IP_TMO_Option_LINEAR_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_LINEAR_Main,
            PQ_IP_TMO_TARGET_SETTING_LINEAR_M_Main,
            PQ_IP_TMO_Option_LINEAR_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_LINEAR_Main,
            PQ_IP_TMO_TARGET_SETTING_LINEAR_H_Main,
            PQ_IP_TMO_Option_LINEAR_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//LOG0, 9
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_LOG0_Main,
            PQ_IP_TMO_TARGET_SETTING_LOG0_L_Main,
            PQ_IP_TMO_Option_LOG0_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_LOG0_Main,
            PQ_IP_TMO_TARGET_SETTING_LOG0_M_Main,
            PQ_IP_TMO_Option_LOG0_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_LOG0_Main,
            PQ_IP_TMO_TARGET_SETTING_LOG0_H_Main,
            PQ_IP_TMO_Option_LOG0_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//LOG1, 10
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_LOG1_Main,
            PQ_IP_TMO_TARGET_SETTING_LOG1_L_Main,
            PQ_IP_TMO_Option_LOG1_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_LOG1_Main,
            PQ_IP_TMO_TARGET_SETTING_LOG1_M_Main,
            PQ_IP_TMO_Option_LOG1_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_LOG1_Main,
            PQ_IP_TMO_TARGET_SETTING_LOG1_H_Main,
            PQ_IP_TMO_Option_LOG1_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//XVYCC, 11
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_XVYCC_Main,
            PQ_IP_TMO_TARGET_SETTING_XVYCC_L_Main,
            PQ_IP_TMO_Option_XVYCC_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_XVYCC_Main,
            PQ_IP_TMO_TARGET_SETTING_XVYCC_M_Main,
            PQ_IP_TMO_Option_XVYCC_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_XVYCC_Main,
            PQ_IP_TMO_TARGET_SETTING_XVYCC_H_Main,
            PQ_IP_TMO_Option_XVYCC_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//BT1361, 12
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_BT1361_Main,
            PQ_IP_TMO_TARGET_SETTING_BT1361_L_Main,
            PQ_IP_TMO_Option_BT1361_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_BT1361_Main,
            PQ_IP_TMO_TARGET_SETTING_BT1361_M_Main,
            PQ_IP_TMO_Option_BT1361_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_BT1361_Main,
            PQ_IP_TMO_TARGET_SETTING_BT1361_H_Main,
            PQ_IP_TMO_Option_BT1361_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//SRGB_SYCC, 13
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_SRGB_SYCC_Main,
            PQ_IP_TMO_TARGET_SETTING_SRGB_SYCC_L_Main,
            PQ_IP_TMO_Option_SRGB_SYCC_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_SRGB_SYCC_Main,
            PQ_IP_TMO_TARGET_SETTING_SRGB_SYCC_M_Main,
            PQ_IP_TMO_Option_SRGB_SYCC_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_SRGB_SYCC_Main,
            PQ_IP_TMO_TARGET_SETTING_SRGB_SYCC_H_Main,
            PQ_IP_TMO_Option_SRGB_SYCC_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//BT2020NCL, 14
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_BT2020NCL_Main,
            PQ_IP_TMO_TARGET_SETTING_BT2020NCL_L_Main,
            PQ_IP_TMO_Option_BT2020NCL_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_BT2020NCL_Main,
            PQ_IP_TMO_TARGET_SETTING_BT2020NCL_M_Main,
            PQ_IP_TMO_Option_BT2020NCL_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_BT2020NCL_Main,
            PQ_IP_TMO_TARGET_SETTING_BT2020NCL_H_Main,
            PQ_IP_TMO_Option_BT2020NCL_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//BT2020CL, 15
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_BT2020CL_Main,
            PQ_IP_TMO_TARGET_SETTING_BT2020CL_L_Main,
            PQ_IP_TMO_Option_BT2020CL_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_BT2020CL_Main,
            PQ_IP_TMO_TARGET_SETTING_BT2020CL_M_Main,
            PQ_IP_TMO_Option_BT2020CL_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_BT2020CL_Main,
            PQ_IP_TMO_TARGET_SETTING_BT2020CL_H_Main,
            PQ_IP_TMO_Option_BT2020CL_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//SMPTE2084, 16
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_SMPTE2084_Main,
            PQ_IP_TMO_TARGET_SETTING_SMPTE2084_L_Main,
            PQ_IP_TMO_Option_SMPTE2084_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_SMPTE2084_Main,
            PQ_IP_TMO_TARGET_SETTING_SMPTE2084_M_Main,
            PQ_IP_TMO_Option_SMPTE2084_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_SMPTE2084_Main,
            PQ_IP_TMO_TARGET_SETTING_SMPTE2084_H_Main,
            PQ_IP_TMO_Option_SMPTE2084_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//SMPTE428, 17
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_SMPTE428_Main,
            PQ_IP_TMO_TARGET_SETTING_SMPTE428_L_Main,
            PQ_IP_TMO_Option_SMPTE428_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_SMPTE428_Main,
            PQ_IP_TMO_TARGET_SETTING_SMPTE428_M_Main,
            PQ_IP_TMO_Option_SMPTE428_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_SMPTE428_Main,
            PQ_IP_TMO_TARGET_SETTING_SMPTE428_H_Main,
            PQ_IP_TMO_Option_SMPTE428_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//HLG, 18
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_HLG_Main,
            PQ_IP_TMO_TARGET_SETTING_HLG_L_Main,
            PQ_IP_TMO_Option_HLG_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_HLG_Main,
            PQ_IP_TMO_TARGET_SETTING_HLG_M_Main,
            PQ_IP_TMO_Option_HLG_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_HLG_Main,
            PQ_IP_TMO_TARGET_SETTING_HLG_H_Main,
            PQ_IP_TMO_Option_HLG_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//BT1886, 19
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_BT1886_Main,
            PQ_IP_TMO_TARGET_SETTING_BT1886_L_Main,
            PQ_IP_TMO_Option_BT1886_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_BT1886_Main,
            PQ_IP_TMO_TARGET_SETTING_BT1886_M_Main,
            PQ_IP_TMO_Option_BT1886_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_BT1886_Main,
            PQ_IP_TMO_TARGET_SETTING_BT1886_H_Main,
            PQ_IP_TMO_Option_BT1886_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//DOLBYMETA, 20
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_DOLBYMETA_Main,
            PQ_IP_TMO_TARGET_SETTING_DOLBYMETA_L_Main,
            PQ_IP_TMO_Option_DOLBYMETA_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_DOLBYMETA_Main,
            PQ_IP_TMO_TARGET_SETTING_DOLBYMETA_M_Main,
            PQ_IP_TMO_Option_DOLBYMETA_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_DOLBYMETA_Main,
            PQ_IP_TMO_TARGET_SETTING_DOLBYMETA_H_Main,
            PQ_IP_TMO_Option_DOLBYMETA_H_Main,
        },
    },
    #endif
    #if PQ_QM_VR
    {//ADOBERGB, 21
        {//Low
            PQ_IP_TMO_SOURCE_SETTING_ADOBERGB_Main,
            PQ_IP_TMO_TARGET_SETTING_ADOBERGB_L_Main,
            PQ_IP_TMO_Option_ADOBERGB_L_Main,
        },
        {//Mid
            PQ_IP_TMO_SOURCE_SETTING_ADOBERGB_Main,
            PQ_IP_TMO_TARGET_SETTING_ADOBERGB_M_Main,
            PQ_IP_TMO_Option_ADOBERGB_M_Main,
        },
        {//High
            PQ_IP_TMO_SOURCE_SETTING_ADOBERGB_Main,
            PQ_IP_TMO_TARGET_SETTING_ADOBERGB_H_Main,
            PQ_IP_TMO_Option_ADOBERGB_H_Main,
        },
    },
    #endif
};
#endif

