#define PQ_GRULE_TMO_MODE_ENABLE 1
#define PQ_GRULE_DEFINE_AUTO_GEN 1
#if (PQ_GRULE_TMO_MODE_ENABLE)
typedef enum
{
#if PQ_GRULE_TMO_MODE_ENABLE
    PQ_GRule_TMO_MODE_Main,
#endif

}
MST_GRule_TMO_Index_Main;
#endif

#if PQ_GRULE_TMO_MODE_ENABLE
typedef enum
{
    PQ_GRule_TMO_MODE_Low_Main,
    PQ_GRule_TMO_MODE_Mid_Main,
    PQ_GRule_TMO_MODE_High_Main,
}
MST_GRule_TMO_MODE_Index_Main;
#endif

#if PQ_GRULE_TMO_MODE_ENABLE
typedef enum
{
    PQ_GRule_Lvl_TMO_MODE_Low_Main,
    PQ_GRule_Lvl_TMO_MODE_Mid_Main,
    PQ_GRule_Lvl_TMO_MODE_High_Main,
}
MST_GRule_TMO_MODE_LvL_Index_Main;
#endif


#define PQ_GRULE_TMO_RULE_NUM_Main 1

#if PQ_GRULE_TMO_MODE_ENABLE
#define PQ_GRULE_TMO_MODE_IP_NUM_Main 3
#define PQ_GRULE_TMO_MODE_NUM_Main 3
#define PQ_GRULE_TMO_MODE_LVL_NUM_Main 3
#endif

#if PQ_GRULE_TMO_MODE_ENABLE
extern code U8 MST_GRule_TMO_MODE_IP_Index_Main[PQ_GRULE_TMO_MODE_IP_NUM_Main];
extern code U8 MST_GRule_TMO_MODE_Main[QM_TMO_INPUTTYPE_NUM_Main][PQ_GRULE_TMO_MODE_NUM_Main][PQ_GRULE_TMO_MODE_IP_NUM_Main];
#endif

#if PQ_GRULE_TMO_MODE_ENABLE
extern code U8 MST_GRule_Panel_1_TMO_MODE_Main[PQ_GRULE_TMO_MODE_LVL_NUM_Main];
#endif

