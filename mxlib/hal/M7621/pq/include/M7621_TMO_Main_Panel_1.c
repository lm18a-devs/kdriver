
code U8 QMAP_Panel_1_TMO_Main[QM_TMO_INPUTTYPE_NUM_Main][PQ_IP_TMO_NUM_Main]=
{
    #if PQ_QM_VR
    {//Panel_1 RESERVED0, 0
    PQ_IP_TMO_SOURCE_SETTING_RESERVED0_Main, PQ_IP_TMO_TARGET_SETTING_RESERVED0_H_Main, PQ_IP_TMO_Option_RESERVED0_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 BT709, 1
    PQ_IP_TMO_SOURCE_SETTING_BT709_Main, PQ_IP_TMO_TARGET_SETTING_BT709_H_Main, PQ_IP_TMO_Option_BT709_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 UNSPECIFIED, 2
    PQ_IP_TMO_SOURCE_SETTING_UNSPECIFIED_Main, PQ_IP_TMO_TARGET_SETTING_UNSPECIFIED_H_Main, PQ_IP_TMO_Option_UNSPECIFIED_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 RESERVED3, 3
    PQ_IP_TMO_SOURCE_SETTING_RESERVED3_Main, PQ_IP_TMO_TARGET_SETTING_RESERVED3_H_Main, PQ_IP_TMO_Option_RESERVED3_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 GAMMA2P2, 4
    PQ_IP_TMO_SOURCE_SETTING_GAMMA2P2_Main, PQ_IP_TMO_TARGET_SETTING_GAMMA2P2_H_Main, PQ_IP_TMO_Option_GAMMA2P2_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 GAMMA2P8, 5
    PQ_IP_TMO_SOURCE_SETTING_GAMMA2P8_Main, PQ_IP_TMO_TARGET_SETTING_GAMMA2P8_H_Main, PQ_IP_TMO_Option_GAMMA2P8_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 BT601525_601625, 6
    PQ_IP_TMO_SOURCE_SETTING_BT601525_601625_Main, PQ_IP_TMO_TARGET_SETTING_BT601525_601625_H_Main, PQ_IP_TMO_Option_BT601525_601625_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 SMPTE240M, 7
    PQ_IP_TMO_SOURCE_SETTING_SMPTE240M_Main, PQ_IP_TMO_TARGET_SETTING_SMPTE240M_H_Main, PQ_IP_TMO_Option_SMPTE240M_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 LINEAR, 8
    PQ_IP_TMO_SOURCE_SETTING_LINEAR_Main, PQ_IP_TMO_TARGET_SETTING_LINEAR_H_Main, PQ_IP_TMO_Option_LINEAR_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 LOG0, 9
    PQ_IP_TMO_SOURCE_SETTING_LOG0_Main, PQ_IP_TMO_TARGET_SETTING_LOG0_H_Main, PQ_IP_TMO_Option_LOG0_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 LOG1, 10
    PQ_IP_TMO_SOURCE_SETTING_LOG1_Main, PQ_IP_TMO_TARGET_SETTING_LOG1_H_Main, PQ_IP_TMO_Option_LOG1_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 XVYCC, 11
    PQ_IP_TMO_SOURCE_SETTING_XVYCC_Main, PQ_IP_TMO_TARGET_SETTING_XVYCC_H_Main, PQ_IP_TMO_Option_XVYCC_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 BT1361, 12
    PQ_IP_TMO_SOURCE_SETTING_BT1361_Main, PQ_IP_TMO_TARGET_SETTING_BT1361_H_Main, PQ_IP_TMO_Option_BT1361_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 SRGB_SYCC, 13
    PQ_IP_TMO_SOURCE_SETTING_SRGB_SYCC_Main, PQ_IP_TMO_TARGET_SETTING_SRGB_SYCC_H_Main, PQ_IP_TMO_Option_SRGB_SYCC_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 BT2020NCL, 14
    PQ_IP_TMO_SOURCE_SETTING_BT2020NCL_Main, PQ_IP_TMO_TARGET_SETTING_BT2020NCL_H_Main, PQ_IP_TMO_Option_BT2020NCL_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 BT2020CL, 15
    PQ_IP_TMO_SOURCE_SETTING_BT2020CL_Main, PQ_IP_TMO_TARGET_SETTING_BT2020CL_H_Main, PQ_IP_TMO_Option_BT2020CL_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 SMPTE2084, 16
    PQ_IP_TMO_SOURCE_SETTING_SMPTE2084_Main, PQ_IP_TMO_TARGET_SETTING_SMPTE2084_H_Main, PQ_IP_TMO_Option_SMPTE2084_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 SMPTE428, 17
    PQ_IP_TMO_SOURCE_SETTING_SMPTE428_Main, PQ_IP_TMO_TARGET_SETTING_SMPTE428_H_Main, PQ_IP_TMO_Option_SMPTE428_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 HLG, 18
    PQ_IP_TMO_SOURCE_SETTING_HLG_Main, PQ_IP_TMO_TARGET_SETTING_HLG_H_Main, PQ_IP_TMO_Option_HLG_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 BT1886, 19
    PQ_IP_TMO_SOURCE_SETTING_BT1886_Main, PQ_IP_TMO_TARGET_SETTING_BT1886_H_Main, PQ_IP_TMO_Option_BT1886_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 DOLBYMETA, 20
    PQ_IP_TMO_SOURCE_SETTING_DOLBYMETA_Main, PQ_IP_TMO_TARGET_SETTING_DOLBYMETA_H_Main, PQ_IP_TMO_Option_DOLBYMETA_H_Main, 
    },
    #endif
    #if PQ_QM_VR
    {//Panel_1 ADOBERGB, 21
    PQ_IP_TMO_SOURCE_SETTING_ADOBERGB_Main, PQ_IP_TMO_TARGET_SETTING_ADOBERGB_H_Main, PQ_IP_TMO_Option_ADOBERGB_H_Main, 
    },
    #endif
};

