
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_RESERVED0_TMO_Main[] = "RESERVED0"; //0
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_BT709_TMO_Main[] = "BT709"; //1
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_UNSPECIFIED_TMO_Main[] = "UNSPECIFIED"; //2
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_RESERVED3_TMO_Main[] = "RESERVED3"; //3
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_GAMMA2P2_TMO_Main[] = "GAMMA2P2"; //4
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_GAMMA2P8_TMO_Main[] = "GAMMA2P8"; //5
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_BT601525_601625_TMO_Main[] = "BT601525_601625"; //6
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_SMPTE240M_TMO_Main[] = "SMPTE240M"; //7
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_LINEAR_TMO_Main[] = "LINEAR"; //8
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_LOG0_TMO_Main[] = "LOG0"; //9
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_LOG1_TMO_Main[] = "LOG1"; //10
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_XVYCC_TMO_Main[] = "XVYCC"; //11
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_BT1361_TMO_Main[] = "BT1361"; //12
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_SRGB_SYCC_TMO_Main[] = "SRGB_SYCC"; //13
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_BT2020NCL_TMO_Main[] = "BT2020NCL"; //14
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_BT2020CL_TMO_Main[] = "BT2020CL"; //15
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_SMPTE2084_TMO_Main[] = "SMPTE2084"; //16
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_SMPTE428_TMO_Main[] = "SMPTE428"; //17
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_HLG_TMO_Main[] = "HLG"; //18
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_BT1886_TMO_Main[] = "BT1886"; //19
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_DOLBYMETA_TMO_Main[] = "DOLBYMETA"; //20
#endif
#if PQ_QM_VR
static code char TEXT_INPUTTYPE_ADOBERGB_TMO_Main[] = "ADOBERGB"; //21
#endif


code char* PQ_INPUTTYPE_TEXT_TMO_Main[]=
{
    #if PQ_QM_VR
    TEXT_INPUTTYPE_RESERVED0_TMO_Main,//0
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_BT709_TMO_Main,//1
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_UNSPECIFIED_TMO_Main,//2
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_RESERVED3_TMO_Main,//3
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_GAMMA2P2_TMO_Main,//4
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_GAMMA2P8_TMO_Main,//5
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_BT601525_601625_TMO_Main,//6
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_SMPTE240M_TMO_Main,//7
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_LINEAR_TMO_Main,//8
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_LOG0_TMO_Main,//9
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_LOG1_TMO_Main,//10
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_XVYCC_TMO_Main,//11
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_BT1361_TMO_Main,//12
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_SRGB_SYCC_TMO_Main,//13
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_BT2020NCL_TMO_Main,//14
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_BT2020CL_TMO_Main,//15
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_SMPTE2084_TMO_Main,//16
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_SMPTE428_TMO_Main,//17
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_HLG_TMO_Main,//18
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_BT1886_TMO_Main,//19
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_DOLBYMETA_TMO_Main,//20
    #endif 
    #if PQ_QM_VR
    TEXT_INPUTTYPE_ADOBERGB_TMO_Main,//21
    #endif 
};

