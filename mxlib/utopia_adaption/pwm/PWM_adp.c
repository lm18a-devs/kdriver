#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>


#include "MsTypes.h"
#include "utopia.h"
#include "drvPWM.h"
#include "drvPWM_v2.h"
#include "PWM_adp.h"
#include "utopia_adp.h"

#include <linux/uaccess.h>

// no pointer member
UADP_SPT_0NXT_DEF(PWM_INIT_PARAM);
UADP_SPT_0NXT_DEF(PWM_OEN_PARAM);
UADP_SPT_0NXT_DEF(PWM_PERIOD_PARAM);
UADP_SPT_0NXT_DEF(PWM_DUTYCYCLE_PARAM);
UADP_SPT_0NXT_DEF(PWM_VDBEN_PARAM);
UADP_SPT_0NXT_DEF(PWM_RESETEN_PARAM);
UADP_SPT_0NXT_DEF(PWM_DBEN_PARAM);
UADP_SPT_0NXT_DEF(PWM_SHITF_PARAM);
UADP_SDT_0_DEF(PWM_PARAM_PRO_VAR);
UADP_SDT_1_DEF(PWM_PARAM_GET_PRO);
// one pointer member
// two pointer member
MS_U32 PWM_adp_Init(FUtopiaIOctl* pIoctl)
{
//member of struct
//set table
    UADP_SPT_0NXT(PWM_INIT_PARAM);
    UADP_SPT_0NXT(PWM_OEN_PARAM);
    UADP_SPT_0NXT(PWM_PERIOD_PARAM);
    UADP_SPT_0NXT(PWM_DUTYCYCLE_PARAM);
    UADP_SPT_0NXT(PWM_VDBEN_PARAM);
    UADP_SPT_0NXT(PWM_RESETEN_PARAM);
    UADP_SPT_0NXT(PWM_DBEN_PARAM);
    UADP_SPT_0NXT(PWM_SHITF_PARAM);

    UADP_SDT_NAME0(PWM_PARAM_PRO_VAR,MS_U8);
    UADP_SDT_NAME1(PWM_PARAM_GET_PRO,PWM_GET_PROPERTY_PARAM,UADP_SDT_P2N,u32PWMVar,PWM_PARAM_PRO_VAR);
    *pIoctl= (FUtopiaIOctl)PWM_adp_Ioctl;
    return 0;
}

MS_U32 PWM_adp_Ioctl(void* pInstanceTmp, MS_U32 u32Cmd, void* const pArgs)
{
    MS_U32 u32Ret=0;
    char buffer_arg[1];
    /* buffer_arg is not unnecessary
    therefor, assign 1 to buffer_arg */
    switch(u32Cmd)
    {
     case MDrv_CMD_PWM_Init:
          u32Ret=UADPBypassIoctl(pInstanceTmp,u32Cmd,pArgs,spt_PWM_INIT_PARAM, spt_PWM_INIT_PARAM,buffer_arg,sizeof(buffer_arg));
     break;
     case MDrv_CMD_PWM_Oen:
          u32Ret=UADPBypassIoctl(pInstanceTmp,u32Cmd,pArgs,spt_PWM_OEN_PARAM, spt_PWM_OEN_PARAM,buffer_arg,sizeof(buffer_arg));
     break;
     case MDrv_CMD_PWM_Period:
          u32Ret=UADPBypassIoctl(pInstanceTmp,u32Cmd,pArgs,spt_PWM_PERIOD_PARAM, spt_PWM_PERIOD_PARAM,buffer_arg,sizeof(buffer_arg));
     break;
     case MDrv_CMD_PWM_DutyCycle:
          u32Ret=UADPBypassIoctl(pInstanceTmp,u32Cmd,pArgs,spt_PWM_DUTYCYCLE_PARAM, spt_PWM_DUTYCYCLE_PARAM,buffer_arg,sizeof(buffer_arg));
     break;
     case MDrv_CMD_PWM_Vdben:
          UADPBypassIoctl(pInstanceTmp,u32Cmd,pArgs,spt_PWM_VDBEN_PARAM, spt_PWM_VDBEN_PARAM,buffer_arg,sizeof(buffer_arg));
     break;
     case MDrv_CMD_PWM_ResetEn:
          UADPBypassIoctl(pInstanceTmp,u32Cmd,pArgs,spt_PWM_RESETEN_PARAM, spt_PWM_RESETEN_PARAM,buffer_arg,sizeof(buffer_arg));
     break;
     case MDrv_CMD_PWM_Dben:
          UADPBypassIoctl(pInstanceTmp,u32Cmd,pArgs,spt_PWM_DBEN_PARAM, spt_PWM_DBEN_PARAM,buffer_arg,sizeof(buffer_arg));
     break;
     case MDrv_CMD_PWM_Shift:
          u32Ret=UADPBypassIoctl(pInstanceTmp,u32Cmd,pArgs,spt_PWM_SHITF_PARAM, spt_PWM_SHITF_PARAM,buffer_arg,sizeof(buffer_arg));
     break;
     case MDrv_CMD_PWM_GetProperty:
          u32Ret=UADPBypassIoctl(pInstanceTmp,u32Cmd,pArgs,spt_PWM_PARAM_GET_PRO, spt_PWM_PARAM_GET_PRO,buffer_arg,sizeof(buffer_arg));
     break;
     default:
     break;
    }
     return u32Ret;
}