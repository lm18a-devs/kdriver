#ifndef _DRVIRTX_H_
#define _DRVIRTX_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "MsTypes.h"
#include "MsDevice.h"

typedef enum _IRTX_Result
{
    IRTX_FAIL = 0
    ,IRTX_OK = 1
    ,IRTX_TIMEOUT
    ,IRTX_QUEUE_FULL
    ,IRTX_BUSY
}IRTX_Result;



MS_BOOL MDrv_IR_TX_Trigger(void);
MS_BOOL MDrv_IR_TX_SetStatus(MS_U16 Status);
MS_BOOL MDrv_IR_TX_SetMemStatus(MS_U16 Status);
MS_BOOL MDrv_IR_TX_SetClkDiv(MS_U8 Div);
MS_BOOL MDrv_IR_TX_SetDelayCycleTime(MS_U16 CycleTime_H, MS_U16 CycleTime_L);
MS_BOOL MDrv_IR_TX_SetMemAddr(MS_U16 MemAddr);
MS_BOOL MDrv_IR_TX_SetMemData(MS_U16 MemData);
MS_BOOL MDrv_IR_TX_SetUnitValue(MS_U16 Unit_Value, MS_U8 Unit_Number);
MS_BOOL MDrv_IR_TX_SetShotCount(MS_U16 H_ShotCount, MS_U16 L_ShotCount, MS_U8 Unit_Number);
MS_BOOL MDrv_IR_TX_Init(void);
MS_BOOL MDrv_IR_TX_SetCarrierCount(MS_U16 Count);


#ifdef __cplusplus
}
#endif
#endif
