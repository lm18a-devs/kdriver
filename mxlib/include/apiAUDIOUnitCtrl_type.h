//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// @file   apiAUDIOUnitCtrl_type.h
/// @brief  all type for api/drv/hal AUDIOUnitCtrl.c
/// @author MStar Semiconductor Inc.
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _API_AUDIO_UNIT_TYPE_H_
#define _API_AUDIO_UNIT_TYPE_H_

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#include standard header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/module.h>  // Needed by all modules
#include <linux/kernel.h>  // Needed for KERN_INFO
#include <linux/fs.h>      // Needed by filp
#include <asm/uaccess.h>   // Needed by segment descriptors
#include <linux/proc_fs.h>
#include <linux/io.h>

#else //User Space
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <pthread.h>
#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [#include local header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#include "MsCommon.h"
#include "MsTypes.h"
#include "MsOS.h"

#ifdef __cplusplus
extern "C"
{
#endif

//-------------------------------------------------------------------------------------------------------------------------------------
/* Initialize, STR */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Connect & Disconnect */
//-------------------------------------------------------------------------------------------------------------------------------------
typedef enum
{
    AUDIO_UNIT_PARSER_MODE_INVALID = -1,
    AUDIO_UNIT_PARSER_MODE_0,
    AUDIO_UNIT_PARSER_MODE_1,
    AUDIO_UNIT_PARSER_MODE_2,
    AUDIO_UNIT_PARSER_MODE_3,
    AUDIO_UNIT_PARSER_MODE_MAX,
} AUDIO_UNIT_PARSER_MODE;

typedef enum
{
    AUDIO_UNIT_PARSER_INPUT_INVALID = -1,
    AUDIO_UNIT_PARSER_INPUT_AFIFO0,
    AUDIO_UNIT_PARSER_INPUT_AFIFO1,
    AUDIO_UNIT_PARSER_INPUT_AFIFO2,
    AUDIO_UNIT_PARSER_INPUT_AFIFO3,
    AUDIO_UNIT_PARSER_INPUT_MAX,
} AUDIO_UNIT_PARSER_INPUT;

typedef enum
{
    AUDIO_UNIT_ADEC_INDEX_INVALID = -1,
    AUDIO_UNIT_ADEC0,
    AUDIO_UNIT_ADEC1,
    AUDIO_UNIT_ADEC_ATV,
    AUDIO_UNIT_ADEC_MAX,
} AUDIO_UNIT_ADEC_INDEX;

typedef enum
{
    AUDIO_UNIT_ADEC_INPUT_INVALID   = -1,
    AUDIO_UNIT_ADEC_INPUT_DTV       = 0,
    AUDIO_UNIT_ADEC_INPUT_HDMI      = 1,
    AUDIO_UNIT_ADEC_INPUT_MM        = 2,
    AUDIO_UNIT_ADEC_INPUT_MM_UNI    = 6, //<--MAX is here!!!
    AUDIO_UNIT_ADEC_INPUT_CLIP      = 3,
    AUDIO_UNIT_ADEC_INPUT_ATV       = 4,
    AUDIO_UNIT_ADEC_INPUT_NONE      = 5,
    AUDIO_UNIT_ADEC_INPUT_MAX       = AUDIO_UNIT_ADEC_INPUT_MM_UNI + 1,
} AUDIO_UNIT_ADEC_INPUT;

typedef enum
{
    AUDIO_UNIT_ADEC_PCM_PATH_INVALID = -1,
    AUDIO_UNIT_ADEC_PCM_PATH_DSP_SRC,
    AUDIO_UNIT_ADEC_PCM_PATH_MCU,
    AUDIO_UNIT_ADEC_PCM_PATH_MAX,
} AUDIO_UNIT_ADEC_PCM_PATH;

typedef enum
{
    AUDIO_UNIT_ADC_INDEX_INVALID   = -1,
    AUDIO_UNIT_ADC0,
    AUDIO_UNIT_ADC1,
    AUDIO_UNIT_ADC_MAX,
} AUDIO_UNIT_ADC_INDEX;

typedef enum
{
    AUDIO_UNIT_ADC_IN_PORT_INVALID  = -1,
    AUDIO_UNIT_ADC0_IN_PORT_0       = 0x02,
    AUDIO_UNIT_ADC0_IN_PORT_1       = 0x12,
    AUDIO_UNIT_ADC0_IN_PORT_2       = 0x22,
    AUDIO_UNIT_ADC0_IN_PORT_3       = 0x32,
    AUDIO_UNIT_ADC0_IN_PORT_4       = 0xA2,
    AUDIO_UNIT_ADC0_IN_PORT_5       = 0xB2,
    AUDIO_UNIT_ADC0_MIC_IN          = 0x72,
    AUDIO_UNIT_ADC1_IN_PORT_0       = 0x09,
    AUDIO_UNIT_ADC1_IN_PORT_1       = 0x19,
    AUDIO_UNIT_ADC1_IN_PORT_2       = 0x29,
    AUDIO_UNIT_ADC1_IN_PORT_3       = 0x39,
    AUDIO_UNIT_ADC1_IN_PORT_4       = 0xA9,
    AUDIO_UNIT_ADC1_IN_PORT_5       = 0xB9,
    AUDIO_UNIT_ADC1_MIC_IN          = 0x79,
} AUDIO_UNIT_ADC_IN_PORT;

typedef enum
{
    AUDIO_UNIT_PCM_MIXER_INDEX_INVALID = -1,
    AUDIO_UNIT_PCM_MIXER0,
    AUDIO_UNIT_PCM_MIXER_MAX,
} AUDIO_UNIT_PCM_MIXER_INDEX;

typedef enum
{
    AUDIO_UNIT_PCM_MIXER_INPUT_INVALID = -1,
    AUDIO_UNIT_PCM_MIXER_INPUT_AMIX0,
    AUDIO_UNIT_PCM_MIXER_INPUT_AMIX1,
    AUDIO_UNIT_PCM_MIXER_INPUT_AMIX2,
    AUDIO_UNIT_PCM_MIXER_INPUT_AMIX3,
    AUDIO_UNIT_PCM_MIXER_INPUT_AMIX4,
    AUDIO_UNIT_PCM_MIXER_INPUT_AMIX5,
    AUDIO_UNIT_PCM_MIXER_INPUT_AMIX6,
    AUDIO_UNIT_PCM_MIXER_INPUT_AMIX7,
    AUDIO_UNIT_PCM_MIXER_INPUT_MAX,
} AUDIO_UNIT_PCM_MIXER_INPUT;

typedef enum
{
    AUDIO_UNIT_CH_SOUND_INVALID = -1,
    AUDIO_UNIT_CH1_SOUND        = 4,
    AUDIO_UNIT_CH2_SOUND        = 5,
    AUDIO_UNIT_CH3_SOUND        = 6,
    AUDIO_UNIT_CH4_SOUND        = 7, //<--MAX is here!!!
    AUDIO_UNIT_CH5_SOUND        = 0,
    AUDIO_UNIT_CH6_SOUND        = 1,
    AUDIO_UNIT_CH7_SOUND        = 2,
    AUDIO_UNIT_CH8_SOUND        = 3,
    AUDIO_UNIT_CH_SOUND_MAX     = AUDIO_UNIT_CH4_SOUND + 1,
} AUDIO_UNIT_CH_SOUND;

typedef enum
{
    AUDIO_UNIT_CH_INPUT_INVALID = -1,
    AUDIO_UNIT_CH_INPUT_ADEC0,
    AUDIO_UNIT_CH_INPUT_ADEC1,
    AUDIO_UNIT_CH_INPUT_ADEC_ATV,
    AUDIO_UNIT_CH_INPUT_HDMI,
    AUDIO_UNIT_CH_INPUT_ADC0,
    AUDIO_UNIT_CH_INPUT_ADC1,
    AUDIO_UNIT_CH_INPUT_SPDIF,
    AUDIO_UNIT_CH_INPUT_R2DMA_DSP1,
    AUDIO_UNIT_CH_INPUT_R2DMA_DSP3,
    AUDIO_UNIT_CH_INPUT_SWDMA_DSP3,
    AUDIO_UNIT_CH_INPUT_HWDMA,
    AUDIO_UNIT_CH_INPUT_NONE,
    AUDIO_UNIT_CH_INPUT_MAX,
} AUDIO_UNIT_CH_INPUT;

typedef  enum
{
    AUDIO_UNIT_FWM_INVALID = -1,
    AUDIO_UNIT_FWM0,
    AUDIO_UNIT_FWM1,
    AUDIO_UNIT_FWM2,
    AUDIO_UNIT_FWM_MAX,
} AUDIO_UNIT_FWM_INDEX;

typedef  enum
{
    AUDIO_UNIT_FWM_INPUT_INVALID = -1,
    AUDIO_UNIT_FWM_INPUT_CH1     = 4,
    AUDIO_UNIT_FWM_INPUT_CH2     = 5,
    AUDIO_UNIT_FWM_INPUT_CH3     = 6,
    AUDIO_UNIT_FWM_INPUT_CH4     = 7, //<--MAX is here!!!
    AUDIO_UNIT_FWM_INPUT_CH5     = 0,
    AUDIO_UNIT_FWM_INPUT_CH6     = 1,
    AUDIO_UNIT_FWM_INPUT_CH7     = 2,
    AUDIO_UNIT_FWM_INPUT_CH8     = 3,
    AUDIO_UNIT_FWM_INPUT_MAX     = AUDIO_UNIT_FWM_INPUT_CH4 + 1,
} AUDIO_UNIT_FWM_INPUT;

typedef  enum
{
    AUDIO_UNIT_SE_INVALID = -1,
    AUDIO_UNIT_SE_PRER2,
    AUDIO_UNIT_SE_DSPSE,
    AUDIO_UNIT_SE_POSTR2,
    AUDIO_UNIT_SE_MAX,
} AUDIO_UNIT_SE_INDEX;

typedef  enum
{
    AUDIO_UNIT_SE_INPUT_INVALID = -1,
    AUDIO_UNIT_SE_INPUT_DLY0,
    AUDIO_UNIT_SE_INPUT_DLY1,
    AUDIO_UNIT_SE_INPUT_MAX,
} AUDIO_UNIT_SE_INPUT;

typedef enum
{
    AUDIO_UNIT_SOUNDOUT_INVALID = -1,
    AUDIO_UNIT_SOUNDOUT_I2S,
    AUDIO_UNIT_SOUNDOUT_LINEOUT0,
    AUDIO_UNIT_SOUNDOUT_LINEOUT1,
    AUDIO_UNIT_SOUNDOUT_LINEOUT2,
    AUDIO_UNIT_SOUNDOUT_LINEOUT3,
    AUDIO_UNIT_SOUNDOUT_SPDIF,
    AUDIO_UNIT_SOUNDOUT_HDMI_ARC,
    AUDIO_UNIT_SOUNDOUT_HDMI_TX,
    AUDIO_UNIT_SOUNDOUT_MAX,
} AUDIO_UNIT_SOUNDOUT_INDEX;

typedef enum
{
    AUDIO_UNIT_SOUNDOUT_INPUT_INVALID       = -1,
    AUDIO_UNIT_SOUNDOUT_INPUT_NULL          = 0,
    AUDIO_UNIT_SOUNDOUT_INPUT_CH7           = 1,
    AUDIO_UNIT_SOUNDOUT_INPUT_FWM0          = 2,
    AUDIO_UNIT_SOUNDOUT_INPUT_DLY0          = 3,
    AUDIO_UNIT_SOUNDOUT_INPUT_FWM1          = 4,
    AUDIO_UNIT_SOUNDOUT_INPUT_DLY1          = 5,
    AUDIO_UNIT_SOUNDOUT_INPUT_FWM2          = 6,
    AUDIO_UNIT_SOUNDOUT_INPUT_DLY2          = 7,
    AUDIO_UNIT_SOUNDOUT_INPUT_AUDIO_DELAY   = 11, //<--MAX is here!!!
    AUDIO_UNIT_SOUNDOUT_INPUT_PRER2         = 8,
    AUDIO_UNIT_SOUNDOUT_INPUT_DSPSE         = 9,
    AUDIO_UNIT_SOUNDOUT_INPUT_POSTR2        = 10,
    AUDIO_UNIT_SOUNDOUT_INPUT_MAX           = AUDIO_UNIT_SOUNDOUT_INPUT_AUDIO_DELAY + 1,
} AUDIO_UNIT_SOUNDOUT_INPUT;

typedef enum
{
    AUDIO_UNIT_PCM_CAPTURE_INVALID = -1,
    AUDIO_UNIT_PCM_CAPTURE0,
    AUDIO_UNIT_PCM_CAPTURE1,
    AUDIO_UNIT_PCM_CAPTURE2,
    AUDIO_UNIT_PCM_CAPTURE_MAX,
} AUDIO_UNIT_PCM_CAPTURE_INDEX;

typedef enum
{
    AUDIO_UNIT_PCM_CAPTURE_INPUT_INVALID    = -1,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_CH1        = 11,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_CH2        = 12,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_CH3        = 13,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_CH4        = 14, //<--MAX is here!!!
    AUDIO_UNIT_PCM_CAPTURE_INPUT_CH5        = 0,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_CH6        = 1,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_CH7        = 2,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_CH8        = 3,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_PCM        = 4,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_PCM_DELAY  = 5,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_PCM_SE     = 6,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_MIXER      = 7,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_FWM0       = 8,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_FWM1       = 9,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_FWM2       = 10,
    AUDIO_UNIT_PCM_CAPTURE_INPUT_MAX        = AUDIO_UNIT_PCM_CAPTURE_INPUT_CH4 + 1,
} AUDIO_UNIT_PCM_CAPTURE_INPUT;

typedef enum
{
    AUDIO_UNIT_MP3_ENC_INVALID = -1,
    AUDIO_UNIT_MP3_ENC0,
    AUDIO_UNIT_MP3_ENC_MAX,
} AUDIO_UNIT_MP3_ENC_INDEX;

typedef enum
{
    AUDIO_UNIT_MP3_ENC_INPUT_INVALID = -1,
    AUDIO_UNIT_MP3_ENC_INPUT_CH5,
    AUDIO_UNIT_MP3_ENC_INPUT_CH6,
    AUDIO_UNIT_MP3_ENC_INPUT_MAX,
} AUDIO_UNIT_MP3_ENC_INPUT;

typedef enum
{
    AUDIO_UNIT_AAC_ENC_INVALID = -1,
    AUDIO_UNIT_AAC_ENC0,
    AUDIO_UNIT_AAC_ENC_MAX,
} AUDIO_UNIT_AAC_ENC_INDEX;

typedef enum
{
    AUDIO_UNIT_AAC_ENC_INPUT_INVALID = -1,
    AUDIO_UNIT_AAC_ENC_INPUT_PCM_CAPTURE0,
    AUDIO_UNIT_AAC_ENC_INPUT_PCM_CAPTURE1,
    AUDIO_UNIT_AAC_ENC_INPUT_MAX,
} AUDIO_UNIT_AAC_ENC_INPUT;

//-------------------------------------------------------------------------------------------------------------------------------------
/* Start & Stop */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* SPDIF */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* HDMI */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* ATV */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Decoder */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Common Decoder */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Common Cmd */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Common */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Customized patch */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Clip Play for ES */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Clip Play for PCM */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Gain, Mute & Delay */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* AENC */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* PCM(Sound Bar Buletooth, PCM Capture) */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* PCM IO */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* MM New Mode */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Mstar Sound Effect */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* Advanced Sound Effect */
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
/* callback functions */
//-------------------------------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------
// [Dbg] for SET
//-----------------------------------------------------------------------------------
typedef struct
{
    MS_U32 Dbg_Param;
    MS_U32 Dbg_Param2;
    MS_U32 Dbg_Param3;
    MS_U32 Dbg_Param4;
    MS_U32 Dbg_Param5;
    MS_U32 Dbg_Param6;
    MS_U32 Dbg_Param7;
    MS_U32 Dbg_Param8;
    MS_U32 Dbg_Param9;
    MS_U32 Dbg_Param10;
} AUDIO_UNIT_DEBUG_PARAM;

//-----------------------------------------------------------------------------------
// [Dbg] for GET
//-----------------------------------------------------------------------------------
typedef struct
{
    MS_U32 Dbg_Info;
    MS_U32 Dbg_Info2;
    MS_U32 Dbg_Info3;
    MS_U32 Dbg_Info4;
    MS_U32 Dbg_Info5;
    MS_U32 Dbg_Info6;
    MS_U32 Dbg_Info7;
    MS_U32 Dbg_Info8;
    MS_U32 Dbg_Info9;
    MS_U32 Dbg_Info10;
} AUDIO_UNIT_DEBUG_INFO;

//Header-free api structure
typedef struct
{
    const char *cmd;
    void *pData;
} St_Audio_Unit_Tunnel;

#ifdef __cplusplus
}
#endif

#endif // _API_AUDIO_UNIT_TYPE_H_
