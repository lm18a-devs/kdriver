//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    apiAUDIOUnitCtrl.c
/// @brief  AUDIO "Unit" Control WITHOUT COMBO SKILL!!!!
/// @author MStar Semiconductor,Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined(ROLLS_ROYCE) || defined(AUDIO_UNIT_CTRL)

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#include standard header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [#include local header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#include "apiAUDIOUnitCtrl.h"
#include "drvAUDIOUnitCtrl.h"
#include "drvAUDIO_if.h"
#include "../drv/audio/internal/drvAUDIO_internal.h"
#include "../drv/audio/internal/drvAUDIOUnitCtrl_config.h"

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#include local library] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [#define] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#define API_AUDIO_UNIT_VERSION                         0x000D
#define API_AUDIO_UNIT_DBG_LOG_TO_FILE_PATH            "/tmp/AudioCUSnDBGLog.log"
#define API_AUDIO_UNIT_DBG_MENULOG_TO_FILE_PATH        "/tmp/AudioCUSnDBGMenuLog.log"
#define API_AUDIO_UNIT_UNUSED(x)                       ((x)=(x))

//AU debug get string input
#define API_AUDIO_UNIT_DEBUG_INPUT_STR_LENGTH  255
#define SEEK_CUR    1
#define SEEK_END    2
#define SEEK_SET    0

#ifdef  CONFIG_MBOOT
#define fflush(x)           //define for mboot compile error
#define fwrite(a,b,c,d)     //define for mboot compile error
#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#define] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
#define ApiAudioUnitDBG(_f,_a...)                                                                                                               \
    {                                                                                                                                           \
        if(pstAudioUnitShmData->g_audio_unit_bEnableNonThreadPrintMsg)                                                                          \
        {                                                                                                                                       \
            printf("\033[1;35m" "[A]" "[%ld]" "[%s]" "[%d]" _f "\033[0m", (unsigned long)MsOS_GetSystemTime(), __FUNCTION__, __LINE__, ##_a);   \
        }                                                                                                                                       \
    }

#define ApiAudioUnitPRINT(_f,_a...)             \
    {                                           \
        printf("\033[1;35m"_f"\033[0m", ##_a);  \
    }

#define ApiAudioUnitThreadDBG(_f,_a...)                                                                                                         \
    {                                                                                                                                           \
        if( (pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg) && (pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg) )                  \
        {                                                                                                                                       \
            printf("\033[0;33m" "[A]" "[%ld]" "[%s]" "[%d]" _f "\033[0m", (unsigned long)MsOS_GetSystemTime(), __FUNCTION__, __LINE__, ##_a);   \
        }                                                                                                                                       \
    }


/* kernel debug proc */
#define API_AUDIO_UNIT_KERNEL_DEBUG_PROC_STRING_BUFFER_LENGTH             128
#define API_AUDIO_UNIT_KERNEL_DEBUG_PROC_ROOT_NAME                        "MstarAudioUnitCtrlDebug"
#define API_AUDIO_UNIT_KERNEL_DEBUG_PROC_MAX_CHILD                        2 //<--Remember add one when add new proc
#define API_AUDIO_UNIT_KERNEL_DEBUG_PROC_CHILD_0001_NDBG                  "0001_nDBG"          /* proc child 1  */
#define API_AUDIO_UNIT_KERNEL_DEBUG_PROC_CHILD_0002_SHOW_VERSION          "0002_ShowVersion"   /* proc child 2  */

#else //User Space
#define SYS_SHM_QUERY          0x00000000UL
#define SYS_SHM_CREATE         0x00000001UL

#ifdef CONFIG_MBOOT
#define ApiAudioUnitDBG(_f,_a...)
#define ApiAudioUnitPRINT(_f,_a...)
#define ApiAudioUnitThreadDBG(_f,_a...)
#else
#define ApiAudioUnitDBG(_f,_a...)                                                                                                                                               \
    {                                                                                                                                                                           \
        if(pstAudioUnitShmData->g_audio_unit_bEnableNonThreadPrintMsg)                                                                                                          \
        {                                                                                                                                                                       \
            printf("\033[1;35m" "[A]" "[%ld]" "[%s]" "[%d]" _f "\033[0m", (unsigned long)MsOS_GetSystemTime(), __FUNCTION__, __LINE__, ##_a);                                   \
        }                                                                                                                                                                       \
        if(pApiAudioUnit_DBG_Log_To_File)                                                                                                                                       \
        {                                                                                                                                                                       \
            fprintf(pApiAudioUnit_DBG_Log_To_File, "\033[1;35m" "[A]" "[%ld]" "[%s]" "[%d]"_f "\033[0m" , (unsigned long)MsOS_GetSystemTime(), __FUNCTION__, __LINE__, ##_a);   \
            fflush(pApiAudioUnit_DBG_Log_To_File);                                                                                                                              \
        }                                                                                                                                                                       \
    }

#define ApiAudioUnitPRINT(_f,_a...)                                 \
    {                                                               \
        printf("\033[1;35m"_f"\033[0m", ##_a);                      \
        if(pApiAudioUnit_DBG_MenuLog_To_File)                       \
        {                                                           \
            fprintf(pApiAudioUnit_DBG_MenuLog_To_File, _f, ##_a);   \
            fflush(pApiAudioUnit_DBG_MenuLog_To_File);              \
        }                                                           \
    }

#define ApiAudioUnitThreadDBG(_f,_a...)                                                                                                                                         \
    {                                                                                                                                                                           \
        if( (pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg) && (pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg) )                                                  \
        {                                                                                                                                                                       \
            if(__LINE__ != pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Line)                                                                                              \
            {                                                                                                                                                                   \
                printf("\033[0;33m" "[A]" "[%ld]" "[%s]" "[%d]" _f "\033[0m", (unsigned long)MsOS_GetSystemTime(), __FUNCTION__, __LINE__, ##_a);                               \
                pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Line = __LINE__;                                                                                              \
            }                                                                                                                                                                   \
        }                                                                                                                                                                       \
        if((pApiAudioUnit_DBG_Log_To_File) && (pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg) && (pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg))                 \
        {                                                                                                                                                                       \
            fprintf(pApiAudioUnit_DBG_Log_To_File, "\033[0;33m" "[A]" "[%ld]" "[%s]" "[%d]"_f "\033[0m", (unsigned long)MsOS_GetSystemTime(), __FUNCTION__, __LINE__, ##_a);    \
            fflush(pApiAudioUnit_DBG_Log_To_File);                                                                                                                              \
        }                                                                                                                                                                       \
    }
#endif

#define API_AUDIO_UNIT_NON_SCANF_DEBUG_CMD_PARAMS_MAX 30

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [variable / struct / enum] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
typedef struct
{
    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Initialize, STR */
    //-------------------------------------------------------------------------------------------------------------------------------------
    //Audio init flag
    MS_BOOL g_audio_unit_Init_Done;
    MS_BOOL g_audio_unit_STR_bSuspendStart;
    MS_BOOL g_audio_unit_STR_bResumeFinish;
    MS_BOOL g_audio_unit_scanf_bSupport;

    //Dynamic Enable all apiAUDIO_v2_customer debug msg
    MS_BOOL g_audio_unit_bEnableNonThreadPrintMsg;
    MS_BOOL g_audio_unit_bEnableThreadPrintMsg;
    MS_U32  g_audio_unit_bEnableThreadPrintMsg_Interval;
    MS_BOOL g_audio_unit_bThreadPrintMsg;
    MS_U32  g_audio_unit_bThreadPrintMsg_Interval;
    MS_U32  g_audio_unit_bThreadPrintMsg_Line;

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Connect & Disconnect */
    //-------------------------------------------------------------------------------------------------------------------------------------
    //Parser connect
    AUDIO_UNIT_PARSER_MODE  g_audio_unit_PARSER_Mode;
    AUDIO_UNIT_PARSER_INPUT g_audio_unit_PARSER_A_Connect;
    AUDIO_UNIT_PARSER_INPUT g_audio_unit_PARSER_B_Connect;
    AUDIO_UNIT_PARSER_INPUT g_audio_unit_PARSER_C_Connect;
    AUDIO_UNIT_PARSER_INPUT g_audio_unit_PARSER_D_Connect;

    //ADEC connect
    AUDIO_UNIT_ADEC_INPUT g_audio_unit_ADEC0_Connect;
    AUDIO_UNIT_ADEC_INPUT g_audio_unit_ADEC1_Connect;
    AUDIO_UNIT_ADEC_INPUT g_audio_unit_ADEC_ATV_Connect;

    //ADC connect
    AUDIO_UNIT_ADC_IN_PORT g_audio_unit_ADC0_Connect;
    AUDIO_UNIT_ADC_IN_PORT g_audio_unit_ADC1_Connect;

    //PCM Mixer connect / ID
    MS_BOOL g_audio_unit_PCM_Mixer_Connect[AUDIO_UNIT_PCM_MIXER_INPUT_MAX];
    int     g_audio_unit_PCM_Mixer_ID[AUDIO_UNIT_PCM_MIXER_INPUT_MAX];

    //Channel Sound Connect
    AUDIO_UNIT_CH_INPUT g_audio_unit_CH5_Sound_Connect;
    AUDIO_UNIT_CH_INPUT g_audio_unit_CH6_Sound_Connect;
    AUDIO_UNIT_CH_INPUT g_audio_unit_CH7_Sound_Connect;
    AUDIO_UNIT_CH_INPUT g_audio_unit_CH8_Sound_Connect;

    //FW Mixer Connect
    MS_BOOL g_audio_unit_FW_Mixer0_Connect[AUDIO_UNIT_FWM_INPUT_MAX];
    MS_BOOL g_audio_unit_FW_Mixer1_Connect[AUDIO_UNIT_FWM_INPUT_MAX];
    MS_BOOL g_audio_unit_FW_Mixer2_Connect[AUDIO_UNIT_FWM_INPUT_MAX];

    //SE(PreR2 / SeDSP / PostR2) connect
    AUDIO_UNIT_SE_INPUT g_audio_unit_SE_PreR2_Connect;
    AUDIO_UNIT_SE_INPUT g_audio_unit_SE_DspSE_Connect;
    AUDIO_UNIT_SE_INPUT g_audio_unit_SE_PostR2_Connect;

    //SOUND OUT connect
    AUDIO_UNIT_SOUNDOUT_INPUT g_audio_unit_Sound_Out_I2S_Connect;
    AUDIO_UNIT_SOUNDOUT_INPUT g_audio_unit_Sound_Out_Line0_Connect;
    AUDIO_UNIT_SOUNDOUT_INPUT g_audio_unit_Sound_Out_Line1_Connect;
    AUDIO_UNIT_SOUNDOUT_INPUT g_audio_unit_Sound_Out_Line2_Connect;
    AUDIO_UNIT_SOUNDOUT_INPUT g_audio_unit_Sound_Out_Line3_Connect;
    AUDIO_UNIT_SOUNDOUT_INPUT g_audio_unit_Sound_Out_SPDIF_Connect;
    AUDIO_UNIT_SOUNDOUT_INPUT g_audio_unit_Sound_Out_ARC_Connect;
    AUDIO_UNIT_SOUNDOUT_INPUT g_audio_unit_Sound_Out_HDMI_Connect;

    //PCM Capture connect
    AUDIO_UNIT_PCM_CAPTURE_INPUT g_audio_unit_PCM_CAPTURE0_Connect;
    AUDIO_UNIT_PCM_CAPTURE_INPUT g_audio_unit_PCM_CAPTURE1_Connect;
    AUDIO_UNIT_PCM_CAPTURE_INPUT g_audio_unit_PCM_CAPTURE2_Connect;

    //MP3 ENC connect
    AUDIO_UNIT_MP3_ENC_INPUT g_audio_unit_MP3_ENC_Connect;

    //AAC ENC connect
    AUDIO_UNIT_AAC_ENC_INPUT g_audio_unit_AAC_ENC_Connect;

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Start & Stop */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* SPDIF */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* HDMI */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* ATV */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Decoder */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Common */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Customized patch */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Customized Internal patch */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Clip Play for ES */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Clip Play for PCM */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Gain, Mute & Delay */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* AENC */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* PCM Capture */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* PCM IO Control */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* MM New Mode */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Mstar Sound Effect */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Advanced Sound Effect */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Thread monitor Enable */
    //-------------------------------------------------------------------------------------------------------------------------------------
    MS_BOOL g_audio_unit_ThreadPrintMsg_Monitor_bEnable;

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Thread monitor interval */
    //-------------------------------------------------------------------------------------------------------------------------------------

} St_Audio_Unit_Shm_data;

static St_Audio_Unit_Shm_data *pstAudioUnitShmData = NULL;

//---Shm in orginal utopia---
extern AUDIO_SHARED_VARS2 * g_AudioVars2;

//---Debug cmd---
typedef enum
{
    En_ApiAudioUnit_cmdIdx_DebugMenu,
    En_ApiAudioUnit_cmdIdx_Max,       //maximum commnad index
}EN_API_AUDIO_CMD_IDX;

typedef struct
{
    char cCommmandName[64];
    EN_API_AUDIO_CMD_IDX u32CommmandIndex;
} API_AUDIO_COMMAND_ST;

const API_AUDIO_COMMAND_ST API_AUDIO_COMMNAD_INFO[] =
{
    {{"ApiAudioUnit_DebugMenu"} , En_ApiAudioUnit_cmdIdx_DebugMenu},
};

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [variable / struct / enum] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
//file for nDBG log
struct file *pApiAudioUnit_DBG_Log_To_File      = NULL;
struct file *pApiAudioUnit_DBG_MenuLog_To_File  = NULL;

//Shm Data
static St_Audio_Unit_Shm_data stApiAudioUnitShmData;

/* kernel debug proc */
/* Define a Proc structure for debug */
struct ApiAudioUnit_kernel_debug_proc_st
{
    struct proc_dir_entry *proc_entry;
    struct file_operations proc_fops;
    char *proc_name;
    umode_t proc_mode;
};
static struct proc_dir_entry *g_proc_root = NULL;

#else //User Space
//file for nDBG log
FILE *pApiAudioUnit_DBG_Log_To_File      = NULL;
FILE *pApiAudioUnit_DBG_MenuLog_To_File  = NULL;

//debug menu operate by non_scanf
unsigned int  ApiAudioUnit_non_scanf_params[API_AUDIO_UNIT_NON_SCANF_DEBUG_CMD_PARAMS_MAX];     // store the decimal command number
unsigned int  ApiAudioUnit_non_scanf_paramsHex[API_AUDIO_UNIT_NON_SCANF_DEBUG_CMD_PARAMS_MAX];  // store the heximal command number
char          ApiAudioUnit_non_scanf_paramsStr[API_AUDIO_UNIT_DEBUG_INPUT_STR_LENGTH+1];        // store the string
unsigned char ApiAudioUnit_non_scanf_paramIdx = 0;
unsigned char ApiAudioUnit_non_scanf_paramsCount = 0;

//AU debug get string input
char ApiAudioUnit_debug_input_string[API_AUDIO_UNIT_DEBUG_INPUT_STR_LENGTH+1] = {'\0'};
FILE *pApiAudioUnit_debug_input_file  = NULL;
FILE *pApiAudioUnit_debug_input_file2 = NULL;
FILE *pApiAudioUnit_debug_input_file3 = NULL;

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Pre-Declaration Area] pre-declared [function] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
/* Initialize, STR */

/* Connect & Disconnect */

/* Start & Stop */

/* SPDIF */

/* HDMI */

/* ATV */

/* Decoder */

/* Common Decoder */

/* Common */

/* Customized patch */

/* Customized Internal patch */

/* Clip Play for ES */

/* Clip Play for PCM */

/* Gain, Mute & Delay */

/* AENC */

/* PCM Capture */

/* PCM IO Control */

/* MM New Mode */

/* Mstar Sound Effect */

/* Advanced Sound Effect */

/* internal used functions */
void    ApiAudioUnit_SHOW_ALL_VERSION(void);
void    ApiAudioUnit_Delay1MS(int delay1MS);
MS_U8*  ApiAudioUnit_MemoryAlloc(MS_U32 allocSize);
void    ApiAudioUnit_MemoryFree(void *pMemoryBuffer);
MS_BOOL ApiAudioUnit_DebugMenu(void);

#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel space
struct file * ApiAudioUnit_FileOpen(const char * filename, const char *mode);
void ApiAudioUnit_FileClose(struct file *fp);
void ApiAudioUnit_FileWrite(const void *p, size_t size, size_t nmemb, struct file * fp);

/* kernel debug proc */
/* proc child 1 */
static ssize_t ApiAudioUnit_kernel_debug_proc_read_nDBG(struct file *file, char __user *buffer, size_t count, loff_t *pos);
static ssize_t ApiAudioUnit_kernel_debug_proc_write_nDBG(struct file *file, const char __user *buffer, size_t count, loff_t *pos);

/* proc child 2 */
static ssize_t ApiAudioUnit_kernel_debug_proc_read_ShowVersion(struct file *file, char __user *buffer, size_t count, loff_t *pos);

#else //User space
FILE * ApiAudioUnit_FileOpen(const char * filename, const char *mode);
int  ApiAudioUnit_FileClose(FILE * fp);
void ApiAudioUnit_FileWrite(const void *p, size_t size, size_t nmemb, FILE *fp);

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [function] declare / implement in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
void ApiAudioUnit_ShmInit_All_variables(void)
{
    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Initialize, STR */
    //-------------------------------------------------------------------------------------------------------------------------------------
    //Audio init flag
    pstAudioUnitShmData->g_audio_unit_Init_Done         = FALSE;
    pstAudioUnitShmData->g_audio_unit_STR_bSuspendStart = FALSE;
    pstAudioUnitShmData->g_audio_unit_STR_bResumeFinish = TRUE;
    pstAudioUnitShmData->g_audio_unit_scanf_bSupport    = TRUE;

    //Dynamic Enable all apiAUDIO_v2_customer debug msg
    pstAudioUnitShmData->g_audio_unit_bEnableNonThreadPrintMsg          = FALSE;
    pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg             = FALSE;
    pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg_Interval    = 500;
    pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg                   = FALSE;
    pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Interval          = 1000;
    pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Line              = 0;

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Connect & Disconnect */
    //-------------------------------------------------------------------------------------------------------------------------------------
    //Parser connect
    pstAudioUnitShmData->g_audio_unit_PARSER_Mode      = AUDIO_UNIT_PARSER_MODE_INVALID;
    pstAudioUnitShmData->g_audio_unit_PARSER_A_Connect = AUDIO_UNIT_PARSER_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_PARSER_B_Connect = AUDIO_UNIT_PARSER_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_PARSER_C_Connect = AUDIO_UNIT_PARSER_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_PARSER_D_Connect = AUDIO_UNIT_PARSER_INPUT_INVALID;

    //ADEC connect
    pstAudioUnitShmData->g_audio_unit_ADEC0_Connect     = AUDIO_UNIT_ADEC_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_ADEC1_Connect     = AUDIO_UNIT_ADEC_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_ADEC_ATV_Connect  = AUDIO_UNIT_ADEC_INPUT_INVALID;

    //ADC connect
    pstAudioUnitShmData->g_audio_unit_ADC0_Connect      = AUDIO_UNIT_ADC_IN_PORT_INVALID;
    pstAudioUnitShmData->g_audio_unit_ADC1_Connect      = AUDIO_UNIT_ADC_IN_PORT_INVALID;

    //PCM Mixer connect / ID
    #if(AUDIO_UNIT_SET_BY_MEMCPY)
    memset(&pstAudioUnitShmData->g_audio_unit_PCM_Mixer_Connect, FALSE,                              sizeof(pstAudioUnitShmData->g_audio_unit_PCM_Mixer_Connect));
    memset(&pstAudioUnitShmData->g_audio_unit_PCM_Mixer_ID,      AUDIO_UNIT_PCM_MIXER_INPUT_INVALID, sizeof(pstAudioUnitShmData->g_audio_unit_PCM_Mixer_ID));
    #endif //#if(AUDIO_UNIT_SET_BY_MEMCPY)

    //Channel Sound Connect
    pstAudioUnitShmData->g_audio_unit_CH5_Sound_Connect = AUDIO_UNIT_CH_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_CH6_Sound_Connect = AUDIO_UNIT_CH_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_CH7_Sound_Connect = AUDIO_UNIT_CH_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_CH8_Sound_Connect = AUDIO_UNIT_CH_INPUT_INVALID;

    //FW Mixer Connect
    #if(AUDIO_UNIT_SET_BY_MEMCPY)
    memset(&pstAudioUnitShmData->g_audio_unit_FW_Mixer0_Connect,        FALSE,  sizeof(pstAudioUnitShmData->g_audio_unit_FW_Mixer0_Connect));
    memset(&pstAudioUnitShmData->g_audio_unit_FW_Mixer1_Connect,        FALSE,  sizeof(pstAudioUnitShmData->g_audio_unit_FW_Mixer1_Connect));
    memset(&pstAudioUnitShmData->g_audio_unit_FW_Mixer2_Connect,        FALSE,  sizeof(pstAudioUnitShmData->g_audio_unit_FW_Mixer2_Connect));
    #endif //#if(AUDIO_UNIT_SET_BY_MEMCPY)

    //SE(PreR2 / SeDSP / PostR2) connect
    pstAudioUnitShmData->g_audio_unit_SE_PreR2_Connect  = AUDIO_UNIT_SE_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_SE_DspSE_Connect  = AUDIO_UNIT_SE_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_SE_PostR2_Connect = AUDIO_UNIT_SE_INPUT_INVALID;

    //SOUND OUT connect
    pstAudioUnitShmData->g_audio_unit_Sound_Out_I2S_Connect       = AUDIO_UNIT_SOUNDOUT_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_Sound_Out_Line0_Connect     = AUDIO_UNIT_SOUNDOUT_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_Sound_Out_Line1_Connect     = AUDIO_UNIT_SOUNDOUT_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_Sound_Out_Line2_Connect     = AUDIO_UNIT_SOUNDOUT_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_Sound_Out_Line3_Connect     = AUDIO_UNIT_SOUNDOUT_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_Sound_Out_SPDIF_Connect     = AUDIO_UNIT_SOUNDOUT_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_Sound_Out_ARC_Connect       = AUDIO_UNIT_SOUNDOUT_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_Sound_Out_HDMI_Connect      = AUDIO_UNIT_SOUNDOUT_INPUT_INVALID;

    //PCM Capture connect
    pstAudioUnitShmData->g_audio_unit_PCM_CAPTURE0_Connect  = AUDIO_UNIT_PCM_CAPTURE_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_PCM_CAPTURE1_Connect  = AUDIO_UNIT_PCM_CAPTURE_INPUT_INVALID;
    pstAudioUnitShmData->g_audio_unit_PCM_CAPTURE2_Connect  = AUDIO_UNIT_PCM_CAPTURE_INPUT_INVALID;

    //MP3 ENC connect
    pstAudioUnitShmData->g_audio_unit_MP3_ENC_Connect = AUDIO_UNIT_MP3_ENC_INPUT_INVALID;

    //AAC ENC connect
    pstAudioUnitShmData->g_audio_unit_AAC_ENC_Connect = AUDIO_UNIT_MP3_ENC_INPUT_INVALID;

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Start & Stop */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* SPDIF */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* HDMI */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* ATV */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Decoder */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Common */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Customized patch */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Customized Internal patch */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Clip Play for ES */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Clip Play for PCM */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Gain, Mute & Delay */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* AENC */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* PCM Capture */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* PCM IO Control */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* MM New Mode */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Mstar Sound Effect */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Advanced Sound Effect */
    //-------------------------------------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Thread monitor Enable */
    //-------------------------------------------------------------------------------------------------------------------------------------
    pstAudioUnitShmData->g_audio_unit_ThreadPrintMsg_Monitor_bEnable = TRUE;

    //-------------------------------------------------------------------------------------------------------------------------------------
    /* Thread monitor interval */
    //-------------------------------------------------------------------------------------------------------------------------------------

}

static void ApiAudioUnit_ThreadPrintMsg_Monitor(void)
{
    while(pstAudioUnitShmData->g_audio_unit_ThreadPrintMsg_Monitor_bEnable)
    {
        if(pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg)
        {
            pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg = TRUE;

            ApiAudioUnit_Delay1MS(pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Interval);

            pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg = FALSE;
            pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Line = 0;

            ApiAudioUnit_Delay1MS(pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Interval);
        }

        ApiAudioUnit_Delay1MS(pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg_Interval);
    }
}

void ApiAudioUnit_Dump_Shm(void)
{
    #if(AUDIO_SHARED_VARS2_eAudioSource)
        ApiAudioUnitPRINT("g_AudioVars2->eAudioSource      = 0x%x \n", (unsigned int)g_AudioVars2->eAudioSource);
    #endif

    #if(AUDIO_SHARED_VARS2_eMainAudioSource)
        ApiAudioUnitPRINT("g_AudioVars2->eMainAudioSource  = 0x%x \n", (unsigned int)g_AudioVars2->eMainAudioSource);
    #endif

    #if(AUDIO_SHARED_VARS2_eSubAudioSource)
        ApiAudioUnitPRINT("g_AudioVars2->eSubAudioSource   = 0x%x \n", (unsigned int)g_AudioVars2->eSubAudioSource);
    #endif

    #if(AUDIO_SHARED_VARS2_eScartAudioSource)
        ApiAudioUnitPRINT("g_AudioVars2->eScartAudioSource = 0x%x \n", (unsigned int)g_AudioVars2->eScartAudioSource);
    #endif
}

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [function] declare / implement in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
//0x9993
void ApiAudioUnit_Where_Am_I(void)
{
    #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
    {
        ApiAudioUnitPRINT("[AUDIO][%s] [%d] [Hello Kernel Space] \n", __FUNCTION__, __LINE__);
    }
    #else //User Space
    {
        ApiAudioUnitPRINT("[AUDIO][%s] [%d] [Hello User Space] \n", __FUNCTION__, __LINE__);
    }
    #endif

    #ifdef AUDIO_UTOPIA_20 //Utopia 2.0
    {
        ApiAudioUnitPRINT("[AUDIO][%s] [%d] [Hello Utopia 2.0] \n", __FUNCTION__, __LINE__);
    }
    #else //Utopia 1.0
    {
        ApiAudioUnitPRINT("[AUDIO][%s] [%d] [Hello Utopia 1.0] \n", __FUNCTION__, __LINE__);
    }
    #endif

    #ifdef CONFIG_MBOOT //mboot
    {
        ApiAudioUnitPRINT("[AUDIO][%s] [%d] [Hello mboot] \n", __FUNCTION__, __LINE__);
    }
    #else //application
    {
        ApiAudioUnitPRINT("[AUDIO][%s] [%d] [Hello Application] \n", __FUNCTION__, __LINE__);
    }
    #endif
}

int ApiAudioUnit_strcmp(const char * str1, const char * str2, size_t n)
{
    #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
    {
        #if(LINUX_KERNEL_VERSION_4_4_3)
        {
            return strncasecmp(str1, str2, n);
        }
        #else
        {
            return strnicmp(str1, str2, n);
        }
        #endif
    }
    #else //User Space
    {
        return strcmp(str1, str2);
    }
    #endif
}

void ApiAudioUnit_Delay1MS(int delay1MS)
{
    #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
    {
        msleep(delay1MS);
    }
    #else //User Space
    {
        MsOS_DelayTask(delay1MS);
    }
    #endif
}

static MS_BOOL ApiAudioUnit_ShmInit(void)
{
    #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
    {
        if(pstAudioUnitShmData) return TRUE;

        if(pstAudioUnitShmData == NULL)
        {
            pstAudioUnitShmData = &stApiAudioUnitShmData;
            ApiAudioUnit_ShmInit_All_variables();
        }
        return TRUE;
    }
    #else //User Space
    {
        MS_U32 u32ShmId;
        MS_U32 u32Addr;
        MS_U32 u32BufSize;
        char SHM_ID[] = "Audio_Unit_Ctrl Driver";
        MS_BOOL bFirstRun = FALSE;

        if(pstAudioUnitShmData) return TRUE;

        MsOS_SHM_Init();

        if(FALSE == MsOS_SHM_GetId((MS_U8 *)SHM_ID, sizeof(St_Audio_Unit_Shm_data), &u32ShmId, &u32Addr, &u32BufSize, SYS_SHM_QUERY))
        {
            if(FALSE == MsOS_SHM_GetId((MS_U8 *)SHM_ID, sizeof(St_Audio_Unit_Shm_data), &u32ShmId, &u32Addr, &u32BufSize, SYS_SHM_CREATE))
            {
                return FALSE;
            }
            memset((MS_U8 *)u32Addr, 0, sizeof(St_Audio_Unit_Shm_data));
            bFirstRun = TRUE;
        }

        pstAudioUnitShmData = (St_Audio_Unit_Shm_data *)u32Addr;

        if(pstAudioUnitShmData == NULL)
        {
            return FALSE;
        }

        // first time to access, so init the data
        if(bFirstRun == TRUE)
        {
            ApiAudioUnit_ShmInit_All_variables();
        }
        return TRUE;
    }
    #endif
}

static void ApiAudioUnit_CreateThread(void)
{
    #ifdef CONFIG_MBOOT //mboot
    {
    }
    #else //application
    {
        //================================================================================================
        //                   ApiAudioUnit_ThreadPrintMsg_Monitor
        //================================================================================================
        #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
        {
            struct task_struct *pApiAudioUnit_ThreadPrintMsg_Thread = NULL;

            pApiAudioUnit_ThreadPrintMsg_Thread = kthread_create((void*)ApiAudioUnit_ThreadPrintMsg_Monitor, NULL, "MStar ApiAudioUnit_ThreadPrintMsg_Monitor");
            if (pApiAudioUnit_ThreadPrintMsg_Thread == NULL)
            {
                ApiAudioUnitPRINT("[Error] [%s] [Can't create ApiAudioUnit_ThreadPrintMsg_Monitor thread] \n", __FUNCTION__);
            }
            else
            {
                ApiAudioUnitDBG("ApiAudioUnit_ThreadPrintMsg_Monitor created successfully \n");
                wake_up_process(pApiAudioUnit_ThreadPrintMsg_Thread);
            }
        }
        #else //User Space
        {
            pthread_t           thread_info17;
            pthread_attr_t      attr17;
            struct sched_param  sched17;
            int                 s32Result17;

            pthread_attr_init(&attr17);
            pthread_attr_setdetachstate(&attr17, PTHREAD_CREATE_JOINABLE);
            pthread_attr_setinheritsched(&attr17, PTHREAD_EXPLICIT_SCHED);
            pthread_attr_setschedpolicy(&attr17, SCHED_RR);
            pthread_attr_getschedparam(&attr17, &sched17);
            sched17.sched_priority = 85;
            pthread_attr_setschedparam(&attr17, &sched17);

            s32Result17 = pthread_create(&thread_info17, &attr17, (void*)ApiAudioUnit_ThreadPrintMsg_Monitor, (void *) NULL);
            if (s32Result17 != 0)
            {
                ApiAudioUnitPRINT("[Error] [%s] [Can't ApiAudioUnit_ThreadPrintMsg_Monitor thread] \n", __FUNCTION__);
            }
            else
            {
                ApiAudioUnitDBG("ApiAudioUnit_ThreadPrintMsg_Monitor thread created successfully \n");
            }
        }
        #endif
    }
    #endif

}

#if(UPPER_SUPPORT_THREAD_DEBUG_MENU)
static void ApiAudioUnit_CreateDebugMenuThread(void)
{
    #ifdef CONFIG_MBOOT //mboot
    {
    }
    #else //application
    {
        #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
        {
            struct task_struct *pAU_DebugMenuThread = NULL;

            pAU_DebugMenuThread = kthread_create((void*)ApiAudioUnit_DebugMenu, NULL, "MStar ApiAudioUnit_DebugMenu");
            if (pAU_DebugMenuThread == NULL)
            {
                ApiAudioUnitPRINT("[Error] [%s] [Can't create ApiAudioUnit_DebugMenu thread] \n", __FUNCTION__);
            }
            else
            {
                ApiAudioUnitDBG("ApiAudioUnit_DebugMenu thread created successfully \n");
                wake_up_process(pAU_DebugMenuThread);
            }
        }
        #else //User Space
        {
            pthread_t           thread_info99;
            pthread_attr_t      attr99;
            struct sched_param  sched99;
            int                 s32Result99;

            pthread_attr_init(&attr99);
            pthread_attr_setdetachstate(&attr99, PTHREAD_CREATE_JOINABLE);
            pthread_attr_setinheritsched(&attr99, PTHREAD_EXPLICIT_SCHED);
            pthread_attr_setschedpolicy(&attr99, SCHED_RR);
            pthread_attr_getschedparam(&attr99, &sched99);
            sched99.sched_priority = 85;
            pthread_attr_setschedparam(&attr99, &sched99);

            s32Result99 = pthread_create(&thread_info99, &attr99, (void*)ApiAudioUnit_DebugMenu, (void *) NULL);
            if (s32Result99 != 0)
            {
                ApiAudioUnitPRINT("[Error] [%s] [Can't create ApiAudioUnit_DebugMenu thread] \n", __FUNCTION__);
            }
            else
            {
                ApiAudioUnitDBG("ApiAudioUnit_DebugMenu thread created successfully \n");
            }
        }
        #endif
    }
    #endif
}
#endif

MS_U8* ApiAudioUnit_MemoryAlloc(MS_U32 allocSize)
{
    MS_U8 *allocBuffer = NULL;

    #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
        allocBuffer = kzalloc(sizeof(MS_U8) * allocSize, GFP_KERNEL);
    #else //User Space
        allocBuffer = malloc(sizeof(MS_U8) * allocSize);
    #endif

    if (allocBuffer == NULL)
    {
        ApiAudioUnitPRINT("%s: Error! fail to allocate 0x%x bytes memory !\n", __FUNCTION__, (unsigned int)(sizeof(MS_U8) * allocSize));
        return allocBuffer;
    }

    memset(allocBuffer, 0x00, sizeof(MS_U8) * allocSize);

    return allocBuffer;
}

void ApiAudioUnit_MemoryFree(void *pMemoryBuffer)
{
    #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel space
        kfree(pMemoryBuffer);
    #else //user space
        free(pMemoryBuffer);
    #endif
}

#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel space
struct file * ApiAudioUnit_FileOpen(const char * filename, const char *mode)
{
    return filp_open(filename, O_RDWR|O_CREAT|O_APPEND, S_IRWXU | S_IRWXG | S_IRWXO);
}

void ApiAudioUnit_FileClose(struct file *fp)
{
    filp_close(fp, NULL);
}

void ApiAudioUnit_FileWrite(const void *p, size_t size, size_t nmemb, struct file * fp)
{
    mm_segment_t fs;
    /* get current->addr_limit. It should be 0-3G */
    fs = get_fs();
    /* set current->addr_limit to 4G */
    set_fs(get_ds());

    if (fp->f_op && fp->f_op->write)
        fp->f_op->write(fp, p, nmemb, &fp->f_pos);

    if (fp->f_op && fp->f_op->flush)
        fp->f_op->flush(fp, NULL);

    /* restore the addr_limit */
    set_fs(fs);
}

#else //User space
FILE * ApiAudioUnit_FileOpen(const char * filename, const char *mode)
{
#ifdef CONFIG_MBOOT
    return NULL;
#else
    return fopen(filename, mode);
#endif
}

int ApiAudioUnit_FileClose(FILE * fp)
{
#ifdef CONFIG_MBOOT
    return NULL;
#else
    return fclose(fp);
#endif
}

void ApiAudioUnit_FileWrite(const void *p, size_t size, size_t nmemb, FILE *fp)
{
#ifdef CONFIG_MBOOT
    return;
#else
    fwrite(p, size, nmemb, fp);
    fflush(fp);
#endif
}
#endif

/* Debug Related functions */
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
MS_BOOL ApiAudioUnit_DebugMenu(void)
{
    //Kernel Space dont support IO. need to use proc
    return TRUE;
}

/* kernel debug proc */
static struct ApiAudioUnit_kernel_debug_proc_st ApiAudioUnit_kernel_debug_proc_child[API_AUDIO_UNIT_KERNEL_DEBUG_PROC_MAX_CHILD] =
{
    {
        /* proc child 1 */
        .proc_entry = NULL,
        .proc_fops  =
        {
            .owner  = THIS_MODULE,
            .read   = ApiAudioUnit_kernel_debug_proc_read_nDBG,
            .write  = ApiAudioUnit_kernel_debug_proc_write_nDBG,
        },
        .proc_name  = API_AUDIO_UNIT_KERNEL_DEBUG_PROC_CHILD_0001_NDBG,
        .proc_mode  = 0x0666,
    },
    {
        /* proc child 2 */
        .proc_entry = NULL,
        .proc_fops  =
        {
            .owner  = THIS_MODULE,
            .read   = ApiAudioUnit_kernel_debug_proc_read_ShowVersion,
        },
        .proc_name  = API_AUDIO_UNIT_KERNEL_DEBUG_PROC_CHILD_0002_SHOW_VERSION,
        .proc_mode  = 0x0666,
    },
};

//-----------------------------------------------------------------------------
// Proc Operations (Init / Exit)
//-----------------------------------------------------------------------------
static int ApiAudioUnit_KERNEL_DEBUG_PROC_init(void)
{
    struct ApiAudioUnit_kernel_debug_proc_st *proc_child = NULL;
    int loop = 0;
    int ret = 0;

    g_proc_root = proc_mkdir(API_AUDIO_UNIT_KERNEL_DEBUG_PROC_ROOT_NAME, NULL);
    if (g_proc_root == NULL)
    {
        ApiAudioUnitDBG(KERN_ERR "Error! fail to create root directory of proc filesystem (%s)!\n", API_AUDIO_UNIT_KERNEL_DEBUG_PROC_ROOT_NAME);
        ret = -EACCES;
    }
    else
    {
        for (loop = 0; loop < API_AUDIO_UNIT_KERNEL_DEBUG_PROC_MAX_CHILD; loop++)
        {
            proc_child = &ApiAudioUnit_kernel_debug_proc_child[loop];

            proc_child->proc_entry = proc_create_data(proc_child->proc_name, proc_child->proc_mode, g_proc_root, &proc_child->proc_fops, NULL);
            if (proc_child->proc_entry == NULL)
            {
                ApiAudioUnitDBG(KERN_ERR "Error! fail to create child directory of proc filesystem (%s)!\n", proc_child->proc_name);
                ret = -EACCES;
                break;
            }
        }
    }
    ApiAudioUnitDBG("\033[1;35m *****MstarAudioCustomerDebug proc insert SUCESS!! \033[0m \n");

    return ret;
}

static inline void ApiAudioUnit_KERNEL_DEBUG_PROC_exit(void)
{
    struct ApiAudioUnit_kernel_debug_proc_st *proc_child = NULL;
    int loop = 0;

    for (loop = 0; loop < API_AUDIO_UNIT_KERNEL_DEBUG_PROC_MAX_CHILD; loop++)
    {
        proc_child = &ApiAudioUnit_kernel_debug_proc_child[loop];

        if (proc_child->proc_entry != NULL)
        {
            remove_proc_entry(proc_child->proc_name, g_proc_root);
        }
    }

    remove_proc_entry(API_AUDIO_UNIT_KERNEL_DEBUG_PROC_ROOT_NAME, NULL);

    return;
}

/*

note:
cat  xxx            // proc read
echo ooo on  > xxx  // proc write
echo ooo off > xxx  // proc write

===============================================================================
 * * * How to use kernel debug proc * * *
===============================================================================
#------------------------------------------
#Child 1: "0001_nDBG"
#------------------------------------------
cd /proc/MstarAudioUnitCtrlDebug/
cat  0001_nDBG
echo EnableNonThreadPrint       > 0001_nDBG
echo EnableThreadPrint          > 0001_nDBG
echo DisableNonThreadPrint      > 0001_nDBG
echo DisableThreadPrint         > 0001_nDBG
echo DisableAllThreadMonitor    > 0001_nDBG

#------------------------------------------
#Child 2: "0002_Version"
#------------------------------------------
cd /proc/MstarAudioUnitCtrlDebug/
cat  0002_ShowVersion

*/

//-----------------------------------------------------------------------------
// Proc Operations (Child 1, "001_AudioLog")
//-----------------------------------------------------------------------------
static ssize_t ApiAudioUnit_kernel_debug_proc_read_nDBG(struct file *file, char __user *buffer, size_t count, loff_t *pos)
{
    ApiAudioUnitPRINT("Proc_Read_Test\n");

    return 0;
}

static ssize_t ApiAudioUnit_kernel_debug_proc_write_nDBG(struct file *file, const char __user *buffer, size_t count, loff_t *pos)
{
    char value_str[API_AUDIO_UNIT_KERNEL_DEBUG_PROC_STRING_BUFFER_LENGTH] = {'\0'};
    char action[API_AUDIO_UNIT_KERNEL_DEBUG_PROC_STRING_BUFFER_LENGTH];

    if (copy_from_user(value_str, buffer, count))
    {
        return -EFAULT;
    }

    value_str[count] = '\0';
    sscanf(value_str, "%s\n", action);

    ApiAudioUnitPRINT("*********%s\n", action);

    if (!strcmp("EnableNonThreadPrint",action))
    {
        ApiAudioUnitPRINT("EnableNonThreadPrint \n");
        pstAudioUnitShmData->g_audio_unit_bEnableNonThreadPrintMsg = TRUE;
    }

    if (!strcmp("EnableThreadPrint",action))
    {
        ApiAudioUnitPRINT("EnableThreadPrint \n");
        pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg = TRUE;
    }

    if (!strcmp("DisableNonThreadPrint",action))
    {
        ApiAudioUnitPRINT("DisableNonThreadPrint \n");
        pstAudioUnitShmData->g_audio_unit_bEnableNonThreadPrintMsg = FALSE;
    }

    if (!strcmp("DisableThreadPrint",action))
    {
        ApiAudioUnitPRINT("DisableThreadPrint \n");
        pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg = FALSE;
    }

    if (!strcmp("DisableAllThreadMonitor",action))
    {
        ApiAudioUnitPRINT("DisableAllThreadMonitor \n");
        pstAudioUnitShmData->g_audio_unit_ThreadPrintMsg_Monitor_bEnable = FALSE;
    }

    return count;
}

//-----------------------------------------------------------------------------
// Proc Operations (Child 2, "0002_ShowVersion")
//-----------------------------------------------------------------------------
static ssize_t ApiAudioUnit_kernel_debug_proc_read_ShowVersion(struct file *file, char __user *buffer, size_t count, loff_t *pos)
{
    ApiAudioUnit_SHOW_ALL_VERSION();

    return 0;
}

#else //User Space
char* ApiAudioUnit_fgets(char* buf, int num, FILE* fp)
{
    char* c_eof = 0;

    if(!fgets(buf, num, fp))
    {
        return NULL;
    }
    while(buf[0]== '\n')
    {
        if(!fgets(buf, num, fp))
        {
            return NULL;
        }
        if((c_eof = strrchr(buf, '\n')))
        {
            *c_eof = '\0';
        }
        else
        {
            int ch;
            while(((ch = fgetc(fp)) != EOF) && (ch != '\n'));
        }

    }
    return buf;
}

unsigned int ApiAudioUnit_Debug_GetHex(const char *szComment)
{
    if(pstAudioUnitShmData->g_audio_unit_scanf_bSupport == TRUE)
    {
        char cmd[256];
        memset(cmd, 0, 256);

        ApiAudioUnitPRINT("%s", szComment);
        fflush(stdout);

        do
        {
            ApiAudioUnit_fgets(cmd, sizeof(cmd), stdin);
        }while( !isxdigit((int)(*cmd)) && (fprintf(stderr, "please enter a hex integer\n") || 1 ));

        cmd[255] = 0x0;

        return (strtoul( cmd, (char**) NULL, 16 ));
    }
    else if(pstAudioUnitShmData->g_audio_unit_scanf_bSupport == FALSE) //NON_SCANF version: Allow debug menu to work without scanf
    {
        unsigned int ret = 0xFFFF;

        if(szComment)
        {
            ApiAudioUnitPRINT("%s", szComment);
            fflush(stdout);
        }

        while(ApiAudioUnit_non_scanf_paramsCount == 0)
        {
            // No more ApiAudioUnit_non_scanf_params, block until more ApiAudioUnit_non_scanf_params become avail.
            sleep(2);
        }
        if((ApiAudioUnit_non_scanf_paramsCount>0) && (ApiAudioUnit_non_scanf_paramIdx < API_AUDIO_UNIT_NON_SCANF_DEBUG_CMD_PARAMS_MAX))
        {
            ApiAudioUnit_non_scanf_paramsCount--;
            ret= ApiAudioUnit_non_scanf_paramsHex[ApiAudioUnit_non_scanf_paramIdx++];
        }

        return ret;
    }

    return TRUE;
}

int ApiAudioUnit_Debug_GetDec(void)
{
    if(pstAudioUnitShmData->g_audio_unit_scanf_bSupport == TRUE)
    {
        int dValue;

        if(scanf("%d", &dValue)==FALSE)
        {
            dValue=9284;
        }

        return dValue;
    }
    else if(pstAudioUnitShmData->g_audio_unit_scanf_bSupport == FALSE) //NON_SCANF version: Allow debug menu to work without scanf
    {
        unsigned int ret = 0xFFFF;
        while(ApiAudioUnit_non_scanf_paramsCount == 0)
        {
            // No more ApiAudioUnit_non_scanf_params, block until more ApiAudioUnit_non_scanf_params become avail.
            sleep(2);
        }
        if((ApiAudioUnit_non_scanf_paramsCount>0) && (ApiAudioUnit_non_scanf_paramIdx < API_AUDIO_UNIT_NON_SCANF_DEBUG_CMD_PARAMS_MAX))
        {
            ApiAudioUnit_non_scanf_paramsCount--;
            ret = ApiAudioUnit_non_scanf_params[ApiAudioUnit_non_scanf_paramIdx++];
        }
        return ret;
    }

    return TRUE;
}

char* ApiAudioUnit_Debug_GetString(void)
{
    if(pstAudioUnitShmData->g_audio_unit_scanf_bSupport == TRUE)
    {
        if(scanf("%s", ApiAudioUnit_debug_input_string)==FALSE)
        {
            memset(ApiAudioUnit_debug_input_string, 'F', API_AUDIO_UNIT_DEBUG_INPUT_STR_LENGTH+1);
        }
    }
    else if(pstAudioUnitShmData->g_audio_unit_scanf_bSupport == FALSE) //NON_SCANF version: Allow debug menu to work without scanf
    {
        while(ApiAudioUnit_non_scanf_paramsCount == 0)
        {
            // No more ApiAudioUnit_non_scanf_params, block until more ApiAudioUnit_non_scanf_params become avail.
            sleep(2);
        }
        ApiAudioUnit_non_scanf_paramsCount--;
        strcpy(ApiAudioUnit_debug_input_string, ApiAudioUnit_non_scanf_paramsStr);
    }

    return ApiAudioUnit_debug_input_string;
}

MS_BOOL ApiAudioUnit_DebugMenu(void)
{
    int arg1=0;
    unsigned int arg2=0, arg3=0, arg4=0, arg5=0, arg6=0, arg7=0, arg8=0, arg9=0, arg10=0;

    while(1)
    {
        ApiAudioUnitPRINT("==========================================================================================   \n");
        ApiAudioUnitPRINT("(0xSHOW) pstAudioUnitShmData->g_audio_unit_scanf_bSupport                (%d)                \n", (unsigned int)pstAudioUnitShmData->g_audio_unit_scanf_bSupport);
        ApiAudioUnitPRINT("==========================================================================================   \n");
        ApiAudioUnitPRINT("(0x0000) pstAudioUnitShmData->g_audio_unit_bEnableNonThreadPrintMsg      (%d)                \n", (unsigned int)pstAudioUnitShmData->g_audio_unit_bEnableNonThreadPrintMsg);
        ApiAudioUnitPRINT("(0x0001) pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg         (%d)                \n", (unsigned int)pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg);
        ApiAudioUnitPRINT("(0x0002) pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg_Interval(%d)                \n", (unsigned int)pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg_Interval);
        ApiAudioUnitPRINT("(0x0003) pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Interval      (%d)                \n", (unsigned int)pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Interval);
        ApiAudioUnitPRINT("(0x0007) Disable All Thread Monitor                                                          \n");
        ApiAudioUnitPRINT("(0x0008) ApiAudioUnit_Dump_Shm                                                               \n");
        ApiAudioUnitPRINT("==========================================================================================   \n");
        ApiAudioUnitPRINT("(0x7202) log Audio Path                                                                      \n");
        ApiAudioUnitPRINT("(0x8888) Enable nDBG log in file                                                             \n");
        ApiAudioUnitPRINT("(0x4444) Close  nDBG log in file                                                             \n");
        ApiAudioUnitPRINT("(0x1111) Refresh menu                                                                        \n");
        ApiAudioUnitPRINT("==========================================================================================   \n");
        ApiAudioUnitPRINT("(0x9993) ApiAudioUnit_Where_Am_I()                                                           \n");
        ApiAudioUnitPRINT("(0x9996) ApiAudioUnit_SHOW_ALL_VERSION()                                                     \n");
        ApiAudioUnitPRINT("(0x9999) EXIT                                                                                \n");
        ApiAudioUnitPRINT("==========================================================================================   \n");
        ApiAudioUnitPRINT("Please enter debug[Function arg2 arg3 arg4 arg5] => 0x1 0x1...                               \n");
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [------AUDIO_DEBUG------Begin] \n\n\n", __FILE__, __FUNCTION__, __LINE__);

        arg1 = ApiAudioUnit_Debug_GetHex("");
        int nUnused = 0;
        nUnused = nUnused;

        switch(arg1)
        {
            case 0x0000:
            {
                ApiAudioUnitPRINT("pstAudioUnitShmData->g_audio_unit_bEnableNonThreadPrintMsg: \n");
                arg2 = ApiAudioUnit_Debug_GetDec();

                pstAudioUnitShmData->g_audio_unit_bEnableNonThreadPrintMsg = (MS_BOOL)arg2;
            }
            break;

            case 0x0001:
            {
                ApiAudioUnitPRINT("pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg: \n");
                arg2 = ApiAudioUnit_Debug_GetDec();

                pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg = (MS_BOOL)arg2;
            }
            break;

            case 0x0002:
            {
                ApiAudioUnitPRINT("pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg_Interval: \n");
                arg2 = ApiAudioUnit_Debug_GetDec();

                pstAudioUnitShmData->g_audio_unit_bEnableThreadPrintMsg_Interval = (MS_BOOL)arg2;
            }
            break;

            case 0x0003:
            {
                ApiAudioUnitPRINT("pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Interval: \n");
                arg2 = ApiAudioUnit_Debug_GetDec();

                pstAudioUnitShmData->g_audio_unit_bThreadPrintMsg_Interval = (MS_BOOL)arg2;
            }
            break;

            case 0x0007:
            {
                ApiAudioUnitPRINT("DisableAllThreadMonitor... \n");
                pstAudioUnitShmData->g_audio_unit_ThreadPrintMsg_Monitor_bEnable = FALSE;
            }
            break;

            case 0x0008:
            {
                ApiAudioUnitPRINT("ApiAudioUnit_Dump_Shm... \n");
                ApiAudioUnit_Dump_Shm();
            }
            break;

            case 0x7202:
            {
                pApiAudioUnit_DBG_MenuLog_To_File = ApiAudioUnit_FileOpen(API_AUDIO_UNIT_DBG_MENULOG_TO_FILE_PATH, "wb");
            }
            break;

            case 0x8888:
            {
                pApiAudioUnit_DBG_Log_To_File        = ApiAudioUnit_FileOpen(API_AUDIO_UNIT_DBG_LOG_TO_FILE_PATH, "wb");
                pApiAudioUnit_DBG_MenuLog_To_File    = ApiAudioUnit_FileOpen(API_AUDIO_UNIT_DBG_MENULOG_TO_FILE_PATH, "wb");
            }
            break;

            case 0x4444:
            {
                fflush(pApiAudioUnit_DBG_Log_To_File);
                if (pApiAudioUnit_DBG_Log_To_File != NULL)
                {
                    ApiAudioUnit_FileClose(pApiAudioUnit_DBG_Log_To_File);
                    pApiAudioUnit_DBG_Log_To_File = NULL;
                }
            }
            break;

            case 0x1111:
            {
                //refresh
            }
            break;

            case 0x9993:
            {
                ApiAudioUnit_Where_Am_I();
            }
            break;

            case 0x9996:
            {
                ApiAudioUnit_SHOW_ALL_VERSION();
            }
            break;

            default:
            case 0x9999:
            {
                ApiAudioUnitPRINT("\n\n\n\n\n\n\n\n");
                ApiAudioUnitPRINT("============================================================    \n");
                ApiAudioUnitPRINT(">>>> Exit AU_Debug menu, BYE BYE!!! Audio Bless You !!! <<<<    \n");
                ApiAudioUnitPRINT("============================================================    \n");
                ApiAudioUnitPRINT("\n\n\n\n\n\n\n\n");

                return TRUE;
            }
            break;
        }

        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg1  = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg1);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg2  = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg2);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg3  = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg3);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg4  = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg4);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg5  = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg5);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg6  = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg6);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg7  = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg7);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg8  = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg8);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg9  = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg9);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [arg10 = 0x%x]   \n", __FILE__, __FUNCTION__, __LINE__, (unsigned int)arg10);
        ApiAudioUnitPRINT("[AUDIO][%s] [%s] [%d] [------AUDIO_DEBUG------End] \n\n\n", __FILE__, __FUNCTION__, __LINE__);
    }
}

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// please understand, it's important.
//-------------------------------------------------------------------------------------------------------------------------------------
/* Initialize, STR */
#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_Initialize(void *pParam)
#else
MS_BOOL  API_AUDIO_UNIT_Initialize(void *pParam)
#endif
{
    ApiAudioUnit_ShmInit();

    if(pstAudioUnitShmData->g_audio_unit_Init_Done == TRUE)
    {
        ApiAudioUnitPRINT("[AUDIO][%s] [%d] [Return for Double Init!!!] \n", __FUNCTION__, __LINE__);
        return TRUE;
    }

    ApiAudioUnitDBG("\n");

    #if (AUDIO_UNIT_INIT_ALL_THREAD)
    ApiAudioUnit_CreateThread();
    #endif //#if (AUDIO_UNIT_INIT_ALL_THREAD)

    #ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
    {
        ApiAudioUnit_KERNEL_DEBUG_PROC_init();
    }
    #endif //#ifdef MSOS_TYPE_LINUX_KERNEL

    pstAudioUnitShmData->g_audio_unit_Init_Done = TRUE;

    return TRUE;
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_Suspend(void)
#else
MS_BOOL  API_AUDIO_UNIT_Suspend(void)
#endif
{
    ApiAudioUnitDBG("\n");

    return FALSE;
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_Resume(void)
#else
MS_BOOL  API_AUDIO_UNIT_Resume(void)
#endif
{
    ApiAudioUnitDBG("\n");

    return FALSE;
}

/* Connect & Disconnect */
#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_SetParserMode(AUDIO_UNIT_PARSER_MODE ParserMode)
#else
MS_BOOL  API_AUDIO_UNIT_SetParserMode(AUDIO_UNIT_PARSER_MODE ParserMode)
#endif
{
    ApiAudioUnitDBG("[ParserMode = 0x%x] \n", (unsigned int)ParserMode);

    return DRV_AUDIO_UNIT_SetParserMode(ParserMode);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_ADEC_Connect(AUDIO_UNIT_ADEC_INDEX currentConnect, AUDIO_UNIT_ADEC_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_ADEC_Connect(AUDIO_UNIT_ADEC_INDEX currentConnect, AUDIO_UNIT_ADEC_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    switch(currentConnect)
    {
        case AUDIO_UNIT_ADEC0:
        {
            pstAudioUnitShmData->g_audio_unit_ADEC0_Connect = inputConnect;
        }
        break;

        case AUDIO_UNIT_ADEC1:
        {
            pstAudioUnitShmData->g_audio_unit_ADEC1_Connect = inputConnect;
        }
        break;

        case AUDIO_UNIT_ADEC_ATV:
        {
            pstAudioUnitShmData->g_audio_unit_ADEC_ATV_Connect = inputConnect;
        }
        break;

        default:
            break;
    }

    return DRV_AUDIO_UNIT_ADEC_Connect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_ADEC_Disconnect(AUDIO_UNIT_ADEC_INDEX currentConnect)
#else
MS_BOOL  API_AUDIO_UNIT_ADEC_Disconnect(AUDIO_UNIT_ADEC_INDEX currentConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] \n", (unsigned int)currentConnect);

    switch(currentConnect)
    {
        case AUDIO_UNIT_ADEC0:
        {
            pstAudioUnitShmData->g_audio_unit_ADEC0_Connect = AUDIO_UNIT_ADEC_INPUT_INVALID;
        }
        break;

        case AUDIO_UNIT_ADEC1:
        {
            pstAudioUnitShmData->g_audio_unit_ADEC1_Connect = AUDIO_UNIT_ADEC_INPUT_INVALID;
        }
        break;

        case AUDIO_UNIT_ADEC_ATV:
        {
            pstAudioUnitShmData->g_audio_unit_ADEC_ATV_Connect = AUDIO_UNIT_ADEC_INPUT_INVALID;
        }
        break;

        default:
            break;
    }

    return DRV_AUDIO_UNIT_ADEC_Disconnect(currentConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_ADC_Connect(AUDIO_UNIT_ADC_INDEX currentConnect, AUDIO_UNIT_ADC_IN_PORT portNum)
#else
MS_BOOL  API_AUDIO_UNIT_ADC_Connect(AUDIO_UNIT_ADC_INDEX currentConnect, AUDIO_UNIT_ADC_IN_PORT portNum)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x][portNum = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)portNum);

    switch(currentConnect)
    {
        case AUDIO_UNIT_ADC0:
        {
            pstAudioUnitShmData->g_audio_unit_ADC0_Connect = portNum;
        }
        break;

        case AUDIO_UNIT_ADC1:
        {
            pstAudioUnitShmData->g_audio_unit_ADC1_Connect = portNum;
        }
        break;

        default:
            break;
    }

    return DRV_AUDIO_UNIT_ADC_Connect(currentConnect, portNum);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_ADC_Disconnect(AUDIO_UNIT_ADC_INDEX currentConnect, AUDIO_UNIT_ADC_IN_PORT portNum)
#else
MS_BOOL  API_AUDIO_UNIT_ADC_Disconnect(AUDIO_UNIT_ADC_INDEX currentConnect, AUDIO_UNIT_ADC_IN_PORT portNum)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x][portNum = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)portNum);

    switch(currentConnect)
    {
        case AUDIO_UNIT_ADC0:
        {
            pstAudioUnitShmData->g_audio_unit_ADC0_Connect = AUDIO_UNIT_ADC_IN_PORT_INVALID;
        }
        break;

        case AUDIO_UNIT_ADC1:
        {
            pstAudioUnitShmData->g_audio_unit_ADC1_Connect = AUDIO_UNIT_ADC_IN_PORT_INVALID;
        }
        break;

        default:
            break;
    }

    return DRV_AUDIO_UNIT_ADC_Disconnect(currentConnect, portNum);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_PCM_Mixer_Connect(AUDIO_UNIT_PCM_MIXER_INDEX currentConnect, AUDIO_UNIT_PCM_MIXER_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_PCM_Mixer_Connect(AUDIO_UNIT_PCM_MIXER_INDEX currentConnect, AUDIO_UNIT_PCM_MIXER_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_PCM_Mixer_Connect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_PCM_Mixer_Disconnect(AUDIO_UNIT_PCM_MIXER_INDEX currentConnect, AUDIO_UNIT_PCM_MIXER_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_PCM_Mixer_Disconnect(AUDIO_UNIT_PCM_MIXER_INDEX currentConnect, AUDIO_UNIT_PCM_MIXER_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_PCM_Mixer_Disconnect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_CH_Sound_Connect(AUDIO_UNIT_CH_SOUND currentConnect, AUDIO_UNIT_CH_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_CH_Sound_Connect(AUDIO_UNIT_CH_SOUND currentConnect, AUDIO_UNIT_CH_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currectChannel = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_CH_Sound_Connect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_CH_Sound_Disconnect(AUDIO_UNIT_CH_SOUND currentConnect, AUDIO_UNIT_CH_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_CH_Sound_Disconnect(AUDIO_UNIT_CH_SOUND currentConnect, AUDIO_UNIT_CH_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currectChannel = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_CH_Sound_Disconnect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_FW_MIXER_Connect(AUDIO_UNIT_FWM_INDEX currentConnect, AUDIO_UNIT_FWM_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_FW_MIXER_Connect(AUDIO_UNIT_FWM_INDEX currentConnect, AUDIO_UNIT_FWM_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_FW_MIXER_Connect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_FW_MIXER_Disconnect(AUDIO_UNIT_FWM_INDEX currentConnect, AUDIO_UNIT_FWM_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_FW_MIXER_Disconnect(AUDIO_UNIT_FWM_INDEX currentConnect, AUDIO_UNIT_FWM_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_FW_MIXER_Disconnect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_SE_Connect(AUDIO_UNIT_SE_INDEX currentConnect, AUDIO_UNIT_SE_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_SE_Connect(AUDIO_UNIT_SE_INDEX currentConnect, AUDIO_UNIT_SE_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_SE_Connect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_SE_Disconnect(AUDIO_UNIT_SE_INDEX currentConnect, AUDIO_UNIT_SE_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_SE_Disconnect(AUDIO_UNIT_SE_INDEX currentConnect, AUDIO_UNIT_SE_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_SE_Disconnect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_SNDOUT_Connect(AUDIO_UNIT_SOUNDOUT_INDEX currentConnect, AUDIO_UNIT_SOUNDOUT_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_SNDOUT_Connect(AUDIO_UNIT_SOUNDOUT_INDEX currentConnect, AUDIO_UNIT_SOUNDOUT_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_SNDOUT_Connect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_SNDOUT_Disconnect(AUDIO_UNIT_SOUNDOUT_INDEX currentConnect, AUDIO_UNIT_SOUNDOUT_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_SNDOUT_Disconnect(AUDIO_UNIT_SOUNDOUT_INDEX currentConnect, AUDIO_UNIT_SOUNDOUT_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_SNDOUT_Disconnect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_PCM_CAPTURE_Connect(AUDIO_UNIT_PCM_CAPTURE_INDEX currentConnect, AUDIO_UNIT_PCM_CAPTURE_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_PCM_CAPTURE_Connect(AUDIO_UNIT_PCM_CAPTURE_INDEX currentConnect, AUDIO_UNIT_PCM_CAPTURE_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_PCM_CAPTURE_Connect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_PCM_CAPTURE_Disconnect(AUDIO_UNIT_PCM_CAPTURE_INDEX currentConnect, AUDIO_UNIT_PCM_CAPTURE_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_PCM_CAPTURE_Disconnect(AUDIO_UNIT_PCM_CAPTURE_INDEX currentConnect, AUDIO_UNIT_PCM_CAPTURE_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[currentConnect = 0x%x] [inputConnect = 0x%x] \n", (unsigned int)currentConnect, (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_PCM_CAPTURE_Disconnect(currentConnect, inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_MP3_ENC_Connect(AUDIO_UNIT_MP3_ENC_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_MP3_ENC_Connect(AUDIO_UNIT_MP3_ENC_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[inputConnect = 0x%x] \n", (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_MP3_ENC_Connect(inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_MP3_ENC_Disconnect(AUDIO_UNIT_MP3_ENC_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_MP3_ENC_Disconnect(AUDIO_UNIT_MP3_ENC_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[inputConnect = 0x%x] \n", (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_MP3_ENC_Disconnect(inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_AAC_ENC_Connect(AUDIO_UNIT_AAC_ENC_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_AAC_ENC_Connect(AUDIO_UNIT_AAC_ENC_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[inputConnect = 0x%x] \n", (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_AAC_ENC_Connect(inputConnect);
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
MS_BOOL _API_AUDIO_UNIT_AAC_ENC_Disconnect(AUDIO_UNIT_AAC_ENC_INPUT inputConnect)
#else
MS_BOOL  API_AUDIO_UNIT_AAC_ENC_Disconnect(AUDIO_UNIT_AAC_ENC_INPUT inputConnect)
#endif
{
    ApiAudioUnitDBG("[inputConnect = 0x%x] \n", (unsigned int)inputConnect);

    return DRV_AUDIO_UNIT_AAC_ENC_Disconnect(inputConnect);
}

/* Start & Stop */

/* SPDIF */

/* HDMI */

/* ATV */

/* Decoder */

/* Common Decoder */

/* Common */

/* Customized patch */

/* Clip Play for ES */

/* Clip Play for PCM */

/* Gain, Mute & Delay */

/* AENC */

/* PCM Capture */

/* PCM IO Control */

/* MM New Mode */

/* Mstar Sound Effect */

/* Advanced Sound Effect */


//=====================================================================================================================================
//                   DebugMenu Begin (debug menu Always put on the bottom)
//=====================================================================================================================================
/*

===============================================================================
 * * * How to use * * *
===============================================================================


*/

static EN_API_AUDIO_CMD_IDX ApiAudioUnit_Command_Parser(St_Audio_Unit_Tunnel* pAudio_tunnel)
{
    MS_U32 i = 0;

    for(i = En_ApiAudioUnit_cmdIdx_DebugMenu; i < En_ApiAudioUnit_cmdIdx_Max; i++)
    {
        if(strlen(pAudio_tunnel->cmd) != strlen(API_AUDIO_COMMNAD_INFO[i].cCommmandName))
        {
            continue;
        }

        if(ApiAudioUnit_strcmp(pAudio_tunnel->cmd, API_AUDIO_COMMNAD_INFO[i].cCommmandName, strlen(API_AUDIO_COMMNAD_INFO[i].cCommmandName)) == 0)
        {
            return API_AUDIO_COMMNAD_INFO[i].u32CommmandIndex;
        }
    }

    ApiAudioUnitPRINT("[A][%s] [%d] unsupport function\n", __FUNCTION__, __LINE__);
    return En_ApiAudioUnit_cmdIdx_Max;
}

/* Debug */
#if UTPA_SUPPORT_KERNEL_IO_CTRL
void _API_AUDIO_UNIT_DebugMenu(void * pVoid, MS_BOOL scanf_bSupport)
#else
void  API_AUDIO_UNIT_DebugMenu(void * pVoid, MS_BOOL scanf_bSupport)
#endif
{
    AUDIO_UNIT_DEBUG_PARAM *dbg_param_t;
    St_Audio_Unit_Tunnel* pAudio_tunnel_set_param;
    pAudio_tunnel_set_param = (St_Audio_Unit_Tunnel*)pVoid;
    dbg_param_t = (AUDIO_UNIT_DEBUG_PARAM*)pAudio_tunnel_set_param->pData;
    API_AUDIO_UNIT_UNUSED(dbg_param_t);

    AUDIO_UNIT_DEBUG_INFO *dbg_info_t;
    St_Audio_Unit_Tunnel* pAudio_tunnel_get_info;
    pAudio_tunnel_get_info = (St_Audio_Unit_Tunnel*)pVoid;
    dbg_info_t = (AUDIO_UNIT_DEBUG_INFO*)pAudio_tunnel_get_info->pData;
    API_AUDIO_UNIT_UNUSED(dbg_info_t);

#if 0
    ApiAudioUnitPRINT("[AUDIO][%s] [%d] [param = %ld] [param2 = %ld] [param3 = %ld] [param4 = %ld] [param5 = %ld] [param6 = %ld] [param7 = %ld] [param8 = %ld] [param9 = %ld] [param10 = %ld]\n", __FUNCTION__, __LINE__,
            dbg_param_t->Dbg_Param,
            dbg_param_t->Dbg_Param2,
            dbg_param_t->Dbg_Param3,
            dbg_param_t->Dbg_Param4,
            dbg_param_t->Dbg_Param5,
            dbg_param_t->Dbg_Param6,
            dbg_param_t->Dbg_Param7,
            dbg_param_t->Dbg_Param8,
            dbg_param_t->Dbg_Param9,
            dbg_param_t->Dbg_Param10
            );

    ApiAudioUnitPRINT("[AUDIO][%s] [%d] [Info = %ld] [Info2 = %ld] [Info3 = %ld] [Info4 = %ld] [Info5 = %ld] [Info6 = %ld] [Info7 = %ld] [Info8 = %ld] [Info9 = %ld] [Info10 = %ld]\n", __FUNCTION__, __LINE__,
            dbg_info_t->Dbg_Info,
            dbg_info_t->Dbg_Info2,
            dbg_info_t->Dbg_Info3,
            dbg_info_t->Dbg_Info4,
            dbg_info_t->Dbg_Info5,
            dbg_info_t->Dbg_Info6,
            dbg_info_t->Dbg_Info7,
            dbg_info_t->Dbg_Info8,
            dbg_info_t->Dbg_Info9,
            dbg_info_t->Dbg_Info10
            );
#endif

    EN_API_AUDIO_CMD_IDX command_index = En_ApiAudioUnit_cmdIdx_Max;

    command_index = ApiAudioUnit_Command_Parser(pAudio_tunnel_set_param);
    command_index = ApiAudioUnit_Command_Parser(pAudio_tunnel_get_info);

    ApiAudioUnit_ShmInit();
    ApiAudioUnit_CreateThread();

    // For applications that don't use scanf, change the behavior so that
    // ApiAudioUnit_DebugMenu() will sleep until new arguments from newly added API
    // API_AUDIO_DebugMenu_Non_Scanf_ParseCommand() become available.
    pstAudioUnitShmData->g_audio_unit_scanf_bSupport = scanf_bSupport;

    switch(command_index)
    {
        case En_ApiAudioUnit_cmdIdx_DebugMenu:
        {
            if(pstAudioUnitShmData->g_audio_unit_scanf_bSupport == TRUE)
            {
                ApiAudioUnit_DebugMenu();
            }
            else if(pstAudioUnitShmData->g_audio_unit_scanf_bSupport == FALSE) //NON_SCANF version: Allow debug menu to work without scanf
            {
                #if(UPPER_SUPPORT_THREAD_DEBUG_MENU)
                {
                    ApiAudioUnit_CreateDebugMenuThread();
                }
                #else
                {
                    ApiAudioUnit_DebugMenu();
                }
                #endif
            }
        }
        break;

        default:
            break;
    }
    return;
}

#if UTPA_SUPPORT_KERNEL_IO_CTRL
void _API_AUDIO_UNIT_DebugMenu_Non_Scanf_ParseCommand(char *cmdString)
#else
void  API_AUDIO_UNIT_DebugMenu_Non_Scanf_ParseCommand(char *cmdString)
#endif
{
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space
//not support in Kernel Space

#else
    char *cmd = NULL;
    int index = 0;

    ApiAudioUnit_non_scanf_paramIdx = 0;
    ApiAudioUnit_non_scanf_paramsCount = 0;
    memset(ApiAudioUnit_non_scanf_params,    0, API_AUDIO_UNIT_NON_SCANF_DEBUG_CMD_PARAMS_MAX);
    memset(ApiAudioUnit_non_scanf_paramsHex, 0, API_AUDIO_UNIT_NON_SCANF_DEBUG_CMD_PARAMS_MAX);
    memset(ApiAudioUnit_non_scanf_paramsStr, 0, API_AUDIO_UNIT_DEBUG_INPUT_STR_LENGTH +1);

    cmd = strtok(cmdString, ";");
    while(cmd != NULL)
    {
        if(cmd[0] < 0x30 || cmd[0] > 0x39) // not integer (0 ~ 9)
        {
            // store the str in global var, only one string is supported in the command so far.
            strcpy(ApiAudioUnit_non_scanf_paramsStr, cmd);
            ApiAudioUnitPRINT("String: \033[0;33m %s \033[0m", ApiAudioUnit_non_scanf_paramsStr);
        }
        else
        {
            // parse the str in both decimal and heximal just in case
            ApiAudioUnit_non_scanf_params[index] = strtoul(cmd, (char**) NULL, 10);
            ApiAudioUnit_non_scanf_paramsHex[index] = strtoul(cmd, (char**) NULL, 16);
        }
        index++;

        cmd = strtok(NULL, ";");
        if(index >= API_AUDIO_UNIT_NON_SCANF_DEBUG_CMD_PARAMS_MAX)
        {
            break;
        }
    }
    ApiAudioUnit_non_scanf_paramsCount = index;

    ApiAudioUnitPRINT("\033[0;33m NON_SCANF: Parse debug command:");
    for(index = 0; index < ApiAudioUnit_non_scanf_paramsCount; index++)
    {
        ApiAudioUnitPRINT("%d ", ApiAudioUnit_non_scanf_params[index]);
    }
    ApiAudioUnitPRINT("\033[0m \n");
#endif
}

//=====================================================================================================================================
//                   HAL_AUDIO_DebugMenu End (debug menu Always put on the bottom)
//=====================================================================================================================================

//------------------------------------------------------------------------------------------------
//Bellow must be EMPTY!!!! DO NOT add any code!!!! (For Version Check)
//------------------------------------------------------------------------------------------------

//0x9996
void ApiAudioUnit_SHOW_ALL_VERSION(void)
{
    MS_U32 kdriver_Version = 0;

    #if defined(KDRIVER_AUDIO_VERSION)
    kdriver_Version = KDRIVER_AUDIO_VERSION;
    #endif //#if defined(KDRIVER_AUDIO_VERSION)

    ApiAudioUnitPRINT("\n\n\n\n\n\n");
    ApiAudioUnitPRINT("[---------------------------------------------]\n");
    ApiAudioUnitPRINT("[API_AUDIO_CTRL].VERSION..||[0x%-4X][0x%-4X]||]\n", __LINE__, (unsigned int)API_AUDIO_UNIT_VERSION);
    ApiAudioUnitPRINT("[KDRIVER_AUDIO]..VERSION..||[0x%-4X][0x%-4X]||]\n", __LINE__, (unsigned int)kdriver_Version);
    ApiAudioUnitPRINT("[---------------------------------------------]\n");
    ApiAudioUnitPRINT("\n\n\n\n\n\n");
}
//------------------------------------------------------------------------------------------------
//Bellow must be EMPTY!!!! DO NOT add any code!!!! (For Version Check)
//------------------------------------------------------------------------------------------------

#endif //#if defined(ROLLS_ROYCE) || defined(AUDIO_UNIT_CTRL)
