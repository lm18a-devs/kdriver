
#if !defined(MSOS_TYPE_LINUX_KERNEL)
#include <stdio.h>
#include <string.h>
#else
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/slab.h>
#endif

#include "utopia_private.h"
#include "utopia.h"
#include "utopia_dapi.h"
#include "MsOS.h"
#include "drvMMIO.h"
#include "utopia_driver_id.h"

#if defined(MSOS_TYPE_LINUX_KERNEL) && defined(STELLAR)
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
//MsOS Debug Process
#define MSOS_PMLOG_DEBUG_PROC_ROOT_NAME                        "MsOS"
#define MSOS_PMLOG_DEBUG_PROC_CHILD_DBG          "PMLog"           /* proc child 1  */


/* proc child 2 */
static ssize_t MSOS_PMLOG_Debug_Proc_Read(struct file *file, char __user *buffer, size_t count, loff_t *pos);
static ssize_t MSOS_PMLOG_Debug_Proc_Write(struct file *file, const char __user *buffer, size_t count, loff_t *pos);
/* Define a Proc structure for debug */

/* Define a Proc structure for debug */
struct MSOS_PMLog_Proc_Struct
{
    struct proc_dir_entry *proc_entry;
    struct file_operations proc_fops;
    char *proc_name;
    umode_t proc_mode;
};

static struct proc_dir_entry *msos_pmlog_proc_root = NULL;
static struct MSOS_PMLog_Proc_Struct msos_pmlog_proc_child[] =
{
    {
        /* proc child 1 */
        .proc_entry = NULL,
        .proc_fops  =
        {
            .owner  = THIS_MODULE,
            .read   = MSOS_PMLOG_Debug_Proc_Read,
            .write  = MSOS_PMLOG_Debug_Proc_Write,
        },
        .proc_name  = MSOS_PMLOG_DEBUG_PROC_CHILD_DBG,
        .proc_mode  = 0x0666,
    },
};

//-----------------------------------------------------------------------------
// Proc Operations (Init / Exit)
//-----------------------------------------------------------------------------
static int MSOS_PMLOG_Debug_Proc_Init(void)
{
    struct MSOS_PMLog_Proc_Struct *proc_child = NULL;
    int loop = 0;
    int ret = 0;
    if (msos_pmlog_proc_root == NULL){
        msos_pmlog_proc_root = proc_mkdir(MSOS_PMLOG_DEBUG_PROC_ROOT_NAME, NULL);
        if (msos_pmlog_proc_root == NULL)
        {
            printk(KERN_ERR "Error! fail to create root directory of proc filesystem (%s)!\n", MSOS_PMLOG_DEBUG_PROC_ROOT_NAME);
            ret = -EACCES;
        }
        else
        {
            for (loop = 0; loop < ARRAY_SIZE(msos_pmlog_proc_child); loop++)
            {
                proc_child = &msos_pmlog_proc_child[loop];

                proc_child->proc_entry = proc_create_data(proc_child->proc_name, proc_child->proc_mode, msos_pmlog_proc_root, &proc_child->proc_fops, NULL);
                if (proc_child->proc_entry == NULL)
                {
                    printk(KERN_ERR "Error! fail to create child directory of proc filesystem (%s)!\n", proc_child->proc_name);
                    ret = -EACCES;
                    break;
                }
            }
        }
    }
    return ret;
}

static inline void MSOS_PMLOG_Debug_Proc_Exit(void)
{
    struct MSOS_PMLog_Proc_Struct *proc_child = NULL;
    int loop = 0;
    if (msos_pmlog_proc_root){
        for (loop = 0; loop < ARRAY_SIZE(msos_pmlog_proc_child); loop++)
        {
            proc_child = &msos_pmlog_proc_child[loop];

            if (proc_child->proc_entry != NULL)
            {
                remove_proc_entry(proc_child->proc_name, msos_pmlog_proc_root);
            }
        }

        remove_proc_entry(MSOS_PMLOG_DEBUG_PROC_ROOT_NAME, NULL);
    }
    return;
}

static ssize_t MSOS_PMLOG_Debug_Proc_Read(struct file *file, char __user *buffer, size_t count, loff_t *pos)
{
    printk("MSTAR PM buffer size is :%d\n",MSOS_DEBUG_PROC_STRING_BUFFER_LENGTH);
    return MSOS_DEBUG_PROC_STRING_BUFFER_LENGTH;
}

static ssize_t MSOS_PMLOG_Debug_Proc_Write(struct file *file, const char __user *buffer, size_t count, loff_t *pos)
{
    char value_str[MSOS_DEBUG_PROC_STRING_BUFFER_LENGTH] = {0,};
    size_t copy_length = (count >= MSOS_DEBUG_PROC_STRING_BUFFER_LENGTH)?(MSOS_DEBUG_PROC_STRING_BUFFER_LENGTH-1):count;
    if (copy_length == 0)
        return -EFAULT;
    if (copy_from_user(value_str, buffer, copy_length))
        return -EFAULT;
    value_str[copy_length] = '\0';
    printk("%s",value_str);
    return copy_length;
}

#endif

UTOPIA_PRIVATE* psUtopiaPrivate;

char moduleNames[][40] = {
#define PREFIX(MODULE) "MODULE_"#MODULE,
	INCLUDED_MODULE
#undef PREFIX
};

char rpoolNames[][40] = {
#define PREFIX(MODULE) "RPOOL_"#MODULE,
	INCLUDED_MODULE
#undef PREFIX
};

char ResourceNames[][40] = {
#define PREFIX(MODULE) "RESOURCE_"#MODULE,
	INCLUDED_MODULE
#undef PREFIX
};

#define PREFIX(MODULE) MS_U32 MODULE_##MODULE##_OPEN = MODULE_##MODULE;
INCLUDED_MODULE
#undef PREFIX

#define PREFIX(MODULE) \
	extern __attribute__((weak)) MS_U32 MODULE##RegisterToUtopia(FUtopiaOpen ModuleType);
INCLUDED_MODULE
#undef PREFIX

#if defined(CONFIG_UTOPIA_FRAMEWORK_KERNEL_DRIVER_64BIT) || defined(CONFIG_UTOPIA_FRAMEWORK_KERNEL_DRIVER_32BIT)

typedef MS_U32 (*FUResgisterModuleList)(FUtopiaOpen ModuleType);

MS_U32 emptyRegisterToUtopia(FUtopiaOpen ModuleType)
{
    //printu("[utopia info] emptyRegisterToUtopia\n" );
	return 0;
}

//FUResgisterModuleList AAResgisterModule = emptyRegisterToUtopia;


#define PREFIX(MODULE) \
    FUResgisterModuleList MODULE##ResgisterModule = emptyRegisterToUtopia;
    INCLUDED_MODULE
#undef PREFIX




MS_U32 autoRegisterModule(void)
{

#define PREFIX(MODULE) \
	   if (MODULE##RegisterToUtopia != NULL && MODULE_TYPE_##MODULE##_FULL) \
	   { \
		   MODULE##ResgisterModule = MODULE##RegisterToUtopia; \
	   }
	   INCLUDED_MODULE
#undef PREFIX



   return 0;
}


#endif


MS_U32 UtopiaInit()
{
	MS_U32 u32Ret = 0;

	printu("[utopia info] utopia init\n" );
    psUtopiaPrivate = (UTOPIA_PRIVATE*)malloc(sizeof(UTOPIA_PRIVATE));
	if (!psUtopiaPrivate) {
		printu("utopia malloc UTOPIA_PRIVATE fail!\n" );
		return FALSE;
	}
	memset(psUtopiaPrivate, 0, sizeof(UTOPIA_PRIVATE));
	psUtopiaPrivate->u32MutexID = MsOS_CreateMutex(E_MSOS_FIFO
			, "UtopiaPrivateMutex", MSOS_PROCESS_SHARED);
    #if defined(MSOS_TYPE_LINUX_KERNEL) && defined(STELLAR)
    MSOS_PMLOG_Debug_Proc_Init();
    #endif

	MsOS_ObtainMutex(psUtopiaPrivate->u32MutexID, MSOS_WAIT_FOREVER);
#if defined(CONFIG_UTOPIA_FRAMEWORK_KERNEL_DRIVER_64BIT) || defined(CONFIG_UTOPIA_FRAMEWORK_KERNEL_DRIVER_32BIT)
    u32Ret=autoRegisterModule();

#define PREFIX(MODULE) \
	u32Ret |=(*(MODULE##ResgisterModule))((FUtopiaOpen)MODULE_TYPE_##MODULE##_FULL);
	INCLUDED_MODULE
#undef PREFIX



#else

#define PREFIX(MODULE) \
	if (MODULE##RegisterToUtopia != NULL && MODULE_TYPE_##MODULE##_FULL) \
	{ \
		u32Ret |= MODULE##RegisterToUtopia((FUtopiaOpen)MODULE_TYPE_##MODULE##_FULL); \
	}
	INCLUDED_MODULE
#undef PREFIX

#endif //CONFIG_UTOPIA_FRAMEWORK_KERNEL_DRIVER_64BIT
	MsOS_ReleaseMutex(psUtopiaPrivate->u32MutexID);

	return u32Ret;
}

MS_U32 UtopiaOpen(MS_U32 u32ModuleID, void** ppInstanceTmp
		, MS_U32 u32ModuleVersion, const void* const pAttribute)
{
    UTOPIA_MODULE* psUtopiaModule = psUtopiaPrivate->psModuleHead;
    UTOPIA_INSTANCE** ppInstance = (UTOPIA_INSTANCE**)ppInstanceTmp;


    while(psUtopiaModule != NULL)
    {
        if(psUtopiaModule->u32ModuleID == (u32ModuleID & (~KERNEL_MODE)))
        {
            int ret = psUtopiaModule->fpOpen((void**)ppInstance, pAttribute);

            if(MSOS_BRANCH_PREDICTION_UNLIKELY(ret))
            {
                printu("[utopia open error] fail to create instance\n");
				RET_OR_BLOCK(ret);
            }

            (*ppInstance)->psModule = psUtopiaModule;
			(*ppInstance)->u32AppRequireModuleVersion = u32ModuleVersion;
            return ret; /* depend on fpOpen, may not be UTOPIA_STATUS_SUCCESS */
        }
        psUtopiaModule = psUtopiaModule->psNext;
    }
	return UTOPIA_STATUS_FAIL;
}
EXPORT_SYMBOL(UtopiaOpen);

MS_U32 UtopiaIoctl(void* pInstanceTmp, MS_U32 u32Cmd, void* const pArgs)
{
	UTOPIA_INSTANCE* pInstance = (UTOPIA_INSTANCE*)pInstanceTmp;
	/* check param. */
	if (MSOS_BRANCH_PREDICTION_UNLIKELY(pInstance == NULL))
	{
		printu("[utopia param error] instance pointer should not be null\n");
		RET_OR_BLOCK(UTOPIA_STATUS_FAIL);
	}

	return TO_INSTANCE_PTR(pInstance)->psModule->fpIoctl(pInstance,
			u32Cmd, pArgs);
}
EXPORT_SYMBOL(UtopiaIoctl);

MS_U32 UtopiaClose(void* pInstantTmp)
{
    UTOPIA_INSTANCE* pInstant = (UTOPIA_INSTANCE*)pInstantTmp;

    UTOPIA_MODULE* psUtopiaModule = psUtopiaPrivate->psModuleHead;
    while(psUtopiaModule != NULL)
    {
        if(psUtopiaModule->u32ModuleID == pInstant->psModule->u32ModuleID)
        {
            return psUtopiaModule->fpClose(pInstant);
        }
        psUtopiaModule = psUtopiaModule->psNext;
    }
	return 0;
}
EXPORT_SYMBOL(UtopiaClose);
