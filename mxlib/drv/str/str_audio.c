#include <linux/device.h>
#include <linux/export.h>
#include <linux/delay.h>

#include "mst_str.h"

#include "../../include/drvAUDIO_if.h"
#include "../audio/internal/drvMAD.h"
#include "../audio/internal/drvADVSOUND.h"
#include "../audio/internal/drvAUDIO_internal.h"
#include "drvAUDIO.h"
#include "halAUDIO.h"
#include "halSOUND.h"
#include "halAUDSP.h"
#include "halMAD.h"
#include "halMAD2.h"
#include "halADVAUDIO.h"
#include "regAUDIO.h"
#include "halAUR2.h"
#include "halSIF.h"

//-------------------------------------------------------------------------------------------------
//  Driver Compiler Options
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Local struct define
//-------------------------------------------------------------------------------------------------
#define NumRegs 8
#define NumRows 15
#define LGSE_Restore_In_Kernel 0

typedef struct
{
    MS_U16 aud_reg[NumRegs];
} AU_RowRegs_t;

typedef struct
{
    AU_RowRegs_t aud_row[NumRows];
    MS_U32 u32Bank;
} AU_BankRegs_t;

//-------------------------------------------------------------------------------------------------
//  Extern Variables
//-------------------------------------------------------------------------------------------------
extern AUDIO_SHARED_VARS2 * g_AudioVars2;

//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------
MS_U32 MAD_Base_Addr = 0x0;

static AUDIO_DSP_CODE_TYPE Dsp2CodeType = AU_DVB_STANDARD_INVALID;
static AUDIO_DSP_CODE_TYPE SifCodeType = AU_DVB_STANDARD_INVALID;
static AUDIO_DSP_CODE_TYPE ADEC0_R2CodeType = AU_DVB_STANDARD_INVALID;
static AUDIO_DSP_CODE_TYPE ADEC1_R2CodeType = AU_DVB_STANDARD_INVALID;
static struct timer_list ADC_charge_timer;

#if( LGSE_Restore_In_Kernel == 1)
static MS_BOOL bIsSTR_Suspend_LGSE_Fail = FALSE;
MS_U8 U8LGSEFN_MODE_REGISTER[4];
MS_U8 U8LGSEFNMAIN_INIT[40];
MS_U8 U8LGSEFNMAIN_VARIABLES[12];
MS_U8 U8LGSEFN000_INIT[1180];
MS_U8 U8LGSEFN000_VARIABLES[4];
MS_U8 U8LGSEFN001_INIT[84];
MS_U8 U8LGSEFN001_VARIABLES[520];
MS_U8 U8LGSEFN004_MODE1_VARIABLES[100];
MS_U8 U8LGSEFN004_MODE2_VARIABLES[60];
MS_U8 U8LGSEFN009_VARIABLES[8];
MS_U8 U8LGSEFN010_INIT[40];
MS_U8 U8LGSEFN017_VARIABLES[648];
MS_U8 U8LGSEFN028_VARIABLES[8];
#endif

AU_BankRegs_t AU_Bank112A;
AU_BankRegs_t AU_Bank112B;
AU_BankRegs_t AU_Bank112C;
AU_BankRegs_t AU_Bank112D;
AU_BankRegs_t AU_Bank112E;
AU_BankRegs_t AU_Bank1603;

//-------------------------------------------------------------------------------------------------
//  Local functions
//-------------------------------------------------------------------------------------------------
static void AU_Backup_BaseAddr(void)
{
    static MS_BOOL bIsBackup = FALSE;

    if (bIsBackup == FALSE)
    {
        // Backup MAD base addr
        MAD_Base_Addr = HAL_AUDIO_GetDspMadBaseAddr(DSP_ADV);
        HAL_AUDIO_SetDspBaseAddr(0, MAD_Base_Addr, MAD_Base_Addr);
        bIsBackup = TRUE;
    }
}

static void AU_BankRegs_Initialize(MS_U32 u32Bank, AU_BankRegs_t * pBank)
{
    int i = 0, j = 0;
    pBank->u32Bank = u32Bank;

    for (i = 0; i < NumRows; i++)
    {
        for (j = 0; j < NumRegs; j++)
        {
            pBank->aud_row[i].aud_reg[j] = 0x0000;
        }
    }
}

static void AU_Backup_Bank(AU_BankRegs_t * pBank)
{
    int i = 0, j = 0;
    if (pBank == NULL)
    {
        printk("AU_Backup_Bank(),  pBank is NULL!! \r\n");
        return;
    }

    STR_PRINT("pBank->u32Bank:%x\n", (int)pBank->u32Bank);

    for (i = 0; i < NumRows; i++)
    {
        for (j = 0; j < NumRegs; j++)
        {
            //printk("((i << 16) +(j << 1)) = %x       ",((i << 4) +(j << 1)));
            //printk("stBank.u32Bank = %x       ",pBank->u32Bank);
            MS_U32 ReadRegAddr = pBank->u32Bank + ((i << 4) + (j << 1));
            //printk("ReadRegAddr = %x       ",ReadRegAddr);
            pBank->aud_row[i].aud_reg[j] = HAL_AUDIO_AbsReadReg(ReadRegAddr);
            //printk("pBank->aud_row[%d].aud_reg[%d] = %x\n",i, j, pBank->aud_row[i].aud_reg[j]);
            //printk("%-4x ", pBank->aud_row[i].aud_reg[j]);
        }
        //printk("\n");
    }
}

static void AU_Restore_Bank(AU_BankRegs_t *stBank)
{
    int i = 0, j = 0;

    STR_PRINT("stBank.u32Bank:%x\n", (int)stBank->u32Bank);

    for (i = 0; i < NumRows; i++)
    {
        for (j = 0; j < NumRegs; j++)
        {
            MS_U32 ReadRegAddr = stBank->u32Bank + ((i << 4) + (j << 1));
            //printk("ReadRegAddr = %x     ",ReadRegAddr);
            HAL_AUDIO_AbsWriteReg(ReadRegAddr, stBank->aud_row[i].aud_reg[j]);
            //printk("HAL_AUDIO_AbsReadReg(ReadRegAddr) = %x \n", HAL_AUDIO_AbsReadReg(ReadRegAddr));
        }
        //printk("\n");
    }
}

#if( LGSE_Restore_In_Kernel == 1)
extern MS_BOOL MDrv_ADVSOUND_SubProcessEnable(ADVFUNC proc, MS_BOOL enable);

static void AU_LGSE_Initialize(void)
{
    memset(U8LGSEFN_MODE_REGISTER, 0, sizeof(U8LGSEFN_MODE_REGISTER));
    memset(U8LGSEFNMAIN_INIT     , 0, sizeof(U8LGSEFNMAIN_INIT));
    memset(U8LGSEFNMAIN_VARIABLES, 0, sizeof(U8LGSEFNMAIN_VARIABLES));
    memset(U8LGSEFN000_INIT      , 0, sizeof(U8LGSEFN000_INIT));
    memset(U8LGSEFN000_VARIABLES , 0, sizeof(U8LGSEFN000_VARIABLES));
    memset(U8LGSEFN001_INIT      , 0, sizeof(U8LGSEFN001_INIT));
    memset(U8LGSEFN001_VARIABLES , 0, sizeof(U8LGSEFN001_VARIABLES));
    memset(U8LGSEFN004_MODE1_VARIABLES, 0, sizeof(U8LGSEFN004_MODE1_VARIABLES));
    memset(U8LGSEFN004_MODE2_VARIABLES, 0, sizeof(U8LGSEFN004_MODE2_VARIABLES));
    memset(U8LGSEFN009_VARIABLES , 0, sizeof(U8LGSEFN009_VARIABLES));
    memset(U8LGSEFN010_INIT      ,     0, sizeof(U8LGSEFN010_INIT));
    memset(U8LGSEFN017_VARIABLES , 0, sizeof(U8LGSEFN017_VARIABLES));
    memset(U8LGSEFN028_VARIABLES , 0, sizeof(U8LGSEFN028_VARIABLES));
}

static void AU_LGSE_Backup(void)
{
    MS_U32 U32Ret = 0;

    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN_MODE_REGISTER     , 0, (MS_U32 *)U8LGSEFN_MODE_REGISTER     , 4);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFNMAIN_INIT          , 0, (MS_U32 *)U8LGSEFNMAIN_INIT          , 40);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFNMAIN_VARIABLES     , 0, (MS_U32 *)U8LGSEFNMAIN_VARIABLES     , 12);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN000_INIT           , 0, (MS_U32 *)U8LGSEFN000_INIT           , 1180);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN000_VARIABLES      , 0, (MS_U32 *)U8LGSEFN000_VARIABLES      , 4);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN001_INIT           , 0, (MS_U32 *)U8LGSEFN001_INIT           , 84);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN001_VARIABLES      , 0, (MS_U32 *)U8LGSEFN001_VARIABLES      , 520);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN004_MODE1_VARIABLES, 0, (MS_U32 *)U8LGSEFN004_MODE1_VARIABLES, 100);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN004_MODE2_VARIABLES, 0, (MS_U32 *)U8LGSEFN004_MODE2_VARIABLES, 60);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN009_VARIABLES      , 0, (MS_U32 *)U8LGSEFN009_VARIABLES      , 8);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN010_INIT           , 0, (MS_U32 *)U8LGSEFN010_INIT           , 40);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN017_VARIABLES      , 0, (MS_U32 *)U8LGSEFN017_VARIABLES      , 648);
    U32Ret += (MS_U32)MDrv_ADVSOUND_GetParam2(LGSEFN028_VARIABLES      , 0, (MS_U32 *)U8LGSEFN028_VARIABLES      , 8);

    //take care of this number
    if(U32Ret < 13)
    {
        printk("U32Ret: %u, LGSE may have problem during suspend\r\n", U32Ret);
        bIsSTR_Suspend_LGSE_Fail = TRUE;
    }
    else
    {
        bIsSTR_Suspend_LGSE_Fail = FALSE;
    }

}

static void AU_LGSE_Restore(void)
{
    MS_U32 U32Ret = 0;

    MDrv_ADVSOUND_SubProcessEnable(LG_SOUNDENGINE_EN  , TRUE);
    U32Ret += (MS_U32)MDrv_ADVSOUND_SetParam2(LGSEFN_MODE_REGISTER     , 0, (MS_U32 *)U8LGSEFN_MODE_REGISTER     , 4);    AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFNMAIN_INIT                            , 0, (MS_U32 *)U8LGSEFNMAIN_INIT          , 40);   AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFNMAIN_VARIABLES                       , 0, (MS_U32 *)U8LGSEFNMAIN_VARIABLES     , 12);   AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFN000_INIT                             , 0, (MS_U32 *)U8LGSEFN000_INIT           , 1180); AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFN000_VARIABLES                        , 0, (MS_U32 *)U8LGSEFN000_VARIABLES      , 4);    AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFN001_INIT                             , 0, (MS_U32 *)U8LGSEFN001_INIT           , 84);   AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFN001_VARIABLES                        , 0, (MS_U32 *)U8LGSEFN001_VARIABLES      , 520);  AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFN004_MODE1_VARIABLES                  , 0, (MS_U32 *)U8LGSEFN004_MODE1_VARIABLES, 100);  AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFN004_MODE2_VARIABLES                  , 0, (MS_U32 *)U8LGSEFN004_MODE2_VARIABLES, 60);   AUDIO_DELAY1US(100);
    U32Ret += (MS_U32)MDrv_ADVSOUND_SetParam2(LGSEFN009_VARIABLES      , 0, (MS_U32 *)U8LGSEFN009_VARIABLES      , 8);    AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFN010_INIT                             , 0, (MS_U32 *)U8LGSEFN010_INIT           , 40);   AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFN017_VARIABLES                        , 0, (MS_U32 *)U8LGSEFN017_VARIABLES      , 648);  AUDIO_DELAY1US(100);
    MDrv_ADVSOUND_SetParam2(LGSEFN028_VARIABLES                        , 0, (MS_U32 *)U8LGSEFN028_VARIABLES      , 8);    AUDIO_DELAY1US(100);

    if( (U32Ret < 2) || (bIsSTR_Suspend_LGSE_Fail == TRUE) )
    {
        printk("LGSE may have problem during resume\r\n");
        bIsSTR_Suspend_LGSE_Fail = FALSE;
    }
}
#endif

static void AU_SIF_LoadCode(void)
{
    HAL_SIF_SendCmd(AU_SIF_CMD_ENABLE_CHANNEL, FALSE, 0);	// REset SIF
    HAL_SIF_SendCmd(AU_SIF_CMD_ENABLE_SIF_SYNTHESIZER, FALSE, 0x00);	// disable SIF Audio sythesizer & disable DVB fix-sync mode

    HAL_AUDIO_AbsWriteMaskByte(0x112CC9, 0x20, 0x20); //0x112CC8[13] = 1 to IDMA port selection to SE DSP

    MsOS_DisableInterrupt(E_INT_FIQ_DEC_DSP2UP);

    if(HAL_AUDIO_Alg2ReloadCode(SifCodeType) == TRUE)
    {
        HAL_SIF_SetDspCodeType(SifCodeType);
    }
    MsOS_EnableInterrupt(E_INT_FIQ_DEC_DSP2UP);

    HAL_SIF_SendCmd(AU_SIF_CMD_ENABLE_CHANNEL, TRUE, 0);
    // Enable SIF Audio synthesizer here to prevent reload unstable noise.
    HAL_SIF_SendCmd(AU_SIF_CMD_ENABLE_SIF_SYNTHESIZER, TRUE, 0x04);	// enable SIF Audio sythesizer

    if (SifCodeType == AU_SIF_PALSUM)
    {
        HAL_SIF_SendCmd(AU_SIF_CMD_SET_CARRIER_DEBOUNCE, 0x80, 0);
        HAL_SIF_SendCmd(AU_SIF_CMD_ENABLE_AGC, TRUE, 0);
        HAL_SIF_SendCmd(AU_SIF_CMD_DK123_MONITOR, TRUE, 0);
    }

    HAL_SIF_SendCmd(AU_SIF_CMD_ENABLE_AUTO_MUTE, FALSE, 0);
    HAL_SIF_SendCmd(AU_SIF_CMD_ENABLE_AUTO_SOUNDMODE, TRUE, 0);

    HAL_SIF_TriggerSifPLL();
}


void _AUDIO_STR_SHOW_VERSION(void);

static void ADC_charge_callback(void)
{
    STR_PRINT("ADC FAST CHARGE OFF\n");
    HAL_AUDIO_AbsWriteMaskReg(REG_ADC_FAST_RECHARGE, 0x1000, 0x0000); //ADC fast charge off
}

extern int suspend_audio(void)
{
    STR_PRINT("enter\n");

    del_timer_sync(&ADC_charge_timer);

   _AUDIO_STR_SHOW_VERSION();

    MDrv_AUDIO_PCM_DeInit();
    MDrv_AUDIO_SET_INIT_FLAG(FALSE);

    AU_Backup_BaseAddr();

    //Mute CH5/CH6 to prevent loud sound when resume due to LGSE reload is not finished.
    //The mute will be recovered to original status by _HAL_AUDIO_MuteDuringLimitedTime_Input in _HAL_AUDIO_POWER_ON_Monitor.
    HAL_SOUND_SetMixModeMute(E_AUDIO_INFO_GAME_IN, GAME1_VOL, TRUE);
    HAL_SOUND_SetMixModeMute(E_AUDIO_INFO_GAME_IN, GAME2_VOL, TRUE);


    AU_BankRegs_Initialize(0x112C00, &AU_Bank112C);
    AU_BankRegs_Initialize(0x112D00, &AU_Bank112D);
    AU_BankRegs_Initialize(0x112E00, &AU_Bank112E);
    AU_BankRegs_Initialize(0x160300, &AU_Bank1603);


    AU_Backup_Bank(&AU_Bank112C);
    AU_Backup_Bank(&AU_Bank112D);
    AU_Backup_Bank(&AU_Bank112E);
    AU_Backup_Bank(&AU_Bank1603);


#if(LGSE_Restore_In_Kernel == 1)
    //Back up LGSE parameters
    AU_LGSE_Initialize();
    AU_LGSE_Backup();
#endif

    Dsp2CodeType = HAL_AUDIO_GetDsp2CodeType();
    SifCodeType = HAL_SIF_GetDspCodeType();
    ADEC0_R2CodeType = HAL_AUDIO_GetDecoder1_Type();
    ADEC1_R2CodeType = HAL_AUDIO_GetDecoder2_Type();

    HAL_MAD_SetDecCmd(AU_DVB_DECCMD_STOP);
    HAL_MAD2_SetDecCmd2(AU_DVB_DECCMD_STOP);

    STR_PRINT("exit\n");
    return 0;
}

#define AU_STR_RESUME_TIME_MEASURE 0
#if AU_STR_RESUME_TIME_MEASURE
unsigned int u32Timer;
unsigned int u32Timer_us;
void AU_STR_Resume_Time_Measure_Timer_Reset(void)
{
    u32Timer = 0;
    u32Timer_us = 0;
    REG_WR(REG_PIU_TIMER0(0x00), (REG_RR(REG_PIU_TIMER0(0x00)) & (~(0x1))));
    REG_WR(REG_PIU_TIMER0(0x02), 0xFFFF);
    REG_WR(REG_PIU_TIMER0(0x03), 0xFFFF);
    REG_WR(REG_PIU_TIMER0(0x00), (REG_RR(REG_PIU_TIMER0(0x00)) | (0x1)));
}
void AU_STR_Resume_Time_Measure_Read_Timer(void)
{
    u32Timer = (((MS_U32)REG_RR(REG_PIU_TIMER0(0x04))) | ((MS_U32)REG_RR(REG_PIU_TIMER0(0x05)) << 16));
    u32Timer_us = ((u32Timer))/12;
    printk("resume time : %d.%06ds\n", u32Timer_us/1000000, u32Timer_us%1000000);
}
#endif

extern int resume_audio_part1(void)
{
    MS_U32 time_out = 0;

    STR_PRINT("enter\n");

#if AU_STR_RESUME_TIME_MEASURE
    AU_STR_Resume_Time_Measure_Timer_Reset();
#endif

    HAL_AUDIO_WritePreInitTable();
    HAL_AUDIO_WriteInitTable();
    HAL_AUDIO_SetPowerOn(TRUE);
    HAL_AUDIO_AbsWriteMaskReg(REG_ADC_FAST_RECHARGE, 0x1000, 0x1000); //ADC fast charge

    //Mute CH5/CH6 to prevent loud sound when resume due to LGSE reload is not finished.
    //The mute will be recovered to original status by _HAL_AUDIO_MuteDuringLimitedTime_Input in _HAL_AUDIO_POWER_ON_Monitor.
    HAL_SOUND_SetMixModeMute(E_AUDIO_INFO_GAME_IN, GAME1_VOL, TRUE);
    HAL_SOUND_SetMixModeMute(E_AUDIO_INFO_GAME_IN, GAME2_VOL, TRUE);

    HAL_AUDIO_SetDspBaseAddr(0, MAD_Base_Addr,MAD_Base_Addr);
    HAL_MAD_SetMemInfo();
    HAL_MAD2_SetMemInfo();

    HAL_SIF_Init();
    HAL_SOUND_Init();
    HAL_AUDSP_DspLoadCode(AU_DVB2_NONE);
    HAL_AUDIO_SeSystemLoadCode();

    HAL_MAD_Init();

    if (IS_AUDIO_DSP_CODE_TYPE_SIF(SifCodeType))
    {
#if 1 // LGE_WebOS_Customized
        //WOSQMST-2301 SIF init conflict with DTV demod init after quick start
        if (g_AudioVars2->eAudioSource == E_AUDIO_INFO_ATV_IN) //just for ATV & APVR case
#else
        if (g_AudioVars2->eAudioSource != E_AUDIO_INFO_DTV_IN) //for ATV & APVR case, need to reload SIF
#endif
        {
            AU_SIF_LoadCode();
        }
        else //If DTV, don't need to load SIF to prevent resume time becomes longer. Reset g_AudioVars2->g_eSifCodeType here, and it would reload when start play by AP
        {
            HAL_SIF_SetDspCodeType(AU_SIF_NONE);
        }
    }

    if (IS_AUDIO_DSP_CODE_TYPE_SE_ENC(Dsp2CodeType))	// Consider DSP encoder case
    {
        /***********************
        If load encoder here, resume time would be longer.
        So reset clear g_AudioVars2->g_u8Dsp2CodeType here, and it would reload when start play by AP.
        ***********************/
        HAL_AUDIO_SetDsp2CodeType(AU_DVB2_ENCODE_NONE, TRUE);
    }

    AU_Restore_Bank(&AU_Bank112C);
    AU_Restore_Bank(&AU_Bank112D);
    AU_Restore_Bank(&AU_Bank112E);
    AU_Restore_Bank(&AU_Bank1603);

    //Because restore 0x112C bank will clear fast charge. So do it here again.
    HAL_AUDIO_AbsWriteMaskReg(REG_ADC_FAST_RECHARGE, 0x1000, 0x1000); //ADC fast charge

    MDrv_AUDIO_PCM_Init();
    MDrv_AUDIO_SET_INIT_FLAG(TRUE);

    STR_PRINT("exit\n");
    return 0;
}

extern int resume_audio_part2(void)
{
    STR_PRINT("enter\n");

    //Restore decoder type
    if(ADEC0_R2CodeType != AU_DVB_STANDARD_INVALID)
    {
        HAL_AUDIO_SetSystem2(AU_DEC_ID1, ADEC0_R2CodeType);
    }
    if(ADEC1_R2CodeType != AU_DVB_STANDARD_INVALID)
    {
        HAL_AUDIO_SetSystem2(AU_DEC_ID3, ADEC1_R2CodeType);
    }

    //Enable DPGA to have fading to prevent pop noise
    HAL_MAD_SetCommInfo(Audio_Comm_infoType_ADEC1_Decoder_Output_Fading, TRUE, 0);
    HAL_MAD_SetCommInfo(Audio_Comm_infoType_ADEC2_Decoder_Output_Fading, TRUE, 0);

    //Enable HDMI RX FREQ untable protection and set threshold same as Madp layer
    HAL_MAD_SetAudioParam2(AU_DEC_ID1, Audio_ParamType_HDMI_RX_Unstable_Threshold, 0x600);
    HAL_MAD_SetAudioParam2(AU_DEC_ID1, Audio_ParamType_HDMI_Event_Mute_Enable, TRUE);

#if(LGSE_Restore_In_Kernel == 1)
    // Reload LGSE parameters
    AU_LGSE_Restore();
#endif


#if(LGE_N_DECODE)
    HAL_MAD_SetAudioParam2(AU_DEC_ID1, Audio_ParamType_MM_TS_PTS_WITH_MSB, 1);
    HAL_MAD_SetAudioParam2(AU_DEC_ID3, Audio_ParamType_MM_TS_PTS_WITH_MSB, 1);
    HAL_MAD_SetAudioParam2(AU_DEC_ID4, Audio_ParamType_MM_TS_PTS_WITH_MSB, 1);
    HAL_MAD_SetAudioParam2(AU_DEC_ID5, Audio_ParamType_MM_TS_PTS_WITH_MSB, 1);
#else
    HAL_MAD_SetCommInfo(Audio_Comm_infoType_ADEC1_33bit_PTS, 1, 0);
    HAL_MAD_SetCommInfo(Audio_Comm_infoType_ADEC2_33bit_PTS, 1, 0);
#endif

    HAL_MAD_SetAudioParam2(AU_DEC_ID1, Audio_ParamType_STR_Resume, TRUE);

    init_timer(&ADC_charge_timer);
    ADC_charge_timer.function = ADC_charge_callback;
    ADC_charge_timer.expires = jiffies + 10 * HZ;

    STR_PRINT("ADD TIMER \n");
    add_timer(&ADC_charge_timer);

#if AU_STR_RESUME_TIME_MEASURE
    AU_STR_Resume_Time_Measure_Read_Timer();
#endif

    STR_PRINT("exit\n");

    return 0;
}


#define AUDIO_STR_VERSION 0x002D
void _AUDIO_STR_SHOW_VERSION(void)
{
    STR_PRINT("\n");
    STR_PRINT("[--------------------------------------]\n");/*@audio*/
    STR_PRINT("[AU_STR] VERSION........[0x%-4X][0x%-4X]\n", __LINE__, AUDIO_STR_VERSION);/*@audio*/
    STR_PRINT("[--------------------------------------]\n");/*@audio*/
}
