#include <linux/device.h>
#include <linux/export.h>
#include <linux/io.h>
#include "mst_str.h"
#include "mst_reg_bank.h"
#include "controller.h"
#include "drvHVD_def.h"
#include "halVPU_EX.h"
#include "regHVD_EX.h"
#include "regVPU_EX.h"
#include "drvMVOP.h"
#include "regMVOP.h"
#include "halMVOP.h"


#define BANK_SIZE 0x80
#define REG_BASE_ADDR  0x100000
#define CHIP_BANK_ADR  (REG_BASE_ADDR+CHIP_REG_BASE)

typedef struct
{
    MS_U16 vdec_mvop_bank[BANK_SIZE];
    MS_U16 vdec_submvop_bank[BANK_SIZE];
    MS_U16 vdec_ip_clock[BANK_SIZE];
    MS_U16 chip_bank[BANK_SIZE];
    MS_BOOL  vdec_is_work[4];
    MS_BOOL  mvop_is_work[2];
    MS_BOOL  bSuspend;
    MS_BOOL  bResume;
    MS_BOOL  bMVOPSuspend;
    MS_BOOL  bMVOPResume;
}vdec_save_reg_t;

#ifdef STELLAR
extern void _MApi_VDEC_EX_V2_QSMOff(MS_U8 u8TaskId);
#endif

static vdec_save_reg_t vdec_save_reg = {0,};

static void mvop_store_reg(void)
{
    int start_index = 0;
    if(vdec_save_reg.bMVOPSuspend)
        return;
    for(start_index = 0x00;start_index<BANK_SIZE;start_index++)
    {
        vdec_save_reg.vdec_mvop_bank[start_index] = MDrv_STR_Read2Byte(MVOP_BANK_ADDR,start_index);
    }
    #if MVOP_SUPPORT_SUB
    for(start_index = 0x00;start_index<BANK_SIZE;start_index++)
    {
        vdec_save_reg.vdec_submvop_bank[start_index] = MDrv_STR_Read2Byte(SUBMVOP_BANK_ADDR,start_index);
    }
    #endif
    for(start_index = 0x00;start_index<BANK_SIZE;start_index++)
    {
        vdec_save_reg.chip_bank[start_index] = MDrv_STR_Read2Byte(CHIP_BANK_ADR,start_index);
    }
    vdec_save_reg.bMVOPSuspend = TRUE;
    vdec_save_reg.bMVOPResume = FALSE;
}

static void mvop_load_reg(void)
{
    int start_index = 0;
    if(vdec_save_reg.bMVOPResume)
        return;
    for(start_index = 0x00;start_index<BANK_SIZE;start_index++)
    {
        MDrv_STR_Write2Byte(CHIP_BANK_ADR,start_index,vdec_save_reg.chip_bank[start_index]);
    }
    for(start_index = 0x00;start_index<BANK_SIZE;start_index++)
    {
        MDrv_STR_Write2Byte(MVOP_BANK_ADDR,start_index,vdec_save_reg.vdec_mvop_bank[start_index]);
    }
    #if MVOP_SUPPORT_SUB
    for(start_index = 0x00;start_index<BANK_SIZE;start_index++)
    {
        MDrv_STR_Write2Byte(SUBMVOP_BANK_ADDR,start_index,vdec_save_reg.vdec_submvop_bank[start_index]);
    }
    #endif
    vdec_save_reg.mvop_is_work[0] =  vdec_save_reg.mvop_is_work[1] = FALSE;
    vdec_save_reg.bMVOPSuspend = FALSE;
    vdec_save_reg.bMVOPResume = TRUE;
}

static void restore_clkgen_mvop_register(void)
{
    int start_index = 0;

    //stc related
    MDrv_STR_Write2Byte(CLKGEN,0x05,vdec_save_reg.vdec_ip_clock[0x05]);
    MDrv_STR_Write2Byte(CLKGEN,0x06,vdec_save_reg.vdec_ip_clock[0x06]);
    MDrv_STR_Write2ByteMsk(CLKGEN,0x04,0x4,0x4);
    MDrv_STR_Write2ByteMsk(CLKGEN,0x04,0x0,0x4);
    //DC related
    MDrv_STR_Write2Byte(CLKGEN,0x74,vdec_save_reg.vdec_ip_clock[0x74]);
    MDrv_STR_Write2Byte(CLKGEN,0x75,vdec_save_reg.vdec_ip_clock[0x75]);
    MDrv_STR_Write2ByteMsk(CLKGEN,0x70,0x2,0x2);
    MDrv_STR_Write2ByteMsk(CLKGEN,0x70,0x0,0x2);
    MDrv_STR_Write2ByteMsk(CLKGEN,0x70,0x200,0x200);
    MDrv_STR_Write2ByteMsk(CLKGEN,0x70,0x0,0x200);
    //MVOP related
    MDrv_STR_Write2Byte(CLKGEN,0x4C,0x100);
}

static void vdec_store_reg(void)
{
    if(vdec_save_reg.bSuspend)
        return;
    #if 1//defined(SUPPORT_VDEC_STR)
    vdec_save_reg.vdec_is_work[0] = HAL_VPU_EX_CheckSTRState(0);
    vdec_save_reg.vdec_is_work[1] = HAL_VPU_EX_CheckSTRState(1);
    vdec_save_reg.mvop_is_work[0] = HAL_MVOP_GetEnableState();
    vdec_save_reg.mvop_is_work[1] = HAL_MVOP_SubGetEnableState();
    #else
    vdec_save_reg.vdec_is_work[0] = FALSE;
    vdec_save_reg.vdec_is_work[1] = FALSE;
    #endif

    if(vdec_save_reg.vdec_is_work[0] || vdec_save_reg.vdec_is_work[1])
    {
        if(MDrv_STR_Read2Byte(CLKGEN,0x30)&0x1)
        {
            vdec_save_reg.vdec_is_work[0] = FALSE;
            vdec_save_reg.vdec_is_work[1] = FALSE;
        }
    }

    if(vdec_save_reg.vdec_is_work[0] || vdec_save_reg.vdec_is_work[1])
    {
        STR_PRINT("VDEC suspend to store register\n");
        int start_index = 0;
        for(start_index = 0x00;start_index<BANK_SIZE;start_index++)
        {
            vdec_save_reg.vdec_ip_clock[start_index] = MDrv_STR_Read2Byte(CLKGEN,start_index);
        }
    }
    HAL_MVOP_PowerStateSuspend();
    #if defined(SUPPORT_VDEC_STR)
    HAL_VPU_EX_EnableSTRResume(0,vdec_save_reg.vdec_is_work[0]);
    HAL_VPU_EX_EnableSTRResume(1,vdec_save_reg.vdec_is_work[1]);
    #endif
    #ifdef STELLAR
    if (vdec_save_reg.vdec_is_work[0])
    {
        _MApi_VDEC_EX_V2_QSMOff(0);
    }
    if (vdec_save_reg.vdec_is_work[1])
    {
        _MApi_VDEC_EX_V2_QSMOff(1);
    }
    #endif
    vdec_save_reg.bSuspend = TRUE;
    vdec_save_reg.bResume = FALSE;
}
static void vdec_load_reg(void)
{
    if(vdec_save_reg.bResume)
        return;

    if(vdec_save_reg.vdec_is_work[0] || vdec_save_reg.vdec_is_work[1])
    {
        #if defined(SUPPORT_VDEC_STR)
        STR_PRINT("VDEC resume to store register in ver 2\n");
        restore_clkgen_mvop_register();
        HAL_VPU_EX_InitAllSTR();
        #endif
    }
    vdec_save_reg.bSuspend = FALSE;
    vdec_save_reg.bResume = TRUE;
}

int suspend_vdec(void)
{
    STR_PRINT("enter\n");
    vdec_store_reg();
    STR_PRINT("exit\n");
    return 0;
}

int resume_vdec(void)
{
    STR_PRINT("enter\n");
    vdec_load_reg();
    STR_PRINT("exit\n");
    return 0;
}

int init_vdec(void)
{
    STR_PRINT("enter\n");
    memset((void*)(&vdec_save_reg),0,sizeof(vdec_save_reg_t));
    STR_PRINT("exit\n");
    return 0;
}


