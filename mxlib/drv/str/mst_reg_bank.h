#ifndef __MST_REG_BANK_H
#define __MST_REG_BANK_H

#define R2_BANK_ADDR  0x100300
#define MAU_BANK_ADDR  0x100400
#define SUBMVOP_BANK_ADDR 0x101300
#define MVOP_BANK_ADDR  0x101400
#define HVD_BANK_ADDR   0x101B00
#define PM_SLEEP              0x000E00
#define ADC_ATOP                    0x102500
#define ADC_DTOP                    0x102600
#define ADC_CHIPTOP                 0x101E00
#define HDMI                        0x102700
#define ADC_ATOPB                   0x103D00
#define HDMI2                       0x101A00
#define SC_0                        0x102E00
#define SC_2                        0x103000
#define VE_0                        0x103B00
#define VE_1                        0x103E00
#define VE_2                        0x103F00
#define DVI_ATOP                    0x110900
#define HDMIA                       0x110A00
#define ADC_DTOPB                   0x111200
#define DVI_ATOP1                   0x113200
#define HDMIB                       0x113300
#define DVI_ATOP2                   0x113400
#define HDMID                       0x113500
#define DVI_PS                      0x113600
#define HDMIC                       0x113700
#define HDCPKEY                     0x123A00
#define CLKGEN_SC2                  0x100A00
#define CLKGEN                      0x100B00
#define MIU0_BASE                   0x101200
#define MIU1_BASE                   0x100600
#define MIU_ATOP0                   0x110D00
#define MIU_ATOP1                   0x161600
#define MIU_ARB                     0x161500
#define MIU_ARB2                    0x162200
#define MIU_ARB3                    0x152000
#define MIU_ARB4                    0x152100
#define MIU_BA0                     0x310200
#define MIU_BA1                     0x310600
#define MIU_BA2                     0x310300
#define MIU_BA3                     0x310700
//frc
#define MIU_FRC1                0x162A00
#define MIU_FRC2                0x162B00
#define MIU_FRC3                0x152B00
#define MIU_FRC4                0x152C00
#define REG_MIU0_EX_BASE            0x162A00UL
#define REG_MIU0_ARBB_BASE          0x152000UL
#define REG_MIU1_EX_BASE            0x162B00UL
#define REG_MIU1_ARBB_BASE          0x152100UL
#define MIU_FRC_G0REQUEST_MASK   (0x0000)
#define MIU_FRC_G1REQUEST_MASK   (0x0000)
#define MIU_FRC_G2REQUEST_MASK   (0x0000)
#define MIU_FRC_G3REQUEST_MASK   (0x0000)
#define MIU_FRC_G4REQUEST_MASK   (0x0000)
#define MIU_FRC_G5REQUEST_MASK   (0x7FFF)
#define MIU_FRC_G6REQUEST_MASK   (0x0000)

//START OF HDMI BANK***********************
#define COMBO_PHY0_P0         0x170200
#define COMBO_PHY1_P0         0x170300
#define COMBO_PHY2_P0         0x172800
#define COMBO_PHY0_P1         0x170400
#define COMBO_PHY1_P1         0x170500
#define COMBO_PHY2_P1         0x172900
#define COMBO_PHY0_P2         0x170600
#define COMBO_PHY1_P2         0x170700
#define COMBO_PHY2_P2         0x172A00
#define COMBO_PHY0_P3         0x170800
#define COMBO_PHY1_P3         0x170900
#define COMBO_PHY2_P3         0x172B00

#define DVI_DTOP_DUAL_P0      0x171000
#define DVI_RSV_DUAL_P0       0x171100
#define HDCP_DUAL_P0          0x171200
#define DVI_DTOP_DUAL_P1      0x171300
#define DVI_RSV_DUAL_P1       0x171400
#define HDCP_DUAL_P1          0x171500
#define DVI_DTOP_DUAL_P2      0x171600
#define DVI_RSV_DUAL_P2       0x171700
#define HDCP_DUAL_P2          0x171800
#define DVI_DTOP_DUAL_P3      0x171900
#define DVI_RSV_DUAL_P3       0x171A00
#define HDCP_DUAL_P3          0x171B00

#define HDMI_DUAL_0           0x173000
#define HDMI2_DUAL_0          0x173100
#define HDMI3_DUAL_0          0x173400
#define HDMI_DUAL_1           0x173200
#define HDMI2_DUAL_1          0x173300
#define HDMI3_DUAL_1          0x173600

#define COMBO_GP_TOP          0x173900
#define SECURE_TZPC           0x173A00
//END OF HDMI BANK***********************

#define SC_BK00                     0x130000
#define SC_BK01                     0x130100
#define SC_BK02                     0x130200
#define SC_BK03                     0x130300
#define SC_BK04                     0x130400
#define SC_BK05                     0x130500
#define SC_BK06                     0x130600
#define SC_BK07                     0x130700
#define SC_BK08                     0x130800
#define SC_BK09                     0x130900
#define SC_BK0A                     0x130A00
#define SC_BK0B                     0x130B00
#define SC_BK0C                     0x130C00
#define SC_BK0D                     0x130D00
#define SC_BK0E                     0x130E00
#define SC_BK0F                     0x130F00
#define SC_BK10                     0x131000
#define SC_BK11                     0x131100
#define SC_BK12                     0x131200
#define SC_BK13                     0x131300
#define SC_BK14                     0x131400
#define SC_BK15                     0x131500
#define SC_BK16                     0x131600
#define SC_BK17                     0x131700
#define SC_BK18                     0x131800
#define SC_BK19                     0x131900
#define SC_BK1A                     0x131A00
#define SC_BK1B                     0x131B00
#define SC_BK1C                     0x131C00
#define SC_BK1D                     0x131D00
#define SC_BK1E                     0x131E00
#define SC_BK1F                     0x131F00
#define SC_BK20                     0x132000
#define SC_BK21                     0x132100
#define SC_BK22                     0x132200
#define SC_BK23                     0x132300
#define SC_BK24                     0x132400
#define SC_BK25                     0x132500
#define SC_BK26                     0x132600
#define SC_BK27                     0x132700
#define SC_BK28                     0x132800
#define SC_BK29                     0x132900
#define SC_BK2A                     0x132A00
#define SC_BK2B                     0x132B00
#define SC_BK2C                     0x132C00
#define SC_BK2D                     0x132D00
#define SC_BK2E                     0x132E00
#define SC_BK2F                     0x132F00
#define SC_BK30                     0x133000
#define SC_BK31                     0x133100
#define SC_BK32                     0x133200
#define SC_BK33                     0x133300
#define SC_BK34                     0x133400
#define SC_BK35                     0x133500
#define SC_BK36                     0x133600
#define SC_BK37                     0x133700
#define SC_BK38                     0x133800
#define SC_BK39                     0x133900
#define SC_BK3A                     0x133A00
#define SC_BK3B                     0x133B00
#define SC_BK3C                     0x133C00
#define SC_BK3D                     0x133D00
#define SC_BK3E                     0x133E00
#define SC_BK3F                     0x133F00
#define SC_BK40                     0x134000
#define SC_BK41                     0x134100
#define SC_BK42                     0x134200
#define SC_BK43                     0x134300
#define SC_BK44                     0x134400
#define SC_BK45                     0x134500
#define SC_BK46                     0x134600
#define SC_BK47                     0x134700
#define SC_BK48                     0x134800
#define SC_BK49                     0x134900
#define SC_BK4A                     0x134A00
#define SC_BK4B                     0x134B00
#define SC_BK4C                     0x134C00
#define SC_BK4D                     0x134D00
#define SC_BK4E                     0x134E00
#define SC_BK4F                     0x134F00
#define SC_BK50                     0x135000
#define SC_BK51                     0x135100
#define SC_BK52                     0x135200
#define SC_BK53                     0x135300
#define SC_BK54                     0x135400
#define SC_BK55                     0x135500
#define SC_BK56                     0x135600
#define SC_BK57                     0x135700
#define SC_BK58                     0x135800
#define SC_BK59                     0x135900
#define SC_BK5A                     0x135A00
#define SC_BK5B                     0x135B00
#define SC_BK5C                     0x135C00
#define SC_BK5D                     0x135D00
#define SC_BK5E                     0x135E00
#define SC_BK5F                     0x135F00
#define SC_BK60                     0x136000
#define SC_BK61                     0x136100
#define SC_BK62                     0x136200
#define SC_BK63                     0x136300
#define SC_BK64                     0x136400
#define SC_BK65                     0x136500
#define SC_BK66                     0x136600
#define SC_BK67                     0x136700
#define SC_BK68                     0x136800
#define SC_BK69                     0x136900
#define SC_BK6A                     0x136A00
#define SC_BK6B                     0x136B00
#define SC_BK6C                     0x136C00
#define SC_BK6D                     0x136D00
#define SC_BK6E                     0x136E00
#define SC_BK6F                     0x136F00
#define SC_BK70                     0x137000
#define SC_BK71                     0x137100
#define SC_BK72                     0x137200
#define SC_BK73                     0x137300
#define SC_BK74                     0x137400
#define SC_BK75                     0x137500
#define SC_BK76                     0x137600
#define SC_BK77                     0x137700
#define SC_BK78                     0x137800
#define SC_BK79                     0x137900
#define SC_BK7A                     0x137A00
#define SC_BK7B                     0x137B00
#define SC_BK7C                     0x137C00
#define SC_BK7D                     0x137D00
#define SC_BK7E                     0x137E00
#define SC_BK7F                     0x137F00

#define SC_BK80                     0x138000
#define SC_BK81                     0x138100
#define SC_BK82                     0x138200
#define SC_BK83                     0x138300
#define SC_BK84                     0x138400
#define SC_BK85                     0x138500
#define SC_BK86                     0x138600
#define SC_BK87                     0x138700
#define SC_BK88                     0x138800
#define SC_BK89                     0x138900
#define SC_BK8A                     0x138A00
#define SC_BK8B                     0x138B00
#define SC_BK8C                     0x138C00
#define SC_BK8D                     0x138D00
#define SC_BK8E                     0x138E00
#define SC_BK8F                     0x138F00
#define SC_BK90                     0x139000
#define SC_BK91                     0x139100
#define SC_BK92                     0x139200
#define SC_BK93                     0x139300
#define SC_BK94                     0x139400
#define SC_BK95                     0x139500
#define SC_BK96                     0x139600
#define SC_BK97                     0x139700
#define SC_BK98                     0x139800
#define SC_BK99                     0x139900
#define SC_BK9A                     0x139A00
#define SC_BK9B                     0x139B00
#define SC_BK9C                     0x139C00
#define SC_BK9D                     0x139D00
#define SC_BK9E                     0x139E00
#define SC_BK9F                     0x139F00
#define SC_BKA0                     0x13A000
#define SC_BKA1                     0x13A100
#define SC_BKA2                     0x13A200
#define SC_BKA3                     0x13A300
#define SC_BKA4                     0x13A400
#define SC_BKA5                     0x13A500
#define SC_BKA6                     0x13A600
#define SC_BKA7                     0x13A700
#define SC_BKA8                     0x13A800
#define SC_BKA9                     0x13A900
#define SC_BKAA                     0x13AA00
#define SC_BKAB                     0x13AB00
#define SC_BKAC                     0x13AC00
#define SC_BKAD                     0x13AD00
#define SC_BKAE                     0x13AE00
#define SC_BKAF                     0x13AF00
#define SC_BKB0                     0x13B000
#define SC_BKB1                     0x13B100
#define SC_BKB2                     0x13B200
#define SC_BKB3                     0x13B300
#define SC_BKB4                     0x13B400
#define SC_BKB5                     0x13B500
#define SC_BKB6                     0x13B600
#define SC_BKB7                     0x13B700
#define SC_BKB8                     0x13B800
#define SC_BKB9                     0x13B900
#define SC_BKBA                     0x13BA00
#define SC_BKBB                     0x13BB00
#define SC_BKBC                     0x13BC00
#define SC_BKBD                     0x13BD00
#define SC_BKBE                     0x13BE00
#define SC_BKBF                     0x13BF00
#define SC_BKC0                     0x13C000
#define SC_BKC1                     0x13C100
#define SC_BKC2                     0x13C200
#define SC_BKC3                     0x13C300
#define SC_BKC4                     0x13C400
#define SC_BKC5                     0x13C500
#define SC_BKC6                     0x13C600
#define SC_BKC7                     0x13C700
#define SC_BKC8                     0x13C800
#define SC_BKC9                     0x13C900
#define SC_BKCA                     0x13CA00
#define SC_BKCB                     0x13CB00
#define SC_BKCC                     0x13CC00
#define SC_BKCD                     0x13CD00
#define SC_BKCE                     0x13CE00
#define SC_BKCF                     0x13CF00
#define SC_BKD0                     0x13D000
#define SC_BKD1                     0x13D100
#define SC_BKD2                     0x13D200
#define SC_BKD3                     0x13D300
#define SC_BKD4                     0x13D400
#define SC_BKD5                     0x13D500
#define SC_BKD6                     0x13D600
#define SC_BKD7                     0x13D700
#define SC_BKD8                     0x13D800
#define SC_BKD9                     0x13D900
#define SC_BKDA                     0x13DA00
#define SC_BKDB                     0x13DB00
#define SC_BKDC                     0x13DC00
#define SC_BKDD                     0x13DD00
#define SC_BKDE                     0x13DE00
#define SC_BKDF                     0x13DF00
#define SC_BKE0                     0x13E000
#define SC_BKE1                     0x13E100
#define SC_BKE2                     0x13E200
#define SC_BKE3                     0x13E300
#define SC_BKE4                     0x13E400
#define SC_BKE5                     0x13E500
#define SC_BKE6                     0x13E600
#define SC_BKE7                     0x13E700
#define SC_BKE8                     0x13E800
#define SC_BKE9                     0x13E900
#define SC_BKEA                     0x13EA00
#define SC_BKEB                     0x13EB00
#define SC_BKEC                     0x13EC00
#define SC_BKED                     0x13ED00
#define SC_BKEE                     0x13EE00
#define SC_BKEF                     0x13EF00
#define SC_BKF0                     0x13F000
#define SC_BKF1                     0x13F100
#define SC_BKF2                     0x13F200
#define SC_BKF3                     0x13F300
#define SC_BKF4                     0x13F400
#define SC_BKF5                     0x13F500
#define SC_BKF6                     0x13F600
#define SC_BKF7                     0x13F700
#define SC_BKF8                     0x13F800
#define SC_BKF9                     0x13F900
#define SC_BKFA                     0x13FA00
#define SC_BKFB                     0x13FB00
#define SC_BKFC                     0x13FC00
#define SC_BKFD                     0x13FD00
#define SC_BKFE                     0x13FE00
#define SC_BKFF                     0x13FF00

#define LPLL_BK                     0x103100
#define MOD_BK                      0x103200
#define CLKGEN1_BK                  0x103300
#define RVD_BK                      0x100A00
#define MOD_A_BK                    0x111E00

// FSC bank
#define REG_FSC_BANK_BASE           0x140000

#define REG_FSC_BK03        		(REG_FSC_BANK_BASE+0x0300)
#define REG_FSC_BK20        		(REG_FSC_BANK_BASE+0x2000)
#define REG_FSC_BK23        		(REG_FSC_BANK_BASE+0x2300)

// FRC bank
#define REG_FRC_BANK_BASE           0x400000

#define REG_FRC_BK2E        		(REG_FRC_BANK_BASE+0x02E00)
#define REG_FRC_BK3E        		(REG_FRC_BANK_BASE+0x03E00)
#define REG_FRC_BK3F        		(REG_FRC_BANK_BASE+0x03F00)

#define REG_FRC_BK115        		(REG_FRC_BANK_BASE+0x11500)
#define REG_FRC_BK119        		(REG_FRC_BANK_BASE+0x11900)
#define REG_FRC_BK121        		(REG_FRC_BANK_BASE+0x12100)
#define REG_FRC_BK134        		(REG_FRC_BANK_BASE+0x13400)
#define REG_FRC_BK135        		(REG_FRC_BANK_BASE+0x13500)
#define REG_FRC_BK136        		(REG_FRC_BANK_BASE+0x13600)
#define REG_FRC_BK13A        		(REG_FRC_BANK_BASE+0x13A00)
#define REG_FRC_BK13B        		(REG_FRC_BANK_BASE+0x13B00)
#define REG_FRC_BK13C        		(REG_FRC_BANK_BASE+0x13C00)
#define REG_FRC_BK13D        		(REG_FRC_BANK_BASE+0x13D00)

#define REG_FRC_BK20A        		(REG_FRC_BANK_BASE+0x20A00)
#define REG_FRC_BK218        		(REG_FRC_BANK_BASE+0x21800)
#define REG_FRC_BK220        		(REG_FRC_BANK_BASE+0x22000)
#define REG_FRC_BK226        		(REG_FRC_BANK_BASE+0x22600)
#define REG_FRC_BK229        		(REG_FRC_BANK_BASE+0x22900)
#define REG_FRC_BK22C        		(REG_FRC_BANK_BASE+0x22C00)
#define REG_FRC_BK232        		(REG_FRC_BANK_BASE+0x23200)
#define REG_FRC_BK233        		(REG_FRC_BANK_BASE+0x23300)
#define REG_FRC_BK23A        		(REG_FRC_BANK_BASE+0x23A00)

#define REG_FRC_BK30D        		(REG_FRC_BANK_BASE+0x30D00)
#define REG_FRC_BK31B        		(REG_FRC_BANK_BASE+0x31B00)
#define REG_FRC_BK320        		(REG_FRC_BANK_BASE+0x32000)
#define REG_FRC_BK33A        		(REG_FRC_BANK_BASE+0x33A00)

#define REG_FRC_BK229_02            (REG_FRC_BANK_BASE+0x22902)

// PM SLEEP bank
#define REG_PM_SLEEP_BASE           0x000E00UL

typedef enum
{
        MIU0_BASE_IDX   =  0x00,    //  0x00
        MIU1_BASE_IDX  ,    //  0x01
        MIU_ARB_IDX    ,    //  0x02
        MIU_ARB2_IDX   ,    //  0x03
        CLKGEN_SC2_IDX     ,
        CLKGEN_IDX     ,    //  0x04
        ADC_ATOP_IDX   ,    //  0x05
        ADC_DTOP_IDX   ,    //  0x06
        ADC_CHIPTOP_IDX,    //  0x07
        HDMI_IDX       ,    //  0x08
        ADC_ATOPB_IDX  ,    //  0x09
        HDMI2_IDX      ,    //  0x0A
        SC_0_IDX       ,    //  0x0B
        SC_2_IDX       ,    //  0x0C
        VE_0_IDX       ,    //  0x0D
        VE_1_IDX       ,    //  0x0E
        VE_2_IDX       ,    //  0x0F
        DVI_ATOP_IDX   ,    //  0x10
        HDMIA_IDX      ,    //  0x11
        ADC_DTOPB_IDX  ,    //  0x12
        DVI_ATOP1_IDX  ,    //  0x13
        HDMIB_IDX      ,    //  0x14
        DVI_ATOP2_IDX  ,    //  0x15
        HDMID_IDX      ,    //  0x16
        DVI_PS_IDX     ,    //  0x17
        HDMIC_IDX      ,    //  0x18
        HDCPKEY_IDX    ,    //  0x19
        MIU_ARB3_IDX,  //0x1A
        COMBO_PHY0_P0_IDX, //0x1B
        COMBO_PHY1_P0_IDX, //0x1C
        COMBO_PHY2_P0_IDX, //0x1D
        COMBO_PHY0_P1_IDX, //0x1E
        COMBO_PHY1_P1_IDX, //0x1F
        COMBO_PHY2_P1_IDX, //Ox20
        COMBO_PHY0_P2_IDX, //0x21
        COMBO_PHY1_P2_IDX, //0x22
        COMBO_PHY2_P2_IDX, //0x23
        COMBO_PHY0_P3_IDX, //0x24
        COMBO_PHY1_P3_IDX, //0x25
        COMBO_PHY2_P3_IDX, //0x26

        DVI_DTOP_DUAL_P0_IDX,//0x27
        DVI_RSV_DUAL_P0_IDX, //0x28
        HDCP_DUAL_P0_IDX,//0x29
        DVI_DTOP_DUAL_P1_IDX,//0x2a
        DVI_RSV_DUAL_P1_IDX,//0x2b
        HDCP_DUAL_P1_IDX,//0x2c
        DVI_DTOP_DUAL_P2_IDX,//0x2d
        DVI_RSV_DUAL_P2_IDX,//0x2e
        HDCP_DUAL_P2_IDX,//0x2f
        DVI_DTOP_DUAL_P3_IDX,//0x30
        DVI_RSV_DUAL_P3_IDX,//0x31
        HDCP_DUAL_P3_IDX,//0x32

        HDMI_DUAL_0_IDX,//0x33
        HDMI2_DUAL_0_IDX,//0x34
        HDMI3_DUAL_0_IDX,//0x35
        HDMI_DUAL_1_IDX,//0x36
        HDMI2_DUAL_1_IDX,//0x37
        HDMI3_DUAL_1_IDX,//0x38
        COMBO_GP_TOP_IDX,//0x39
        SECURE_TZPC_IDX,//0x3a
        MIU_ARB4_IDX,//0x3b
        MIU_BA0_IDX,//0x3c
        MIU_BA1_IDX,//0x3d
        MIU_BA2_IDX,//0x3e
        MIU_BA3_IDX,//0x3f
#if 1 //FRC_INSIDE
// frc
        MIU_FRC1_IDX,//0x40
        MIU_FRC2_IDX,//0x41
        MIU_FRC3_IDX,//0x42
        MIU_FRC4_IDX,//0x43
#endif
        XC_RELATED_BANKNUM  // scaler related bank, now number is 0x1A
} EN_SC_STR_BANK_INDEX;

typedef enum
{
        XC_FSC_BK03_IDX   =  0x00,
		XC_FSC_BK20_IDX,
		XC_FSC_BK23_IDX,

		XC_FRC_BK2E_IDX,
		XC_FRC_BK3E_IDX,
		XC_FRC_BK3F_IDX,
		XC_FRC_BK115_IDX,
		XC_FRC_BK119_IDX,
		XC_FRC_BK121_IDX,
		XC_FRC_BK134_IDX,
		XC_FRC_BK135_IDX,
		XC_FRC_BK136_IDX,
		XC_FRC_BK13A_IDX,
		XC_FRC_BK13B_IDX,
		XC_FRC_BK13C_IDX,
		XC_FRC_BK13D_IDX,
		XC_FRC_BK20A_IDX,
		XC_FRC_BK218_IDX,
		XC_FRC_BK220_IDX,
		XC_FRC_BK226_IDX,
		XC_FRC_BK229_IDX,
		XC_FRC_BK22C_IDX,
		XC_FRC_BK232_IDX,
		XC_FRC_BK233_IDX,
		XC_FRC_BK23A_IDX,
		XC_FRC_BK30D_IDX,
		XC_FRC_BK31B_IDX,
		XC_FRC_BK320_IDX,
		XC_FRC_BK33A_IDX,

        XC_FRC_BANKNUM  // scaler related bank, now number is 0x1A
} EN_SC_STR_XC_FRC_BANK_INDEX;


#endif // End of __MST_REG_BANK_H

