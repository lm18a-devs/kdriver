#include <linux/device.h>
#include <linux/export.h>
#include <linux/io.h>

#include "mst_str.h"
#include "MsTypes.h"
#include "mhal_mfe.h"

/********************************************************************
    Common API
********************************************************************/
#define DUMP_REG_VALUE(u16Reg_in,reg_start)                    \
    do{ static int cnt=0;static int sz =0; int reg_idx = reg_start ;   \
            sz = sizeof(u16Reg_in)/sizeof(MS_U16);                         \
            for(cnt = 0; cnt < sz ; cnt++)                                        \
            {                                                                               \
                printk("[%02X]0x%04X  ",(int)reg_idx,u16Reg_in[cnt]);       \
                reg_idx+=1;                                                             \
                if((cnt+1) % 8 == 0)  printk("\n");                               \
            } printk("\n");                                                                \
        }while(0)


/********************************************************************
    MFE Define
********************************************************************/
#define MFE_STR_DEBUG   0

#define BANK_SIZE 0x80

#define MFE_REG_BASE0 (REG_BANK_MFE<<1)
#define MFE_REG_BASE1 (REG_BANK_MFE1<<1)

#define REG_BASE_ADDR  0x100000
#define MFE_BANK0_ADR  (REG_BASE_ADDR|MFE_REG_BASE0)
#define MFE_BANK1_ADR  (REG_BASE_ADDR|MFE_REG_BASE1)

static struct str_save_mfe_t {
    MS_U16 mfe_Bank0[BANK_SIZE];
    MS_U16 mfe_Bank1[BANK_SIZE];
} str_save_mfe;

static void str_save_mfe_reg(void)
{
    STR_PRINT("enter\n");
    MS_U16 u16Reg = 0x0;
    MS_U32 u32RegAddress = 0x0;
    MS_U16 start_index = 0x0;

    memset(&str_save_mfe, 0x0, sizeof(str_save_mfe));

    //save mfe bank0
    for(start_index = 0x0; start_index < BANK_SIZE; start_index++)
    {
        str_save_mfe.mfe_Bank0[start_index] = MDrv_STR_Read2Byte(MFE_BANK0_ADR, start_index);
    }

#if MFE_STR_DEBUG
    STR_PRINT("Save mfe_Bank0  =====================================+\n");
    DUMP_REG_VALUE(str_save_mfe.mfe_Bank0, u16Reg);
    STR_PRINT("DUMP mfe_Bank0  END =================================-\n\n");
#endif

    //save mfe bank1
    for(start_index = 0x0; start_index < BANK_SIZE; start_index++)
    {
        str_save_mfe.mfe_Bank1[start_index] = MDrv_STR_Read2Byte(MFE_BANK1_ADR, start_index);
    }

#if MFE_STR_DEBUG
    STR_PRINT("Save mfe_Bank1  =====================================+\n");
    DUMP_REG_VALUE(str_save_mfe.mfe_Bank1, u16Reg);
    STR_PRINT("DUMP mfe_Bank1 END =================================-\n\n");
#endif

    STR_PRINT("exit\n");
}
static void str_load_mfe_reg(void)
{
    STR_PRINT("enter\n");

    MS_U16 u16Reg = 0x0;
    MS_U32 u32RegAddress = 0x0;
    MS_U16 start_index = 0x0;

    //load mfe bank0
    for(start_index = 0x0; start_index < BANK_SIZE; start_index++)
    {
        MDrv_STR_Write2Byte(MFE_BANK0_ADR, start_index, str_save_mfe.mfe_Bank0[start_index]);
    }

#if MFE_STR_DEBUG
    STR_PRINT("Load mfe_Bank0 to Reg  ==============================+\n");
    DUMP_REG_VALUE(str_save_mfe.mfe_Bank0, u16Reg);
    STR_PRINT("DUMP mfe_Bank0 END =================================-\n\n");
#endif

    //load mfe bank1
    for(start_index = 0x0; start_index < BANK_SIZE; start_index++)
    {
        MDrv_STR_Write2Byte(MFE_BANK1_ADR, start_index, str_save_mfe.mfe_Bank1[start_index]);
    }

#if MFE_STR_DEBUG
    STR_PRINT("Load mfe_Bank1 to Reg  ==========================+\n");
    DUMP_REG_VALUE(str_save_mfe.mfe_Bank1, u16Reg);
    STR_PRINT("DUMP mfe_Bank1 END =============================-\n\n");
#endif

    STR_PRINT("exit\n");
}

extern int suspend_mfe(void)
{
    STR_PRINT("enter\n");
    str_save_mfe_reg();
    STR_PRINT("exit\n");
    return 0;
}

extern int resume_mfe(void)
{
    STR_PRINT("enter\n");
    str_load_mfe_reg();
    STR_PRINT("exit\n");
    return 0;
}
