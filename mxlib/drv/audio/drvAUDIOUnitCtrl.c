//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    drvAUDIOUnitCtrl.c
/// @brief  AUDIO "Unit" Control WITHOUT COMBO SKILL!!!!
/// @author MStar Semiconductor,Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined(ROLLS_ROYCE) || defined(AUDIO_UNIT_CTRL)

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#include standard header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [#include local header] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#include "drvAUDIOUnitCtrl.h"
#include "halAUDIOUnitCtrl.h"
#include "drvAUDIO_if.h"
#include "./internal/drvAUDIO_internal.h"
#include "./internal/drvAUDIOUnitCtrl_config.h"
#include "./internal/drvAUDIOUnitCtrl_customer_config.h"

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#include local library] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [#define] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [#define] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [variable / struct / enum] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [variable / struct / enum] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Pre-Declaration Area] pre-declared [function] declare in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
/* Initialize, STR */

/* Connect & Disconnect */

/* Start & Stop */

/* SPDIF */

/* HDMI */

/* ATV */

/* Decoder */

/* Common Decoder */

/* Common */

/* Customized patch */

/* Customized Internal patch */

/* Clip Play for ES */

/* Clip Play for PCM */

/* Gain, Mute & Delay */

/* AENC */

/* PCM Capture */

/* PCM IO Control */

/* MM New Mode */

/* Mstar Sound Effect */

/* Advanced Sound Effect */

/* internal used functions */

#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// [Share Area] shared [function] declare / implement in this area.
//-------------------------------------------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------------------------------------------
// [Distinction Area] [kernel space / user space] [function] declare / implement in this area.
//-------------------------------------------------------------------------------------------------------------------------------------
#ifdef MSOS_TYPE_LINUX_KERNEL //Kernel Space

#else //User Space

#endif

//-------------------------------------------------------------------------------------------------------------------------------------
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// public APIs open to upper layer, DO NOT add Internal function / variable / define.
// please understand, it's important.
//-------------------------------------------------------------------------------------------------------------------------------------
/* Initialize, STR */

/* Connect & Disconnect */
MS_BOOL DRV_AUDIO_UNIT_SetParserMode(AUDIO_UNIT_PARSER_MODE ParserMode)
{
    return HAL_AUDIO_UNIT_SetParserMode(ParserMode);
}

MS_BOOL DRV_AUDIO_UNIT_ADEC_Connect(AUDIO_UNIT_ADEC_INDEX currentConnect, AUDIO_UNIT_ADEC_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_ADEC_Connect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_ADEC_Disconnect(AUDIO_UNIT_ADEC_INDEX currentConnect)
{
    return HAL_AUDIO_UNIT_ADEC_Disconnect(currentConnect);
}

MS_BOOL DRV_AUDIO_UNIT_ADC_Connect(AUDIO_UNIT_ADC_INDEX currentConnect, AUDIO_UNIT_ADC_IN_PORT portNum)
{
    return HAL_AUDIO_UNIT_ADC_Connect(currentConnect, portNum);
}

MS_BOOL DRV_AUDIO_UNIT_ADC_Disconnect(AUDIO_UNIT_ADC_INDEX currentConnect, AUDIO_UNIT_ADC_IN_PORT portNum)
{
    return HAL_AUDIO_UNIT_ADC_Disconnect(currentConnect, portNum);
}

MS_BOOL DRV_AUDIO_UNIT_PCM_Mixer_Connect(AUDIO_UNIT_PCM_MIXER_INDEX currentConnect, AUDIO_UNIT_PCM_MIXER_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_PCM_Mixer_Connect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_PCM_Mixer_Disconnect(AUDIO_UNIT_PCM_MIXER_INDEX currentConnect, AUDIO_UNIT_PCM_MIXER_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_PCM_Mixer_Disconnect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_CH_Sound_Connect(AUDIO_UNIT_CH_SOUND currentConnect, AUDIO_UNIT_CH_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_CH_Sound_Connect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_CH_Sound_Disconnect(AUDIO_UNIT_CH_SOUND currentConnect, AUDIO_UNIT_CH_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_CH_Sound_Disconnect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_FW_MIXER_Connect(AUDIO_UNIT_FWM_INDEX currentConnect, AUDIO_UNIT_FWM_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_FW_MIXER_Connect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_FW_MIXER_Disconnect(AUDIO_UNIT_FWM_INDEX currentConnect, AUDIO_UNIT_FWM_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_FW_MIXER_Disconnect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_SE_Connect(AUDIO_UNIT_SE_INDEX currentConnect, AUDIO_UNIT_SE_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_SE_Connect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_SE_Disconnect(AUDIO_UNIT_SE_INDEX currentConnect, AUDIO_UNIT_SE_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_SE_Disconnect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_SNDOUT_Connect(AUDIO_UNIT_SOUNDOUT_INDEX currentConnect, AUDIO_UNIT_SOUNDOUT_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_SNDOUT_Connect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_SNDOUT_Disconnect(AUDIO_UNIT_SOUNDOUT_INDEX currentConnect, AUDIO_UNIT_SOUNDOUT_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_SNDOUT_Disconnect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_PCM_CAPTURE_Connect(AUDIO_UNIT_PCM_CAPTURE_INDEX currentConnect, AUDIO_UNIT_PCM_CAPTURE_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_PCM_CAPTURE_Connect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_PCM_CAPTURE_Disconnect(AUDIO_UNIT_PCM_CAPTURE_INDEX currentConnect, AUDIO_UNIT_PCM_CAPTURE_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_PCM_CAPTURE_Disconnect(currentConnect, inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_MP3_ENC_Connect(AUDIO_UNIT_MP3_ENC_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_MP3_ENC_Connect(inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_MP3_ENC_Disconnect(AUDIO_UNIT_MP3_ENC_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_MP3_ENC_Disconnect(inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_AAC_ENC_Connect(AUDIO_UNIT_AAC_ENC_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_AAC_ENC_Connect(inputConnect);
}

MS_BOOL DRV_AUDIO_UNIT_AAC_ENC_Disconnect(AUDIO_UNIT_AAC_ENC_INPUT inputConnect)
{
    return HAL_AUDIO_UNIT_AAC_ENC_Disconnect(inputConnect);
}

/* Start & Stop */

/* SPDIF */

/* HDMI */

/* ATV */

/* Decoder */

/* Common Decoder */

/* Common */

/* Customized patch */

/* Clip Play for ES */

/* Clip Play for PCM */

/* Gain, Mute & Delay */

/* AENC */

/* PCM Capture */

/* PCM IO Control */

/* MM New Mode */

/* Mstar Sound Effect */

/* Advanced Sound Effect */

#endif //#if defined(ROLLS_ROYCE) || defined(AUDIO_UNIT_CTRL)

