#if !defined(MSOS_TYPE_LINUX_KERNEL)
#include "string.h"
#include <stdio.h>
#else
#include <linux/string.h>
#include <linux/slab.h>
#endif
#include "MsTypes.h"
#include "utopia_dapi.h"
#include "drvGPIO.h"
#include "drvGPIO_private.h"
#include "MsOS.h"
#include "utopia.h"


enum
{
    GPIO_POOL_ID_GPIO0=0
} eGPIOPoolID;


#ifdef CONFIG_UTOPIA_PROC_DBG_SUPPORT
#define CmdCnt 3
static int array[CmdCnt];

typedef enum{
    MDBCMD_GET_GPIO = 0x0,
    MDBCMD_SET_GPIO,
    MDBCMD_GET_GPIO_OEN,
}UTOPIA_MDBCMD_GPIO;

static int atoi_mstar(char *s)
{
    int sum = 0;
    int i = 0;
    int b = 0;

    for(i = 0; i < CmdCnt; i++)
        array[i] = 0;
    //printf("S's size: %d\n", sizeof(s));
    for(i = 0;s[i] != '\0';i++)
    {
        if((s[i]=='A') || s[i]=='a')
        {
          sum = sum*16+10;
          array[b] = array[b]*16+10;
        }
        else if((s[i]=='B') || (s[i]=='b'))
        {
          sum = sum*16+11;
          array[b] = array[b]*16+11;
        }
        else if((s[i]=='C') || (s[i]=='c'))
        {
          sum = sum*16+12;
          array[b] = array[b]*16+12;
        }
        else if((s[i]=='D') || s[i]=='d')
        {
          sum = sum*16+13;
          array[b] = array[b]*16+13;
        }
        else if((s[i]=='E') || s[i]=='e')
        {
          sum = sum*16+14;
          array[b] = array[b]*16+14;
        }
        else if((s[i]=='F') || s[i]=='f')
        {
          sum = sum*16+15;
          array[b] = array[b]*16+15;
        }
        else if (s[i]==' ')
        {
          b++;
          if (b > CmdCnt-1)
            break;
        }
        else
        {
          sum = sum*16+s[i]-'0';
          array[b] = array[b]*16+s[i]-'0';
        }
    }
    //printf("b=%d\n", b);
    //printf("array [0] : %x\n",array[0]);
    //printf("array [1] : %x\n",array[1]);
    //printf("array [2] : %x\n",array[2]);
    //printf("array [3] : %x\n",array[3]);
    //printf("array [4] : %x\n",array[4]);
    //printf("array [5] : %x\n",array[5]);

    return sum;
};

void MDrv_GPIO_Mdb_Parse(MS_U64* u64ReqHdl)
{
    int temp = 0;

    mdrv_gpio_init();
    array[1]++;
    switch(array[0])
    {
        case MDBCMD_GET_GPIO:
            MdbPrint(u64ReqHdl, "---------MStar GPIO INPUT STATUS----------\n");
            temp = mdrv_gpio_get_level(array[1]);
            MdbPrint(u64ReqHdl, "GPIO PAD: %d\n", array[1]);
            MdbPrint(u64ReqHdl, "GET STATUS: %d\n", temp);
            break;
        case MDBCMD_SET_GPIO:
            MdbPrint(u64ReqHdl, "--------- MStar GPIO SETTING ---------\n");
            if(array[2] == 0)
            {
                mdrv_gpio_set_low(array[1]);
                MdbPrint(u64ReqHdl, "GPIO PAD: %d\n", array[1]);
                MdbPrint(u64ReqHdl, "SET GPIO: 0\n");
            }
            else if (array[2] == 1)
            {
                mdrv_gpio_set_high(array[1]);
                MdbPrint(u64ReqHdl, "GPIO PAD: %d\n", array[1]);
                MdbPrint(u64ReqHdl, "SET GPIO: 1\n");
            }
            else
            {
                MdbPrint(u64ReqHdl, "Error command: %d, HIGH:1 LOW:0\n", array[3]);
            }
            break;
        case MDBCMD_GET_GPIO_OEN:
            MdbPrint(u64ReqHdl, "---------MStar GPIO GET OEN STATUS----------\n");
            temp = mdrv_gpio_get_inout(array[1]);
            MdbPrint(u64ReqHdl, "GPIO PAD: %d\n", array[1]);
            MdbPrint(u64ReqHdl, "GET OEN STATUS: ", temp);
            if(temp == 0)
                MdbPrint(u64ReqHdl, "OUTPUT\n");
            else
                MdbPrint(u64ReqHdl, "INPUT\n");
            break;
        default:
            MdbPrint(u64ReqHdl, "Check CMD.\n");
            MdbPrint(u64ReqHdl, "GET_GPIO: 0\n");
            MdbPrint(u64ReqHdl, "SET_GPIO: 1\n");
            MdbPrint(u64ReqHdl, "GET_GPIO_OEN: 2\n");
          break;
    }
}

MS_U32 GPIOMdbIoctl(MS_U32 cmd, const void* const pArgs)
{
    MDBCMD_CMDLINE_PARAMETER *paraCmdLine;
    MDBCMD_GETINFO_PARAMETER *paraGetInfo;
    char tmpInput[40] = "";
    int i = 0;

    switch(cmd)
    {
        case MDBCMD_CMDLINE:
            paraCmdLine = (MDBCMD_CMDLINE_PARAMETER *)pArgs;
            //MdbPrint(paraCmdLine->u64ReqHdl,"LINE:%d, MDBCMD_CMDLINE\n", __LINE__);
            //MdbPrint(paraCmdLine->u64ReqHdl,"u32CmdSize: %d\n", paraCmdLine->u32CmdSize);
            //MdbPrint(paraCmdLine->u64ReqHdl,"pcCmdLine: %s\n", paraCmdLine->pcCmdLine);

            //TC MDB function----------------------------
            strncpy(tmpInput, paraCmdLine->pcCmdLine, sizeof(tmpInput));
            if(strncmp(tmpInput, "help", 4) == 0)
            {
                MdbPrint(paraCmdLine->u64ReqHdl,"---------MStar GPIO Help---------\n");
                MdbPrint(paraCmdLine->u64ReqHdl,"1.  Get GPIO status(high/low)\n");
                MdbPrint(paraCmdLine->u64ReqHdl,"      #echo 0 (GPIO NUM HEX) > /proc/utopia_mdb/gpio\n");
                MdbPrint(paraCmdLine->u64ReqHdl,"      [e.g.]#echo 0 55 > /proc/utopia_mdb/gpio\n");
                MdbPrint(paraCmdLine->u64ReqHdl,"2.  Set GPIO (HIGH/LOW)\n");
                MdbPrint(paraCmdLine->u64ReqHdl,"      echo 1 (GPIO NUM HEX) (HIGH/LOW)> /proc/utopia_mdb/gpio\n");
                MdbPrint(paraCmdLine->u64ReqHdl,"      [e.g.]echo 0 > /proc/utopia_mdb/swi2c\n");
                MdbPrint(paraCmdLine->u64ReqHdl,"3.  Get GPIO OEN\n");
                MdbPrint(paraCmdLine->u64ReqHdl,"      echo 2 (GPIO NUM) > proc/utopia_mdb/gpio\n");
                MdbPrint(paraCmdLine->u64ReqHdl,"      [e.g.]echo 2 55 > /proc/utopia_mdb/gpio\n");
            }
            else
            {
                atoi_mstar(tmpInput);
                MDrv_GPIO_Mdb_Parse(paraCmdLine->u64ReqHdl);
            }
            paraCmdLine->result = MDBRESULT_SUCCESS_FIN;
            break;
        case MDBCMD_GETINFO:
            paraGetInfo = (MDBCMD_GETINFO_PARAMETER *)pArgs;
            //MdbPrint(paraGetInfo->u64ReqHdl,"LINE:%d, MDBCMD_GETINFO\n", __LINE__);
            paraGetInfo->result = MDBRESULT_SUCCESS_FIN;
            break;
        default:
            MdbPrint(paraGetInfo->u64ReqHdl,"unknown cmd\n", __LINE__);
            break;
    }
    return 0;
}
#endif

// this func will be call to init by utopia20 framework
void GPIORegisterToUtopia(FUtopiaOpen ModuleType)
{
     void* pUtopiaModule = NULL;
    UtopiaModuleCreate(MODULE_GPIO, 8, &pUtopiaModule);
    UtopiaModuleRegister(pUtopiaModule);
    // register func for module, after register here, then ap call UtopiaOpen/UtopiaIoctl/UtopiaClose can call to these registered standard func
    UtopiaModuleSetupFunctionPtr(pUtopiaModule, (FUtopiaOpen)GPIOOpen, (FUtopiaClose)GPIOClose, (FUtopiaIOctl)GPIOIoctl);
    // 2. Resource register
    void* psResource = NULL;
    // start func to add resources of a certain Resource_Pool
    UtopiaModuleAddResourceStart(pUtopiaModule, GPIO_POOL_ID_GPIO0);
    // create a resouce and regiter it to a certain Resource_Pool, resource can alloc private for internal use
    UtopiaResourceCreate("gpio0", sizeof(GPIO_RESOURCE_PRIVATE), &psResource);
    UtopiaResourceRegister(pUtopiaModule, psResource, GPIO_POOL_ID_GPIO0);

    //UtopiaResourceCreate("cmdq1", sizeof(CMDQ_RESOURCE_PRIVATE), &psResource);
    //UtopiaResourceRegister(pUtopiaModule, psResource, CMDQ_POOL_ID_CMDQ0);
    // end func to add resources of a certain Resource_Pool(this will set the ResourceSemaphore of this ResourcePool)
    UtopiaModuleAddResourceEnd(pUtopiaModule, GPIO_POOL_ID_GPIO0);

    #ifdef CONFIG_UTOPIA_PROC_DBG_SUPPORT
    UtopiaModuleRegisterMdbNode("gpio", (FUtopiaMdbIoctl)GPIOMdbIoctl);
    #endif
}

MS_U32 GPIOOpen(void** ppInstance, MS_U32 u32ModuleVersion, void* pAttribute)
{
	GPIO_INSTANT_PRIVATE *pGpioPri = NULL;
	void *pGpioPriVoid = NULL;

	//UTOPIA_TRACE(MS_UTOPIA_DB_LEVEL_TRACE,printf("enter %s %d\n",__FUNCTION__,__LINE__));
	// instance is allocated here, also can allocate private for internal use, ex, BDMA_INSTANT_PRIVATE
	UtopiaInstanceCreate(sizeof(GPIO_INSTANT_PRIVATE), ppInstance);
	// setup func in private and assign the calling func in func ptr in instance private
	UtopiaInstanceGetPrivate(*ppInstance, &pGpioPriVoid);
	pGpioPri= (GPIO_INSTANT_PRIVATE*)pGpioPriVoid;
	pGpioPri->fpGpioInit= (IOCTL_GPIO_INIT)mdrv_gpio_init_U2K;
	pGpioPri->fpGpioGetSts= (IOCTL_GPIO_GET_STS)MDrv_GPIO_GetStatus_U2K;
	pGpioPri->fpGpioSetH= (IOCTL_GPIO_SET_H)mdrv_gpio_set_high_U2K;
	pGpioPri->fpGpioSetL= (IOCTL_GPIO_SET_L)mdrv_gpio_set_low_U2K;
	pGpioPri->fpGpioSetInput= (IOCTL_GPIO_SET_INPUT)mdrv_gpio_set_input_U2K;
	pGpioPri->fpGpioGetInout= (IOCTL_GPIO_GET_INOUT)mdrv_gpio_get_inout_U2K;
	pGpioPri->fpGpioGetLevel= (IOCTL_GPIO_GET_LEVEL)mdrv_gpio_get_level_U2K;
	pGpioPri->fpGpioAttachInterrupt= (IOCTL_GPIO_ATTACH_INTERRUPT)mdrv_gpio_attach_interrupt_U2K;
	pGpioPri->fpGpioDetachInterrupt= (IOCTL_GPIO_DETACH_INTERRUPT)mdrv_gpio_detach_interrupt_U2K;
	pGpioPri->fpGpioDisableInterrupt = (IOCTL_GPIO_DISABLE_INTERRUPT)mdrv_gpio_disable_interrupt_U2K;
	pGpioPri->fpGpioEnableInterrupt = (IOCTL_GPIO_ENALBE_INTERRUPT)mdrv_gpio_enable_interrupt_U2K;
	pGpioPri->fpGpioDisableInterruptAll= (IOCTL_GPIO_DISABLE_INTERRUPT_ALL)mdrv_gpio_disable_interrupt_all_U2K;
	pGpioPri->fpGpioEnableInterruptAll = (IOCTL_GPIO_ENABLE_INTERRUPT_ALL)mdrv_gpio_enable_interrupt_all_U2K;
	pGpioPri->fpGpioInterruptAction = (IOCTL_GPIO_INTERRUPT_ACTION) mdrv_gpio_interrupt_action_U2K;
	return UTOPIA_STATUS_SUCCESS;
}

// FIXME: why static?
MS_U32 GPIOIoctl(void* pInstance, MS_U32 u32Cmd, void* pArgs)
{
	void* pModule = NULL;
	GPIO_INSTANT_PRIVATE* psGpioInstPri = NULL;
	MS_U32 u32Ret = TRUE;
	MS_U32 u32InOutVal;
	MS_U32 u32LevelVal;
	
	UtopiaInstanceGetModule(pInstance, &pModule);
	UtopiaInstanceGetPrivate(pInstance, (void**)&psGpioInstPri);
	GPIO_PRIVATE_PARAM *param = NULL;
	if (pArgs != NULL)
            param= (GPIO_PRIVATE_PARAM *) pArgs;
        else
            return UTOPIA_STATUS_FAIL;

    switch(u32Cmd)
    {
        case MDrv_CMD_GPIO_Init:
            psGpioInstPri->fpGpioInit();
            break;
        case MDrv_CMD_GPIO_GetStauts:
            psGpioInstPri->fpGpioGetSts(param->privatGpioSts.pSts);
            break;
        case MDrv_CMD_GPIO_Set_High:
            psGpioInstPri->fpGpioSetH(param->privateGpioSetHigh.gpio_num);
            break;
        case MDrv_CMD_GPIO_Set_Low:
            psGpioInstPri->fpGpioSetL(param->privateGpioSetLow.gpio_num);
            break;
        case  MDrv_CMD_GPIO_Set_Input:
            psGpioInstPri->fpGpioSetInput(param->privateGpioInput.gpio_num);
            break;
        case MDrv_CMD_GPIO_Get_Inout:
		u32InOutVal = psGpioInstPri->fpGpioGetInout(param->privateGpioGetInout.gpio_num);
		param->privateGpioGetInout.u32InOut=u32InOutVal;
            break;
        case  MDrv_CMD_GPIO_Get_Level:
             u32LevelVal = psGpioInstPri->fpGpioGetLevel(param->privateGpioGetLevel.gpio_num);
		param->privateGpioGetLevel.u32Level=u32LevelVal;
            break;
        case MDrv_CMD_GPIO_Attach_Interrupt:
		u32Ret = psGpioInstPri->fpGpioAttachInterrupt(param->privateGpioAttachInterrupt.gpio_num,param->privateGpioAttachInterrupt.gpio_edge_type,param->privateGpioAttachInterrupt.GPIO_Callback);
            break;
        case MDrv_CMD_GPIO_Detach_Interrupt:
            u32Ret = psGpioInstPri->fpGpioDetachInterrupt(param->privateGpioDetachInterrupt.gpio_num);
            break;
        case  MDrv_CMD_GPIO_En_Interrupt:
	     u32Ret = psGpioInstPri->fpGpioEnableInterrupt(param->privateGpioEnableInterrupt.gpio_num);
        case MDrv_CMD_GPIO_Dis_Interrupt:
	     u32Ret = psGpioInstPri->fpGpioDisableInterrupt(param->privateGpioDisableInterrupt.gpio_num);
            break;
        case MDrv_CMD_GPIO_Dis_Interrupt_All:
	     psGpioInstPri->fpGpioDisableInterruptAll();
            break;
        case MDrv_CMD_GPIO_En_Interrupt_All:
	     psGpioInstPri->fpGpioEnableInterruptAll();
            break;
        case MDrv_CMD_GPIO_Interrupt_Action:
	     psGpioInstPri->fpGpioInterruptAction();
	     break;
        default:
            break;
    }
	return  (u32Ret==TRUE?UTOPIA_STATUS_SUCCESS:UTOPIA_STATUS_FAIL); // FIXME: error code
}

MS_U32 GPIOClose(void* pInstance)
{
	UtopiaInstanceDelete(pInstance);

	return UTOPIA_STATUS_SUCCESS;
}


